// remotesc.cpp
// Author: oleshii
// Created on Wen May 3 13:33:23 MSK 2017
// Copyright (c) zecurion. All rights reserved.
// Remote shadow copy management thread

#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <pthread.h>
#include <dirent.h>
#include <sys/stat.h>

#include <typeinfo>
#include <string>
#include <vector>
#include <algorithm>
#include <set>
using std::string;
using std::vector;
using std::find;

#include "../../MAC/common/zlck_comm_defs.h"
#include "../../MAC/zlib/stdafx.h"
#include "../../MAC/zlib/zlib.h"
#include "../../MAC/zlib/zlib_stringa.h"
#include "../../MAC/service/zlockservice/zlockservice/zntf_msg.h"
#include "../../MAC/service/zlockservice/zlockservice/preflang.h"
#include "../../MAC/zlib/zlib_array.h"

#if  1//DEBUG
#define ASSERT assert
#else
#define ASSERT
#endif
#define LOG ZTRACE

#include "../sczlock/sc_uiface.h"
#include "../zlock/dmacro.h"
#include "../../MAC/service/zlockservice/zlockservice/getcfg.h"
#include "../../MAC/service/zlockservice/zlockservice/macsvc.h"
#include "../../MAC/zweb_client/zws_client.h" // zecurion HTML client library support

#define ACTION  "hint=\"%s\""
#define ATTACH  "Attachments"
#define MPARS   ".MessageParameters"
#define MINARGS 4
#define MAXARGS (MINARGS + 1)

string GetSCLocalPath(void);
int GetCurrentLanguage(void);
ZLCK_SVC_DATA_s* GetTLS(int in_ID);
void GetUser(ZLib_StringA& inou_szUser);
int ReadCfg(bool in_bInv, SVCCONFIG* pcfg);
void GetHostName(ZLib_StringA& inou_szHost);
void Clean(const ZLib_StringA& in_sFolderName);
ZLib_StringA GetCfgIPAddress(const char* in_pszServer);
ZLib_StringA GetFileContent(const ZLib_StringA& in_szFname);
extern "C" int ZipCompress(int argc, char* argv[]); // zip compression
int GetStreamContents(const string& szName, vector<string>& inou_vszData, bool in_bFs);
void GetDateTime(ZLib_StringA& ref_sDate, ZLib_StringA& ref_sTime, time_t in_DateTime);
bool GetFONSrvs(ZLibT_Array<ZLib_StringA>& inou_Arr, const char* in_pszLSrv, BYTE in_uStT, const char* in_pszCfSrv);

// Local Shadow Copy archive contents
// $(LSC)/{GUID}/Attachments/file
// $(LSC)/{GUID}/.MessageParameters (XML contents)

// Notify archive contents
// $(LNTF)/{GUID}/.MessageParameters

template <class T> bool is_Notify(const vector<T>& instr) { return find(instr.begin(), instr.end(), ACTION) != instr.end(); }
template <class T> bool is_Sc(const vector<T>& instr) { return !is_Notify(instr); }
template <class T> bool is_Empty(const T& in_v) { return NOTHING < in_v.Get_Num_Elements(); }

static bool get_WD(string& inou_szfName)
{
    char asz[PATH_MAX] = {0};
    if(!inou_szfName.empty())
        inou_szfName.clear();
    if((NULL == getwd(asz)) || (NOTHING == asz[OK]))
    {
        ZTRACE("%s !!! getwd error %d %s !!!\n", __func__, errno, strerror(errno));
        return false;
    }
    inou_szfName = asz;
    return true;
}

static int chg_WD(const string& in_sCD)
{
    do
    {
        if(!IS_SUCCEEDED(chdir((in_sCD.c_str()))))
        {
            LOG("%s !!! chdir error %d %s !!!\n", __func__, errno, strerror(errno));
            return -1;
        }
    } while (false);
    return OK;
}

static int send_Request(const ZLib_StringA& in_sUrl, const ZLib_StringA& in_sRequest)
{
    ZLib_StringA stmp;
    int ret = NOTHING;
    ZTRACE("%s %s\n", __func__, (LPCTSTR)in_sUrl);
    do
    {
        ret = ZWS_Request_Simple2((LPCSTR)in_sUrl, (LPCSTR)in_sRequest, (unsigned int)in_sRequest.Get_Length(), &stmp);
        ZTRACE("%s res %d resp length %d\n", __func__, ret, (int)stmp.Get_Length());
        if(OK200 != ret)
        {
            LOG("%s !!! ZWS_Request_Simple error %d !!!\n", __func__, ret);
            break;
        }
        LOG("%s %s\n", __func__, (LPCSTR)stmp);
    } while (false);
    return ret;
}

static int write_In(const string& in_sName, const string& in_sContent)
{
    int r = NOTHING;
    ZLib_File_File file;
    do
    {
        if(!file.Create(in_sName.c_str()))
        {
            ZTRACE("%s !!! Create file %s error %d %s !!!\n", __func__, in_sName.c_str(), errno, strerror(errno));
            r = -1;
            break;
        }
        if(file.Write(in_sContent.c_str(), in_sContent.length()) != in_sContent.length())
        {
            LOG("%s !!! Write file error %d %s !!!\n", __func__, errno, strerror(errno));
            file.Close();
            unlink(in_sName.c_str());
            r = -2;
            break;
        }
    } while(false);
    return r;
}

static int get_SCFileName(const string& in_psDir, string& inou_sFile)
{
    struct stat statbuf;
    struct dirent* dp = NULL;
    DIR* dirp = NULL;
    int r = OK;

    do
    {
        //ZTRACE("%s %s\n", __func__, in_psDir.c_str());
        dirp = opendir(in_psDir.c_str());
        if(NULL == dirp)
        {
            LOG("%s !!! opendir %s error %d %s\n", __func__, in_psDir.c_str(), errno, strerror(errno));
            ERR(r, -1);
        }
        while(NULL != (dp = readdir(dirp)))
        {
            //ZTRACE("%s %s\n", __func__, dp->d_name);
            if(NOTHING == strcmp(dp->d_name, ".") || NOTHING == strcmp(dp->d_name, ".."))
                continue;
            inou_sFile = in_psDir + '/' + dp->d_name;
            // meta files
            if(NULL == strstr(inou_sFile.c_str(), META))
            {
                inou_sFile.clear();
                continue;
            }
            if(!IS_SUCCEEDED(stat(inou_sFile.c_str(), &statbuf)))
            {
                LOG("%s !!! stat %s error %d %s !!!\n", __func__, inou_sFile.c_str(), errno, strerror(errno));
                inou_sFile.clear();
                continue;
            }
            if(NOTHING == statbuf.st_size || !IS_FLG(statbuf.st_mode, S_IFREG))
            {
                LOG("%s inappropriate entity %s\n", __func__, inou_sFile.c_str());
                inou_sFile.clear();
                continue;
            }
            break;
        }
        if(NULL != dirp)
            closedir(dirp);
    } while (false);
    return r;
}

static const char* get_LocalString(int in_MsgID, int in_Lng)
{
    const char* psret = "Unknown message";
    int i;

    do
    {
        if(ZLCK_ERR_SUCCESS > in_MsgID || ZLCK_ERR_DIR_READED < in_MsgID)
        {
            ZTRACE("%s !!! Illegal message id %d!!!\n", __func__, in_MsgID);
            break;
        }
        for(i = 0; i < ARRAYSIZE(arMsg); i++)
        {
            if(in_MsgID == arMsg[i].id)
            {
                switch(in_Lng)
                {
                    case RUSSIAN:
                        psret = arMsg[i].locmsg;
                    break;
                    default:
                        psret = arMsg[i].szmsg;
                    break;
                }
            }
        }
    } while (false);
    return psret;
}

static void get_Contents(const string& szName, ZLib_StringA& inou_sCont)
{
    ZLib_String s = GetFileContent(szName.c_str());
    ASSERT(!s.Is_Empty() && "!!! Can't read file !!!");
    inou_sCont = ZLib_StringA(s.Get_Data(), s.Get_Length());
}

template <typename T> class __attribute__ ((visibility("hidden"))) CGetName
{
private:
    CGetName(void) = delete;
    CGetName(CGetName&) = delete;
    CGetName(const CGetName&) = delete;
    CGetName& operator=(const CGetName&) = delete;
    CGetName& operator=(CGetName) = delete;
public:
    CGetName(const T& s_ref, T& d_ref)
    {
        do
        {
            size_t sz = s_ref.rfind("/");
            if(T::npos == sz)
                d_ref = s_ref;
            else
                d_ref = s_ref.substr(sz + 1);
        } while(false);
    }
};

template <typename T, typename T2> class __attribute__ ((visibility("hidden"))) CUuidStr
{
private:
    CUuidStr(void) = delete;
    CUuidStr(CUuidStr&) = delete;
    CUuidStr(const CUuidStr&) = delete;
    CUuidStr& operator=(const CUuidStr&) = delete;
    CUuidStr& operator=(CUuidStr) = delete;
public:
    CUuidStr(const T& in_ref, T2& out_ref)
    {
        size_t fp = in_ref.find('{');
        if(T::npos != fp)
        {
            size_t lp = in_ref.find('}');
            if(T::npos != lp && ++lp > fp)
                out_ref = T(in_ref.c_str(), fp, lp - fp);
        }
        ASSERT(!out_ref.Is_Empty() && "!!! An invalid GUID !!!");
    }
};

template <class T1, class T2> class __attribute__ ((visibility("hidden"))) CDirCreator
{
private:
    CDirCreator(void) = delete;
    CDirCreator(CDirCreator&) = delete;
    CDirCreator(const CDirCreator&) = delete;
    CDirCreator& operator=(const CDirCreator&) = delete;
    CDirCreator& operator=(CDirCreator) = delete;
public:
    explicit CDirCreator(T1& inou_Ref, const T2& in_sFolder)
    {
        string s(STORPATH);
        s += in_sFolder;
        inou_Ref = mkdir(s.c_str(), 0777);
        if(!IS_SUCCEEDED(inou_Ref))
            LOG("%s !!! mkdir %s error %d %s !!!\n", __func__, s.c_str(), errno, strerror(errno));
    }
};

template <class T, class T1> class __attribute__ ((visibility("hidden"))) CCreatorCombine
{
private:
    CCreatorCombine(void) = delete;
    CCreatorCombine(CCreatorCombine&) = delete;
    CCreatorCombine(const CCreatorCombine&) = delete;
    CCreatorCombine& operator=(const CCreatorCombine&) = delete;
    CCreatorCombine& operator=(CCreatorCombine) = delete;
public:
    explicit CCreatorCombine(T& inou_Ref, const T1& in_sExt, const T1& in_sInt)
    {
        CDirCreator<T, T1>(inou_Ref, in_sExt);
        if(IS_SUCCEEDED(inou_Ref))
            CDirCreator<T, T1>(inou_Ref, in_sExt + '/' + in_sInt);
    }
};

template <typename T, typename T1> class __attribute__ ((visibility("hidden"))) CFRename
{
private:
    CFRename(void) = delete;
    CFRename(CFRename&) = delete;
    CFRename(const CFRename&) = delete;
    CFRename& operator=(const CFRename&) = delete;
    CFRename& operator=(CFRename) = delete;
public:
    explicit CFRename(const T& in_refold, const T& in_refnew, T1& out_ref)
    {
        out_ref = rename(in_refold.c_str(), in_refnew.c_str());
        //ZTRACE("%s::%s %s %s\n", typeid(this).name(), __func__, in_refold.c_str(), in_refnew.c_str());
        if(!IS_SUCCEEDED(out_ref))
            ZTRACE("%s::%s !!! rename %s error %d %s !!!\n", typeid(this).name(), __func__, errno, strerror(errno));
    }
};

//==================================================================================================================== 
//messageGUID="%s"                                                                            messageGUID="%s"
//computer="%s"                                                                               computer="%s"
//messageType="20"                                                                            messageType="20"
//direction="2"                                                                               direction="2"
//attachments="%s"                /media/329C-AE0D/test0fKvjk.txt                             hint="%s"
//fs_local_path="%s"              /tmp/zlock/sc/{019ecdb9-b762-400a-8fc8-c5ba96a79e1f}.txt    fs_local_path="%s"
//attachmentsSize="%d"            10                                                          attachmentsSize="%d"
//date="%s"                       1493383182000                                               date="%s"
//time="%s"                                                                                   time="%s"
//user="%s"                       500                                                         user="%s"
//process="%s"                    Python                                                      process="%s"
//device="%s"                     1Z3N0MHX                                                    device="%s"
//attribs="%s"                    Новая политика 0                                            attribs="%s"
//====================================================================================================================
enum class CONTENTS { NAME, SCNAME, SIZE, DATETIME, USERID, PROCESS, DEVICE, POLICY };
template <class T, class T2> class __attribute__ ((visibility("hidden"))) CMsgParam
{
private:
    CMsgParam(void) = delete;
    CMsgParam(CMsgParam&) = delete;
    CMsgParam(const CMsgParam&) = delete;
    CMsgParam& operator=(const CMsgParam&) = delete;
    CMsgParam& operator=(CMsgParam) = delete;
    enum class METAIDENTS { MGUID, COMPUTER, MTYPE, DIRECTION, TRUTHNAME, INSCNAME, SIZE, DATETIME, USERID, PROCESS, DEVICE, POLICY, META_STRINGS };
    size_t assoc(size_t m)
    {
        if(static_cast<size_t>(METAIDENTS::DEVICE) == m)
            return static_cast<size_t>(CONTENTS::DEVICE);
        if(static_cast<size_t>(METAIDENTS::PROCESS) == m)
            return static_cast<size_t>(CONTENTS::PROCESS);
        if(static_cast<size_t>(METAIDENTS::POLICY) == m)
            return static_cast<size_t>(CONTENTS::POLICY);
        ASSERT(FALSE && "!!! An invalid data ID !!!");
        return -1;
    }
public:
    explicit CMsgParam(const vector<T>& in_vtmpl, const vector<T>& in_vcont, vector<T>& vout)
    {
        for(size_t i = NOTHING; i < static_cast<size_t>(METAIDENTS::META_STRINGS); ++i)
        {
            //ZTRACE("%s::%s %d\n", typeid(this).name(), __func__, (int)i);
            T s;
            if(static_cast<size_t>(METAIDENTS::MGUID) == i)
            {
                T2 tmp;
                CUuidStr<T, T>(in_vcont[i + 1], s);
                tmp.Format(in_vtmpl[i].c_str(), s.c_str());
                s = tmp.Get_Data();
            }
            else if(static_cast<size_t>(METAIDENTS::COMPUTER) == i)
            {
                T2 tmp, tmp1;
                GetHostName(tmp);
                tmp1.Format(in_vtmpl[i].c_str(), (const char*)tmp);
                s = tmp1.Get_Data();
            }
            else if(static_cast<size_t>(METAIDENTS::MTYPE) == i || static_cast<size_t>(METAIDENTS::DIRECTION) == i)
                s = in_vtmpl[i].c_str();
            else if(static_cast<size_t>(METAIDENTS::TRUTHNAME) == i)
            {
                if(is_Notify(in_vtmpl))
                {
                    int n = atoi(in_vcont[i].c_str());
                    ASSERT(NOTHING < n && "!!! An invalid message identifier !!!");
                    T2 tmp(get_LocalString(n, GetCurrentLanguage())), tmp1;
                    ASSERT(!tmp.Is_Empty() && "!!! An invalid message !!!");
                    tmp.Replace("%1", "%s");
                    tmp1.Format(tmp.Get_Data(), in_vcont[static_cast<size_t>(CONTENTS::SCNAME)].c_str());
                    s = tmp1.Get_Data();
                }
                else
                {
                    T ss;
                    CGetName<T>(in_vcont[static_cast<size_t>(CONTENTS::NAME)], ss);
                    ASSERT(!ss.empty() && "!!! An incorrect file name !!!");
                    T2 tmp;
                    tmp.Format(in_vtmpl[i].c_str(), ss.c_str());
                    s = tmp.Get_Data();
                }
            }
            else if(static_cast<size_t>(METAIDENTS::INSCNAME) == i)
            {
                T2 tmp;
                tmp.Format(in_vtmpl[i].c_str(), in_vcont[static_cast<size_t>(CONTENTS::NAME)].c_str());
                s = tmp.Get_Data();
            }
            else if(static_cast<size_t>(METAIDENTS::SIZE) == i)
            {
                T2 tmp;
                //ZTRACE("%s::%s METAIDENTS::SIZE %s %s\n", typeid(this).name(), __func__, in_vtmpl[i].c_str(), in_vcont[static_cast<size_t>(CONTENTS::SIZE)].c_str());
                tmp.Format(in_vtmpl[i].c_str(), in_vcont[static_cast<size_t>(CONTENTS::SIZE)].c_str());
                s = tmp.Get_Data();
            }
            else if(static_cast<size_t>(METAIDENTS::DATETIME) == i)
            {
                T2 tmp1, tmp2, tmp3;
                //ZTRACE("%s::%s METAIDENTS::DATETIME [%d] %s %s\n", typeid(this).name(), __func__, i, in_vcont[static_cast<size_t>(CONTENTS::DATETIME)].c_str());
                time_t tmt = strtoull(in_vcont[static_cast<size_t>(CONTENTS::DATETIME)].c_str(), NULL, 10);
                GetDateTime(tmp1, tmp2, tmt);
                //ZTRACE("%s::%s METAIDENTS::DATETIME %s %llu %s %s\n", typeid(this).name(), __func__, in_vcont[static_cast<size_t>(CONTENTS::DATETIME)].c_str(),
                    //(unsigned long long)tmt, (const char*)tmp1, (const char*)tmp2);
                tmp3.Format(in_vtmpl[i].c_str(), tmp1.Get_Data());
                s = tmp3.Get_Data();
                ASSERT(!s.empty() && "!!! An invalid data !!!");
                vout.push_back(s);
                tmp3.Format(in_vtmpl[i + 1].c_str(), tmp2.Get_Data());
                s = tmp3.Get_Data();
                ASSERT(!s.empty() && "!!! An invalid data !!!");
                vout.push_back(s);
                continue;
            }
            // TODO: other user is online, previously saved entities handling
            else if(static_cast<size_t>(METAIDENTS::USERID) == i)
            {
                T2 tmp, tmp1;
                GetUser(tmp);
                tmp1.Format(in_vtmpl[i + 1].c_str(), tmp.Get_Data());
                s = tmp1.Get_Data();
                //ZTRACE("%s::%s METAIDENTS::USERID [%d] %s\n", typeid(this).name(), __func__, (int)i, (const char*)tmp1);
            }
            else if(static_cast<size_t>(METAIDENTS::PROCESS) == i || static_cast<size_t>(METAIDENTS::DEVICE) == i || static_cast<size_t>(METAIDENTS::POLICY) == i)
            {
                T2 tmp;
                tmp.Format(in_vtmpl[i + 1].c_str(), in_vcont[assoc(i)].c_str());
                s = tmp.Get_Data();
                if(static_cast<size_t>(METAIDENTS::POLICY) == i)
                    s += ">";
            }
            ASSERT(!s.empty() && "!!! An invalid data !!!");
            vout.push_back(s);
        }
    }
};

template <class T, class T1> class __attribute__ ((visibility("hidden"))) CFReader
{
private:
    CFReader(void) = delete;
    CFReader(CFReader&) = delete;
    CFReader(const CFReader&) = delete;
    CFReader& operator=(const CFReader&) = delete;
    CFReader& operator=(CFReader) = delete;
public:
    explicit CFReader(const T& in_refName, vector<T>& outv, T1& out_res)
    {
        out_res = GetStreamContents(in_refName, outv, false);
        if(!IS_SUCCEEDED(out_res) || outv.empty())
            ZTRACE("%s !!! Can't read file %s !!!\n", __func__, in_refName.c_str());
    }
};

template <typename T> class __attribute__ ((visibility("hidden"))) CJoin
{
private:
    CJoin(void) = delete;
    CJoin(CJoin&) = delete;
    CJoin(const CJoin&) = delete;
    CJoin& operator=(const CJoin&) = delete;
    CJoin& operator=(CJoin) = delete;
public:
    explicit CJoin(const vector<T>& inv, T& sum)
    {
        sum = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n";
        sum += "<Parameters\n";
        for(size_t i = NOTHING; i < inv.size(); ++i)
        {
            sum += inv[i] + '\n';
        }
        sum += "</Parameters>\n";
    }
};

// for notifications only, minizip -9 ./{GUID}.zip ./{GUID}/.MessageParameters
template <typename T, typename T1> class __attribute__ ((visibility("hidden"))) CCompressSimple
{
private:
    CCompressSimple(void) = delete;
    CCompressSimple(CCompressSimple&) = delete;
    CCompressSimple(const CCompressSimple&) = delete;
    CCompressSimple& operator=(const CCompressSimple&) = delete;
    CCompressSimple& operator=(CCompressSimple) = delete;
public:
    explicit CCompressSimple(const T& in_sPath, T1& out_ref)
    {
        do
        {
            // TODO: Temporary objects must be moved to the stack local variables
            char* arg_vec[MINARGS] = {NULL, NULL, NULL, NULL};
            T guid, b_path;
            if(!get_WD(b_path))
                ERR(out_ref, -1);
            out_ref = chg_WD(STORPATH);
            if(!IS_SUCCEEDED(out_ref))
                break;
            ASSERT(!b_path.empty() && "!!! An invalid path !!!");
            CUuidStr<T, T1>(in_sPath, guid);
            arg_vec[0] = const_cast<char*>("minizip");
            arg_vec[1] = const_cast<char*>("-9");
            arg_vec[2] = (T("./") + guid + ".zip").c_str();
            arg_vec[3] = (T("./") + guid + T("/.MessageParameters")).c_str();
            out_ref = ZipCompress(MINARGS, arg_vec);
            if(!IS_SUCCEEDED(out_ref))
                ZTRACE("%s !!! Can't compress file !!!\n", __func__);
            out_ref = chg_WD(b_path);
        } while(false);
    }
};

template <typename T, typename T2> class __attribute__ ((visibility("hidden"))) CTabber
{
private:
    CTabber(void) = delete;
    CTabber(CTabber&) = delete;
    CTabber(const CTabber&) = delete;
    CTabber& operator=(const CTabber&) = delete;
    CTabber& operator=(CTabber) = delete;
public:
    explicit CTabber(vector<T>& iniu_v, T2& inou_refFake)
    {
        for(unsigned i = 0; i < iniu_v.size(); ++i)
            iniu_v[i] = T("\t") + iniu_v[i];
    }
};

// attachments, minizip -9 {GUID}.zip ./{GUID}/.MessageParameters ./{GUID}/Attachments/file
template <typename T, typename T1> class __attribute__ ((visibility("hidden"))) CCompressCombo
{
private:
    CCompressCombo(void) = delete;
    CCompressCombo(CCompressCombo&) = delete;
    CCompressCombo(const CCompressCombo&) = delete;
    CCompressCombo& operator=(const CCompressCombo&) = delete;
    CCompressCombo& operator=(CCompressCombo) = delete;
public:
    explicit CCompressCombo(const T& in_sPath, const T& in_sFile, T1& out_ref)
    {
        do
        {
            char* arg_vec[MAXARGS] = {NULL, NULL, NULL, NULL, NULL};
            T guid, b_path, name, trg, mpr, spr;
            if(!get_WD(b_path))
                ERR(out_ref, -1);
            out_ref = chg_WD(STORPATH);
            if(!IS_SUCCEEDED(out_ref))
                break;
            ASSERT(!b_path.empty() && "!!! An invalid path !!!");
            CUuidStr<T, T>(in_sPath, guid);
            arg_vec[0] = const_cast<char*>("minizip");
            arg_vec[1] = const_cast<char*>("-9");
            trg = T("./") + guid + ".zip";
            arg_vec[2] = const_cast<char*>(trg.c_str());
            mpr = T("./") + guid + "/.MessageParameters";
            arg_vec[3] = const_cast<char*>(mpr.c_str());
            CGetName<T>(in_sFile, name);
            ASSERT(!name.empty() && "!!! An invalid file name !!!");
            spr = T("./") + guid + "/"ATTACH"/" + name;
            arg_vec[4] = const_cast<char*>(spr.c_str());
            //ZTRACE("%s::%s %s %s %s %s %s\n", typeid(this).name(), __func__, arg_vec[0], arg_vec[1], arg_vec[2], arg_vec[3], arg_vec[4]);
            out_ref = ZipCompress(MAXARGS, arg_vec);
            if(!IS_SUCCEEDED(out_ref))
                ZTRACE("%s !!! Can't compress file !!!\n", __func__);
            out_ref = chg_WD(b_path);
        } while(false);
    }
};

// this instance for the attachment only
class __attribute__ ((visibility("hidden"))) CFPoster
{
private:
    CFPoster(void) = delete;
    CFPoster(CFPoster&) = delete;
    CFPoster(const CFPoster&) = delete;
    CFPoster& operator=(const CFPoster&) = delete;
    CFPoster& operator=(CFPoster) = delete;
    void clean(const string& in_sGuid, const string& in_sMeta) const
    {
        if(!in_sGuid.empty())
            unlink((string(STORPATH) + in_sGuid + ".zip").c_str());
        unlink(in_sMeta.c_str());
    }
public:
    explicit CFPoster(const string& in_sFname, const vector<string>& in_Vs, const ZLib_StringA& in_sUrl)
    {
        int r = OK;
        string guid, orig_name;
        ZLib_StringA sc, stmp;
        vector<string> metacontv, msgparcontv;
        do
        {
            LOG("%s::%s %s %s\n", typeid(this).name(), __func__, in_sFname.c_str(), (const char*)in_sUrl);
            // meta file reading
            CFReader<string, int>(in_sFname, metacontv, r);
            LOG("%s::%s file read %d\n", typeid(this).name(), __func__, (int)metacontv.size());
            if(!IS_SUCCEEDED(r) || static_cast<size_t>(CONTENTS::POLICY) > metacontv.size())
            {
                LOG("%s::%s !!! File %s reading error !!!\n", typeid(this).name(), __func__, in_sFname.c_str());
                break;
            }
            CUuidStr<string, string>(metacontv[static_cast<size_t>(CONTENTS::SCNAME)], guid);
            LOG("%s::%s %s\n", typeid(this).name(), __func__, guid.c_str());
            ASSERT(!giud.empty() && "!!! An invalid GUID !!!");
            // There are /tmp/{GUID}/ and /tmp/{GUID}/Attachments/ directories making
            CCreatorCombine<int, string>(r, guid, ATTACH);
            if(!IS_SUCCEEDED(r))
            {
                LOG("%s::%s !!! Make directories error !!!\n", typeid(this).name(), __func__);
                break;
            }
            CGetName<string>(metacontv[static_cast<size_t>(CONTENTS::NAME)], orig_name);
            LOG("%s::%s %s\n", typeid(this).name(), __func__, orig_name.c_str());
            ASSERT(!orig_name.empty() && "!!! An invalid original file name !!!");
            // renaming to the original name with a moving to the previously created folder
            CFRename<string, int>(metacontv[static_cast<size_t>(CONTENTS::SCNAME)], string(STORPATH) + guid + '/' + ATTACH + '/' + orig_name, r);
            if(!IS_SUCCEEDED(r))
            {
                LOG("%s::%s !!! File %s renaming error %d %s !!!\n", typeid(this).name(), __func__, metacontv[static_cast<size_t>(CONTENTS::SCNAME)].c_str(), 
                    errno, strerror(errno));
                break;
            }
            // .MessageParameters file contents generation
            CMsgParam<string, ZLib_StringA>(in_Vs, metacontv, msgparcontv);
            LOG("%s::%s .MsgParams conversion strings %d\n", typeid(this).name(), __func__, (int)msgparcontv.size());
            ASSERT(!msgparcontv.empty() && "!!! An invalid .MessageParameters contents !!!");
            // joining a vector of strings into the single entity and XML contents get
            CJoin<string>(msgparcontv, orig_name);
            ASSERT(!orig_name.empty() && "!!! An invalid joined .MessageParameters contents !!!");
            // The /tmp/{GUID}/.MessageParameters file writing
            if(!IS_SUCCEEDED(write_In(string(STORPATH) + guid + "/.MessageParameters", orig_name)))
            {
                LOG("%s::%s !!! .MessageParameters file writiong error %d %s !!!\n", typeid(this).name(), __func__, errno, strerror(errno));
                break;
            }
            // /tmp/{GUID}/.MessageParameters and /tmp/{GUID}/Attachments/file compression to the /tmp/{GUID}.zip
            CCompressCombo<string, int>(metacontv[static_cast<size_t>(CONTENTS::SCNAME)], metacontv[static_cast<size_t>(CONTENTS::NAME)], r);
            if(!IS_SUCCEEDED(r))
            {
                LOG("%s::%s !!! The compression of the %s%s.zip error !!!\n", typeid(this).name(), __func__, STORPATH, guid.c_str());
                break;
            }
            get_Contents(string(STORPATH) + guid + ".zip", sc);
            ASSERT(!sc.Is_Empty() && "!!! An incorrect compressed file !!!");
            stmp.Format(MSGBODY, (unsigned)sc.Get_Length(), (const char*)in_sUrl);
            stmp = stmp + sc;
            sc = HTOK;
            sc += in_sUrl;
            if(NULL == strchr(in_sUrl, ':'))
                sc += PORT_TOKEN;
            send_Request(sc, stmp);
        } while (false);
        clean(guid, in_sFname);
        Clean(ZLib_StringA(STORPATH) + guid.c_str());
    }
};

void* ZLCK_Strategy_Apply(const ZLCK_TLS_WRAP_p_s in_pWrap)
{
    fd_set rfds;
    BYTE j = false;
    int r = NOTHING;
    vector<string> vestr;
    struct timeval tv = {0};
    ZLibT_Array<ZLib_StringA> addr;
    ZLCK_SVC_DATA_s* pData = (ZLCK_SVC_DATA_s*)in_pWrap->pvTls;
    ZLCK_SC_REM_STS_p_s pSts = (ZLCK_SC_REM_STS_p_s)&pData->aszData[OK];

    do
    {
        LOG("%s ++\n", __func__);
        ASSERT(NULL != in_pvData && "!!! An invalid data !!!");
        r = pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL); // cancel immediately
        if(!IS_SUCCEEDED(r))
        {
            ZTRACE("%s !!! pthread_setcanceltype error %d %s\n", __func__, r, strerror(r));
            break;
        }
        if(!GetFONSrvs(addr, pSts->szLogServer, j, (const char*)GetTLS(GET_CONFIG)->aszData))
        {
            ZTRACE("%s !!! Can't obtain a servers list !!!\n", __func__);
            break;
        }
        ZTRACE("%s servers %d\n", __func__, (int)addr.Get_Num_Elements());
        ASSERT(NOTHING < addr.Get_Num_Elements() && "!!! An invalid server addresses !!!");
        CFReader<string, int>(in_pWrap->pszStrategy, vestr, r);
        ZTRACE("%s reading strategy %s %d\n", __func__, in_pWrap->pszStrategy, (int)vestr.size());
        if(!IS_SUCCEEDED(r) || vestr.empty())
            break;
        ZTRACE("%s %s\n", __func__, in_pWrap->pszPath);
        CTabber<string, int>(vestr, r);
        for(;;)
        {
            string sFile;
            if(!IS_SUCCEEDED(get_SCFileName(in_pWrap->pszPath, sFile)))
                break;
            if(!sFile.empty())
            {
                for(unsigned i = NOTHING; i < addr.Get_Num_Elements(); ++i)
                    CFPoster(sFile, vestr, GetCfgIPAddress(addr.Get_At(i)));
            }
            FD_ZERO(&rfds);
            FD_SET(pData->sock[READ], &rfds);
            // once per 5 secunds
            tv.tv_sec = TM_WT_NO_FS_SC;
            tv.tv_usec = NOTHING;
            r = select(pData->sock[READ] + 1, &rfds, NULL, NULL, &tv);
            if(SLEEP == r)
            {
                // end of job identifier
                if(NOTHING < pData->ubyEOJ)
                {
                    ZTRACE("%s Job is completed\n", __func__);
                    close(pData->sock[READ]);
                    close(pData->sock[WRITE]);
                    pData->sock[READ] = pData->sock[WRITE] = NOTHING;
                    pData->mself = NOTHING;
                    break;
                }
            }
        }
    } while (false);
    if(!vestr.empty())
        vestr.clear();
    if(!is_Empty(addr))
        addr.Clear();
    LOG("%s --\n", __func__);
    return NULL;
}

void* ZLCK_SVCM_Server_SC_Handler(void* in_pvData)
{
    SVCCONFIG cfg = {0};
    ZLCK_TLS_WRAP_s wrp = {0};
    int r = ReadCfg(false, &cfg);
    ASSERT(IS_SUCCEEDED(r) && "!!! An inconsistent configuration state !!!");
    ASSERT(NULL != cfg.pszScStrategy && "!!! A Shadow Copy strategy don't assigned !!!");
    wrp.pszStrategy = cfg.pszScStrategy;
    string s = GetSCLocalPath();
    wrp.pszPath = const_cast<char*>(s.c_str());
    //ZTRACE("%s %s %s\n", __func__, GetSCLocalPath().c_str(), wrp.pszPath);
    wrp.pvTls = in_pvData;
    return ZLCK_Strategy_Apply(&wrp);
}