// modldr.cpp
// Author: oleshii
// Created on Wen Dec 21 08:45:44 MSK 2016
// Copyright (c) zecurion. All rights reserved.
// The driver (un)loader

#include <unistd.h>
#include <stdio.h>
#include <assert.h>
#include <errno.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <typeinfo>
#include <string>
using std::string;

#if  1//DEBUG
#define ASSERT assert
#else
#define ASSERT
#endif

#include "../../MAC/zlib/stdafx.h"
#include "../../MAC/zlib/zlib.h"
#include "../../MAC/zlib/zlib_stringa.h"
#include "../zlock/dmacro.h"

#define LOG ZTRACE

ZLib_StringA GetFileContent(const ZLib_StringA& in_szFname);
extern "C" long init_module(void*, unsigned long, const char*);
extern "C" int delete_module(const char *name);
// extern "C" long delete_module(const char *, unsigned int); O_EXCL in concrete, manual sayd single parameter about, source archive assigns two

enum class LACTIONS { LOAD, UNLOAD };

class __attribute__ ((visibility("hidden"))) CModLdr
{
private:
    CModLdr(void) = delete;
    CModLdr(CModLdr&) = delete;
    CModLdr(const CModLdr&) = delete;
    CModLdr& operator=(const CModLdr&) = delete;
    CModLdr& operator=(CModLdr) = delete;
private:
    bool isLoaded(string& in_s) const
    {
        int fd = NOTHING;
        do
        {
            fd = open(in_s.c_str(), O_RDWR);
            if(NOTHING > fd)
                LOG("%s::%s !!! open error %d %s !!!\n", typeid(this).name(), __func__, errno, strerror(errno));
            else
                LOG("%s::%s the module %s loaded already\n", typeid(this).name(), __func__, in_s.c_str());
        } while (false);
        if(NOTHING < fd)
            close(fd);
        return NOTHING < fd;
    }
    int load(string& in_sMpath) const
    {
        int r = OK;
        ZLib_StringA scont, sopt;
        do
        {
            //sopt = in_sMpath.c_str();
            scont = GetFileContent(ZLib_StringA(in_sMpath.c_str()));
            if(scont.Is_Empty())
                ERR(r, EIO);
            sopt.Empty();
            r = (int)init_module((void*)scont.Get_Data(), scont.Get_Length(), sopt.Get_Data());
            if(!IS_SUCCEEDED(r))
                LOG("%s::%s !!! init_module %s error %d %s !!!\n", typeid(this).name(), __func__, in_sMpath.c_str(), errno, strerror(errno));
            else
                LOG("%s::%s init_module %s OK\n", typeid(this).name(), __func__, in_sMpath.c_str());
        } while(false);
        return r;
    }
public:
    explicit CModLdr(string in_sMpath, string in_sLink, int& inou_refRet, LACTIONS en)
    {
        do
        {
            if(in_sMpath.empty() || in_sLink.empty())
                ERR(inou_refRet, EINVAL);
            bool b = isLoaded(in_sLink);
            if(LACTIONS::LOAD == en)
            {
                if(b)
                    RET(inou_refRet, OK);
                inou_refRet = load(in_sMpath);
            }
            else if(LACTIONS::UNLOAD == en)
            {
                if(b)
                    inou_refRet = delete_module(in_sMpath.c_str());
            }
        } while (false);
    }
};

int LoadModule(const char* in_pszPath, const char* in_pszBsdLnk)
{
    int ret = OK;
    CModLdr(in_pszPath, in_pszBsdLnk, ret, LACTIONS::LOAD);
    return ret;
}

int UnloadModule(const char* in_pszPath, const char* in_pszBsdLnk)
{
    int ret = OK;
    CModLdr(in_pszPath, in_pszBsdLnk, ret, LACTIONS::UNLOAD);
    return ret;
}