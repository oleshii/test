#include <linux/errno.h>
#include <linux/slab.h>
#include <linux/file.h>
#include <linux/sched.h>
#include "sc_utils.h"
#include "sc_types.h"

static struct mutex gs_Lock = __MUTEX_INITIALIZER(gs_Lock);

static LIST_HEAD(gs_List);

void Zsck_Storage_Init(void)
{
    INIT_LIST_HEAD(&gs_List);
}

#define LOCK(m)     mutex_lock(m)
#define UNLOCK(m)   mutex_unlock(m)

// reference the volume
static void zsck_Vol_Ref(PZLFS entry)
{
    __sync_fetch_and_add(&entry->m_RefCnt, 1);
}

// reference the file
static void zsck_File_Ref(PZLFSE entry)
{
    __sync_fetch_and_add(&entry->m_Cnt, 1);
}

static void zsck_File_Release(PZLFSE entry, PZLFS vol, bool in_bLock)
{
    if(likely(entry) && likely(vol))
    {
        // the locking done by caller
        if(likely(in_bLock))
            LOCK(&vol->m_Lock);
        list_del(&entry->m_Link);
        __sync_fetch_and_sub(&vol->m_Cnt, 1);
        if(likely(in_bLock))
            UNLOCK(&vol->m_Lock);
        LOG("%s::%s file %s will be released finaly\n", TAG, __func__, entry->pszName);
        if(NULL != entry->pszName)
            KFREE(entry->pszName);
        KFREE(entry);
    }
}

// unreference the file
static void zsck_File_Unref(PZLFS vol, PZLFSE entry)
{
    __sync_fetch_and_sub(&entry->m_Cnt, 1);
    // the release of the file entry have to be placed here
    if(NOTHING == entry->m_Cnt)
        zsck_File_Release(entry, vol, true);
}

// release the volume
static void zsck_Vol_Release(PZLFS entry)
{
    do
    {
        if(likely(entry))
        {
            LOCK(&gs_Lock);
            list_del(&entry->m_Lnk);
            UNLOCK(&gs_Lock);
            KFREE(entry);
        }
    } while (false);
}

static void zsck_Flist_Release(PZLFS vol)
{
    struct list_head* plist = NULL;
    PZLFSE file = NULL;
    do
    {
        LOCK(&vol->m_Lock);
        while(!list_empty(&vol->m_List))
        {
            plist = vol->m_List.next;
            file = list_entry(plist, typeof(*file), m_Link);
            zsck_File_Release(file, vol, false);
        }
        UNLOCK(&vol->m_Lock);
    } while (false);
}

// dereference the volume
static void zsck_Vol_Unref(PZLFS entry)
{
    __sync_fetch_and_sub(&entry->m_RefCnt, 1);
    if(NOTHING == entry->m_RefCnt)
    {
        LOG("%s::%s The volume %s will be released finaly\n", TAG, __func__, entry->m_szMntPt);
        zsck_Flist_Release(entry);
        zsck_Vol_Release(entry);
    }
}

static inline bool zsck_Str_Cpy(char* dst, size_t dstLen, const char* src)
{
    size_t len = strlen(src);
    if(len > dstLen)
        return false;
    memcpy(dst, src, len);
    return true;
}

static int zsck_Init_Volume(PZLFS entry, const char* pszMnt, const char* pszBsd, const char* pszDevName, const char* pszPolName, 
    unsigned char uSc, unsigned char uFt, struct vfsmount* mnt)
{
    int r = OK;
    do
    {
        if(!zsck_Str_Cpy(&entry->m_szMntPt[OK], sizeof(entry->m_szMntPt), pszMnt))
            ERR(r, -ERANGE);
        if(!zsck_Str_Cpy(&entry->m_szBsd[OK], sizeof(entry->m_szBsd), pszBsd))
            ERR(r, -ERANGE);
        if(!zsck_Str_Cpy(&entry->m_szDeviceName[OK], sizeof(entry->m_szDeviceName), pszDevName))
            ERR(r, -ERANGE);
        if(!zsck_Str_Cpy(&entry->m_szPolName[OK], sizeof(entry->m_szPolName), pszPolName))
            ERR(r, -ERANGE);
        entry->bsc = uSc;
        entry->bfo = uFt;
        entry->m_MntPoint = mnt;
        INIT_LIST_HEAD(&entry->m_List);
        INIT_LIST_HEAD(&entry->m_Lnk);
        mutex_init(&entry->m_Lock);
        // on create we'll reference it
        zsck_Vol_Ref(entry);
    } while (false);
    return r;
}

static void zsck_Vol_Add(const PZLFS entry)
{
    LOCK(&gs_Lock);
    list_add_tail(&entry->m_Lnk, &gs_List);
    UNLOCK(&gs_Lock);
}

static bool zsck_Is_Data_Valid(const char* pszMnt, const char* pszBsd, const char* pszDevName, const char* pszPolName, unsigned char uSc, unsigned char uFt,
    struct vfsmount* mnt)
{
    do
    {
        if(!zsck_Is_Char_Ptr_Valid(pszMnt))
            break;
        if(!zsck_Is_Char_Ptr_Valid(pszBsd))
            break;
        if(!zsck_Is_Char_Ptr_Valid(pszDevName))
            break;
        if(!zsck_Is_Char_Ptr_Valid(pszPolName))
            break;
        if(!zsck_Is_Ptr_Valid(mnt))
            break;
        return NOTHING < (uFt | uSc);
    } while(false);
    return false;
}

int Zsck_Vol_Add(const char* pszMnt, const char* pszBsd, const char* pszDevName, const char* pszPolName, unsigned char uSc, unsigned char uFt,
    struct vfsmount* mnt)
{
    int r = OK;
    PZLFS entry = NULL;
    do
    {
        if(!zsck_Is_Data_Valid(pszMnt, pszBsd, pszDevName, pszPolName, uSc, uFt, mnt))
            ERR(r, -EINVAL);
        entry = KALLOC(sizeof(ZLFS));
        if(!likely(entry))
            ERRXD(TAG, "!!! memory allocation error !!!\n", r, -ENOMEM);
        if(!IS_SUCCEEDED(zsck_Init_Volume(entry, pszMnt, pszBsd, pszDevName, pszPolName, uSc, uFt, mnt)))
            ERRXD(TAG, "!!! zsck_Init_Volume error !!!\n", r, -EIO);
        zsck_Vol_Add(entry);
    } while (false);
    if(!IS_SUCCEEDED(r))
    {
        if(likely(entry))
            KFREE(entry);
    }
    return r;
}

void Zsck_Vol_Put(PZLFS entry) { return zsck_Vol_Unref(entry); }

// returns the referenced entry of the volume
PZLFS Zsck_Vol_Get_By_Mnt(struct vfsmount* mnt)
{
    struct list_head* plist = NULL;
    PZLFS entry = NULL, tmp = NULL;
    if(likely(mnt))
    {
        LOCK(&gs_Lock);
        if(!list_empty(&gs_List))
        {
            list_for_each(plist, &gs_List)
            {
                tmp = list_entry(plist, typeof(*tmp), m_Lnk);
                if(tmp->m_MntPoint == mnt)
                {
                    entry = tmp;
                    zsck_Vol_Ref(entry);
                    break;
                }
            }
        }
        UNLOCK(&gs_Lock);
    }
    return entry;
}

// returns the referenced entry of the volume
PZLFS Zsck_Vol_Get_By_Name(const char* in_pszName, int in_Id)
{
    struct list_head* plist = NULL;
    PZLFS entry = NULL, tmp = NULL;
    if(zsck_Is_Char_Ptr_Valid(in_pszName))
    {
        LOCK(&gs_Lock);
        if(!list_empty(&gs_List))
        {
            list_for_each(plist, &gs_List)
            {
                tmp = list_entry(plist, typeof(*tmp), m_Lnk);
                if(((MNT == in_Id) && !strcmp(tmp->m_szMntPt, in_pszName)) ||
                    ((BSD == in_Id) && !strcmp(tmp->m_szBsd, in_pszName)))
                {
                    entry = tmp;
                    zsck_Vol_Ref(entry);
                    break;
                }
            }
        }
        UNLOCK(&gs_Lock);
    }
    return entry;
}

// the referenced file entry will be returned to
PZLFSE Zsck_Get_File_By_File(const PZLFS entry, const struct file* in_pFile, const struct task_struct* in_pTask)
{
    PZLFSE to_ret = NULL;
    struct list_head* list = NULL;    
    if(zsck_Is_Ptr_Valid(entry) && zsck_Is_Ptr_Valid(in_pFile))
    {
        LOCK(&entry->m_Lock);
        if(!list_empty(&entry->m_List))
        {
            list_for_each(list, &entry->m_List)
            {
                PZLFSE fe = list_entry(list, typeof(*fe), m_Link);
                if(fe->m_pNode == in_pFile && fe->m_pProc == in_pTask)
                    RET(to_ret, fe);
            }
            if(zsck_Is_Ptr_Valid(to_ret))
                zsck_File_Ref(to_ret);
        }
        UNLOCK(&entry->m_Lock);
    }
    return to_ret;
}

static bool zsck_Is_Valid_Data(PZLFS entry, struct file* in_pFile, int fd, const char* in_pszFileName, struct task_struct* in_pTask)
{
    do
    {
        if(!zsck_Is_Char_Ptr_Valid(in_pszFileName))
            break;
        if(!zsck_Is_Ptr_Valid(entry))
            break;
        if(!zsck_Is_Ptr_Valid(in_pTask))
            break;
        if(!zsck_Is_Ptr_Valid(in_pFile))
            break;
        return fd > NOTHING;
    } while(false);
    return false;
}

static bool zsck_Allocate_File_Entry(PZLFSE* entry, size_t in_NameLen)
{
    *entry = KALLOC(sizeof(ZLFSE));
    if(!likely(*entry))
        return false;
    (*entry)->pszName = KALLOC(in_NameLen);
    if(!likely((*entry)->pszName))
    {
        KFREE(*entry);
        *entry = NULL;
    }
    return likely(*entry) && likely((*entry)->pszName);
}

static void zsck_Init_File_Entry(PZLFSE entry, struct file* in_pFile, int fd, const char* in_pszFileName, struct task_struct* in_pTask, uid_t in_Uid, size_t in_Sz)
{
    entry->m_Fd = fd;
    entry->m_pNode = in_pFile;
    entry->m_UsrUid = in_Uid;
    entry->m_Length = in_Sz;
    memcpy(entry->pszName, in_pszFileName, strlen(in_pszFileName));
    entry->m_pProc = in_pTask;
    memcpy(entry->szProc, in_pTask->comm, sizeof(entry->szProc));
    INIT_LIST_HEAD(&entry->m_Link);
    zsck_File_Ref(entry);
}

static void zsck_Ins_File(PZLFS vol, PZLFSE file)
{
    LOCK(&vol->m_Lock);
    list_add_tail(&file->m_Link, &vol->m_List);
    __sync_fetch_and_add(&vol->m_Cnt, 1);
    UNLOCK(&vol->m_Lock);
}

int Zsck_Add_File(PZLFS entry, struct file* in_pFile, int fd, const char* in_pszFileName, struct task_struct* in_pTask, uid_t in_Uid, size_t in_Sz)
{
    int r = OK;
    PZLFSE lfse = NULL;
    do
    {
        if(!zsck_Is_Valid_Data(entry, in_pFile, fd, in_pszFileName, in_pTask))
            ERR(r, -EIO);
        if(!zsck_Allocate_File_Entry(&lfse, strlen(in_pszFileName) + 1))
            ERRXD(TAG, "!!! memory allocation error !!!\n", r, -ENOMEM);
        zsck_Init_File_Entry(lfse, in_pFile, fd, in_pszFileName, in_pTask, in_Uid, in_Sz);
        zsck_Ins_File(entry, lfse);
    } while(false);
    return r;
}

void Zsck_Put_File(PZLFS vol, PZLFSE file) { return zsck_File_Unref(vol, file); }

void Zsck_Release_All(void)
{
    PZLFS tmp = NULL;
    struct list_head* plist = NULL;
    LOCK(&gs_Lock);
    while(!list_empty(&gs_List))
    {
        plist = gs_List.next;
        tmp = list_entry(plist, typeof(*tmp), m_Lnk);
        list_del(&tmp->m_Lnk);
        zsck_Flist_Release(tmp);
        KFREE(tmp);
    }
    UNLOCK(&gs_Lock);
}

bool Zsck_Is_Storage_Empty(void)
{
    bool b;
    LOCK(&gs_Lock);
    b = list_empty(&gs_List);
    UNLOCK(&gs_Lock);
    return b;
}