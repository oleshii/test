#include <linux/init.h>
#include <linux/module.h>
#include <linux/notifier.h>
#include <linux/usb.h>
#include <linux/kallsyms.h>
#include <linux/errno.h>
#include <linux/slab.h>
#include <linux/usb/hcd.h>
#include <linux/genhd.h>
#include <linux/kdev_t.h>
#include "debug.h"

MODULE_LICENSE("GPL");
MODULE_VERSION("1.0.0");
MODULE_DESCRIPTION("USB hook zecurion driver");
MODULE_AUTHOR("Oleg Nikitenko <nikitenko@securit.ru>");

#define MOUSE   "USB Mouse"
#define HUB     "USB Hub"
#define DEV     "/dev/"
#define MNTTAB  "/etc/mtab"
#define DEVDIR  "/dev/.udev/links/"
#define MNTPART "-part"
#define LNKPART "by-id"
#define TRAILS   0x10

// warning: hex escape sequence out of range
static const char* apsNnDevs[] = 
{
    MOUSE, 
    HUB
};

// forward global declarations
int Zlck_storage_add(void const* in_pUsbDev);    
void Zlck_storage_deref(struct ZLCK_DEV_STOR_s* entry);
#define DEREF   Zlck_storage_deref

inline static int zlck_usb_is_device_necessary(struct usb_device const* in_pUsbDev)
{
    int i;
    for(i = 0; i < ARRAY_SIZE(apsNnDevs); i++)
    {
        if(NULL != in_pUsbDev->product)
        {
            if(strstr(in_pUsbDev->product, apsNnDevs[i]))
                return FALSE;
        }
    }
    return TRUE;
}

// Some things weren't bound directly, only by address from the kallsyms
unsigned long Zlck_sym_resolve(const char* in_pszSym)
{
    return kallsyms_lookup_name(in_pszSym);
}

static int zlck_usb_dev_display(const char* in_ps, struct usb_device const* in_pDev)
{
    int r = OK;
    do
    {
        if(NULL == in_pDev)
        {
            r -= EIO;
            break;
        }
        LOG("%s::%s %s %s %s %s %s %s\n", TAG, __func__, in_ps, dev_name(&in_pDev->dev),
            0 < in_pDev->devpath[0] ? &in_pDev->devpath[0] : "NULL",
            NULL != in_pDev->product ? in_pDev->product : "NULL", 
            NULL != in_pDev->manufacturer ? in_pDev->manufacturer : "NULL", 
            NULL != in_pDev->serial ? in_pDev->serial : "NULL");
    } while (FALSE);
    return r;
}

int Zlck_fill_USB_device_info(ZLCK_DRV_DEVICE_DATA_p_s inou_pData, void const* in_pvDev)
{
    int r = OK;
    struct usb_device const* in_pDev = (struct usb_device const*)in_pvDev;
    do
    {
        if(NULL == inou_pData || NULL == in_pDev)
        {
            r -= EINVAL;
            LOG("%s::%s !!! Invalid data !!!\n", TAG, __func__);
            break;
        }
        inou_pData->bClass = in_pDev->descriptor.bDeviceClass;
        inou_pData->bProtocol = in_pDev->descriptor.bDeviceProtocol;
        inou_pData->idVendor = in_pDev->descriptor.idVendor;
        inou_pData->idProduct = in_pDev->descriptor.idProduct;
        inou_pData->bSubClass = in_pDev->descriptor.bDeviceSubClass;
        LOG("%s::%s Configuring device from Manufacturer %x Product %x SerialNumber %x\n", TAG, __func__, in_pDev->descriptor.iManufacturer, in_pDev->descriptor.iProduct, 
            in_pDev->descriptor.iSerialNumber);
        if(NULL == in_pDev->config)
        {
            r -= EIO;
            LOG("%s::%s !!! No configuartion for device %llx !!!\n", TAG, __func__, (unsigned long long)in_pDev);
            break;
        }
        if(OK != in_pDev->config->interface[0]->unregistering)
        {
            r -= ENODEV;
            LOG("%s::%s !!! Device %llx is being unregistered !!!\n", TAG, __func__, (unsigned long long)in_pDev);
            break;
        }
        if(NULL == in_pDev->config->interface[0]->cur_altsetting)
        {
            r -= EIO;
            LOG("%s::%s !!! No active interface for device %llx !!!\n", TAG, __func__, (unsigned long long)in_pDev);
            break;
        }
        inou_pData->bInterfaceClass = in_pDev->config->interface[0]->cur_altsetting->desc.bInterfaceClass;
        // 00h Device Unspecified Device class is unspecified, interface descriptors are used to determine needed drivers
        // 02h Both Communications and CDC Control Modem, Ethernet adapter, Wi-Fi adapter
        if((OK == inou_pData->bClass) || (2 != inou_pData->bClass))
        {
            inou_pData->bSubClass = in_pDev->config->interface[0]->cur_altsetting->desc.bInterfaceSubClass;
            inou_pData->bProtocol = in_pDev->config->interface[0]->cur_altsetting->desc.bInterfaceProtocol;
        }
        LOG("%s::%s %s Configuring device from Vendor %x idProduct %x\n", TAG, __func__, dev_name(&in_pDev->config->interface[0]->dev), inou_pData->idVendor, 
            inou_pData->idProduct);
        LOG("%s::%s Configuring device from bDeviceClass %x bDeviceSubClass %x bDeviceProtocol %x\n", TAG, __func__, inou_pData->bClass, inou_pData->bSubClass, 
            inou_pData->bProtocol);
        if(OK < in_pDev->descriptor.iManufacturer)
        {
            if(NULL != in_pDev->manufacturer)
            {
                inou_pData->sManufacturer = strdup(in_pDev->manufacturer);
                if(NULL == inou_pData->sManufacturer)
                {
                    LOG("%s::%s !!! (0) strdup error !!!\n", TAG, __func__);
                    r -= ENOMEM;
                    break;
                }
            }
        }
        if(OK < in_pDev->descriptor.iProduct)
        {
            if(NULL != in_pDev->product)
            {
                inou_pData->sProduct = strdup(in_pDev->product);
                if(NULL == inou_pData->sProduct)
                {
                    LOG("%s::%s !!! (1) strdup error !!!\n", TAG, __func__);
                    r -= ENOMEM;
                    break;
                }
            }
        }
        if(OK < in_pDev->descriptor.iSerialNumber)
        {
            if(NULL != in_pDev->serial)
            {
                inou_pData->sSerialNumber = strdup(in_pDev->serial);
                if(NULL == inou_pData->sSerialNumber)
                {
                    LOG("%s::%s !!! (1) strdup error !!!\n", TAG, __func__);
                    r -= ENOMEM;
                    break;
                }
            }
        }
    } while (FALSE);
    if(!IS_SUCCEEDED(r))
    {
        if(NULL != inou_pData)
        {
            if(NULL != inou_pData->sManufacturer)
                KFREE(inou_pData->sManufacturer);
            if(NULL != inou_pData->sProduct)
                KFREE(inou_pData->sProduct);
            if(NULL != inou_pData->sSerialNumber)
                KFREE(inou_pData->sSerialNumber);
        }
    }
    return r;
}

// notifications about USB devices insert/remove
static int zlck_usb_notify(struct notifier_block* in_Blk, unsigned long in_Act, void* in_pData)
{
    struct ZLCK_DEV_STOR_s* Zlck_storage_get_by_dev(void const* in_pUsbDev);
    //LOG("%s::%s %s\n", TAG, __func__, USBNCODE(in_Act));
    do
    {
        if(USB_DEVICE_ADD == in_Act)
        {
            // rejection by device type mice, keyboards, hubs, hcs
            if(!IS_SUCCEEDED(Zlck_storage_add(in_pData)))
                LOG("%s::%s !!! Zlck_storage_add error for device %llx !!!\n", TAG, __func__, (unsigned long long)in_pData);
            break;
        }
        else if(USB_DEVICE_REMOVE == in_Act)
        {
            struct ZLCK_DEV_STOR_s* entry = Zlck_storage_get_by_dev(in_pData);
            if(NULL != entry)
            {
                zlck_usb_dev_display(USBNCODE(in_Act), (struct usb_device*)in_pData);
                // 1-st dereferencing
                DEREF(entry);
                // 2-nd dereferencing
                DEREF(entry);
            }
            break;
        }
    } while (FALSE);
    return NOTIFY_OK;
}

static int zlck_is_matched(struct ZLCK_DIR_ENTRY_s* in_pDe, struct ZLCK_DEV_STOR_s* in_pDs)
{
    do
    {
        if(NULL == in_pDe || NULL == in_pDs)
            break;
        if(OK == in_pDe->name[OK])
            break;
        if(NULL != in_pDs->devdata.sSerialNumber)
        {
            if(strstr(in_pDe->name, in_pDs->devdata.sSerialNumber))
                return TRUE;
        }
        if(NULL != in_pDs->devdata.sProduct)
        {
            if(strstr(in_pDe->name, in_pDs->devdata.sProduct))
                return TRUE;
        }
        if(NULL != in_pDs->devdata.sManufacturer)
        {
            if(strstr(in_pDe->name, in_pDs->devdata.sManufacturer))
                return TRUE;
        }
    } while (FALSE);
    return FALSE;
}

static struct ZLCK_DIR_ENTRY_s* zlck_get_matched(struct ZLCK_DEV_STOR_s* in_pDs, struct list_head* proot)
{
    struct list_head* list = NULL;
    struct ZLCK_DIR_ENTRY_s* pde = NULL, *to_ret = NULL;
    do
    {
        list_for_each(list, proot)
        {
            pde = list_entry(list, typeof(*pde), link);
            LOG("%s::%s matching %s\n", TAG, __func__, pde->name);
            if(zlck_is_matched(pde, in_pDs))
            {
                LOG("%s::%s MATCHED\n", TAG, __func__);
                to_ret = pde;
                break;
            }
        }
    } while (FALSE);
    return to_ret;
}

// on idea, function parameter should be somthing like to the "b8:17"
static inline unsigned int zlck_get_sysidx(char const* in_pszDevName)
{
    unsigned int aDevIdx[2] = {0, 0};
    sscanf(in_pszDevName, "b%d:%d", &aDevIdx[0], &aDevIdx[1]);
    return MKDEV(aDevIdx[0], aDevIdx[1]);
}

static struct block_device* zlck_get_block_device(struct ZLCK_DIR_ENTRY_s* in_pDe)
{
    unsigned int nDevIdx = -1;
    struct block_device* dev = NULL;
    do
    {
        if(NULL == in_pDe)
            break;
        if(OK == in_pDe->name[OK])
            break;
        nDevIdx = zlck_get_sysidx(in_pDe->name);
        if(OK == nDevIdx)
        {
            LOG("%s::%s !!! can't obtain the system index for %s !!!\n", TAG, __func__, in_pDe->name);
            break;
        }
        LOG("%s::%s system index for %s %d:%d\n", TAG, __func__, in_pDe->name, MAJOR(nDevIdx), MINOR(nDevIdx));
        dev = bdget(nDevIdx);
        if(NULL == dev)
        {
            LOG("%s::%s !!! can't obtain device by index %d:%d !!!\n", TAG, __func__, MAJOR(nDevIdx), MINOR(nDevIdx));
            break;
        }
        bdput(dev);
        LOG("%s::%s index %d:%d => block_device %llx\n", TAG, __func__, MAJOR(nDevIdx), MINOR(nDevIdx), (unsigned long long)dev);
    } while (FALSE);
    return dev;
}

static int zlck_is_lnk_presents(struct list_head* root, char* in_pszLnk, int in_Id)
{
    int r = OK;
    struct list_head* list = NULL;
    struct ZLCK_STRING_ENTRY_s* pstre = NULL;
    do
    {
        list_for_each(list, root)
        {
            char* ps = NULL;
            pstre = list_entry(list, typeof(*pstre), link);
            if(DEVICE_LNK == in_Id)
            {
                if(NULL != pstre->psz1)
                    ps = pstre->psz1;
            }
            if(MOUNT_POINT == in_Id)
            {
                if(NULL != pstre->psz2)
                    ps = pstre->psz2;
            }
            if(NULL == ps)
                continue;
            if(NULL != strstr(ps, in_pszLnk))
            {
                r |= TRUE;
                break;
            }
        }
    } while (FALSE);
    return r;
}

static int zlck_set_dev_lnk(struct ZLCK_DEV_STOR_s* inou_pDstor, struct block_device const* blkdev)
{
    int r = OK;
    char asz[0x20] = {0};
    struct ZLCK_STRING_ENTRY_s* entry = NULL;
    do
    {
        // an intended data sanity check
        if(NULL == blkdev->bd_disk)
        {
            r -= ENODEV;
            break;
        }
        // an intended data sanity check
        if(NULL == blkdev->bd_part)
        {
            r -= ENODEV;
            break;
        }
        bdevname((struct block_device*)blkdev, asz);
        LOG("%s::%s %s\n", TAG, __func__, asz);
        // it should be SCSI
        if(('s' != asz[OK] && 'S' != asz[OK]) ||
            ('d' != asz[1] && 'D' != asz[1]))
        {
            r -= EXDEV;
            LOG("%s::%s !!! device name %s is invalid !!!\n", TAG, __func__, asz);
            break;
        }
        if(!IS_SUCCEEDED(zlck_is_lnk_presents(&inou_pDstor->partlist, asz, DEVICE_LNK)))
        {
            LOG("%s::%s !!! device %s is presented already !!!\n", TAG, __func__, asz);
            r -= EEXIST;
            break;
        }
        entry = KALLOC(sizeof(struct ZLCK_STRING_ENTRY_s));
        if(NULL == entry)
        {
            LOG("%s::%s !!! memory allocation error (0) !!!\n", TAG, __func__);
            r -= ENOMEM;
            break;
        }
        memset(entry, 0, sizeof(struct ZLCK_STRING_ENTRY_s));
        entry->psz1 = KALLOC(sizeof(DEV) + strlen(asz) + 1);
        if(NULL == entry->psz1)
        {
            LOG("%s::%s !!! memory allocation error (0) !!!\n", TAG, __func__);
            r -= ENOMEM;
            break;
        }
        memset(entry->psz1, 0, sizeof(DEV) + strlen(asz) + 1);
        strcpy(entry->psz1, DEV);
        strcat(entry->psz1, asz);
        list_add_tail(&entry->link, &inou_pDstor->partlist);
        LOG("%s::%s %s\n", TAG, __func__, BSD(entry));
    } while (FALSE);
    if(!IS_SUCCEEDED(r))
    {
        if(NULL != entry)
            KFREE(entry);
    }
    return r;
}

// try to obtain the device representation inside of /dev tree
static int zlck_obtain_disk_name(struct ZLCK_DEV_STOR_s* inou_pDstor)
{
    void Zlck_strlist_clean(struct list_head* head);
    int Zlck_grab_dir(char* const in_pszName, void* inou_pv);
    struct ZLCK_STRING_ENTRY_s inst = {{0}, NULL, NULL}, sins = {{0}, NULL, NULL};
    struct block_device* blkdev = NULL;    
    struct ZLCK_DIR_ENTRY_s* pde = NULL;
    char* pszDirName = NULL;
    int r = -EPERM;
    do
    {
        LOG("%s::%s ++\n", TAG, __func__);
        inst.psz1 = MNTPART;
        inst.psz2 = LNKPART;
        INIT_LIST_HEAD(&inst.link);
        INIT_LIST_HEAD(&sins.link);
        // 1-st stage, reading top directory
        if(!IS_SUCCEEDED(Zlck_grab_dir(DEVDIR, &inst)))
            break;
        if(list_empty(&inst.link))
        {
            LOG("%s::%s !!! no matching currently in the dir %s !!!\n", TAG, __func__, DEVDIR);
            break;
        }
        // 2-nd stage, get matching
        // may be more than single partition here, perhaps an internal cycle with the entry removing is necessary
        pde = zlck_get_matched(inou_pDstor, &inst.link);
        // on idea, the single entry only may be presented here
        if(NULL == pde)
        {
            LOG("%s::%s !!! no matched entries in the dir %s for now !!!\n", TAG, __func__, DEVDIR);
            break;
        }
        LOG("%s::%s matched %s\n", TAG, __func__, OK == pde->name[OK] ? "NULL" : pde->name);
        pszDirName = KALLOC(pde->nlen + sizeof(DEVDIR) + TRAILS /* insurance */);
        if(NULL == pszDirName)
        {
            LOG("%s::%s !!! memory allocation error !!!\n", TAG, __func__);
            break;
        }
        memset(pszDirName, 0, pde->nlen + sizeof(DEVDIR) + TRAILS);
        strcpy(pszDirName, DEVDIR);
        strcat(pszDirName, pde->name);
        LOG("%s::%s %s\n", TAG, __func__, pszDirName);
        // 3-rd stage, reading bottom directory
        if(!IS_SUCCEEDED(Zlck_grab_dir(pszDirName, &sins)))
            break;
        // sanity check, it should be arised never
        if(list_empty(&sins.link))
        {
            LOG("%s::%s !!! dir %s is empty !!!\n", TAG, __func__, pszDirName);
            break;
        }
        // 4-th stage, try to obtain the device index, device itself, device name
        blkdev = zlck_get_block_device(list_first_entry(&sins.link, struct ZLCK_DIR_ENTRY_s , link));
        LOG("%s::%s block device %llx\n", TAG, __func__, (unsigned long long)blkdev);
        if(NULL == blkdev)
            break;
        if(!IS_SUCCEEDED(zlck_set_dev_lnk(inou_pDstor, blkdev)))
            break;
        r ^= r;
    } while (FALSE);
    if(!list_empty(&sins.link))
        Zlck_strlist_clean(&sins.link);
    if(!list_empty(&inst.link))
        Zlck_strlist_clean(&inst.link);
    if(NULL != pszDirName)
        KFREE(pszDirName);
    LOG("%s::%s %d --\n", TAG, __func__, r);
    return r;
}

struct ZLCK_STRING_ENTRY_s* zlck_dev_get_fse(struct list_head* in_pRoot, int in_Idx)
{
    int idx = OK;
    struct list_head* list = NULL;
    struct ZLCK_STRING_ENTRY_s* tmp = NULL, *to_ret = NULL;
    list_for_each(list, in_pRoot)
    {
        tmp = list_entry(list, typeof(*tmp), link);
        if(idx == in_Idx)
        {
            to_ret = tmp;
            break;
        }
        idx++;
    }
    return to_ret;
}

static int zlck_set_mntpoint(struct ZLCK_STRING_ENTRY_s* inou_pEntry, char* in_pszMntList)
{
    int r = OK;
    char* pszMnts = NULL, *pszMnte = NULL;
    do
    {
        pszMnts = strstr(in_pszMntList, BSD(inou_pEntry));
        LOG("%s::%s (1)\n", TAG, __func__);
        if(NULL == pszMnts)
        {
            LOG("%s::%s !!! entry for %s don't found in list !!!\n", TAG, __func__, BSD(inou_pEntry));
            r -= ENOKEY;
            break;
        }
        // 1-st space
        pszMnts = strchr(pszMnts, ' ');
        LOG("%s::%s (2)\n", TAG, __func__);
        if(NULL == pszMnts)
        {
            LOG("%s::%s !!! (0) An invalid mount point for %s  !!!\n", TAG, __func__, BSD(inou_pEntry));
            r -= ENODATA;
            break;
        }
        ++pszMnts;
        if('/' != pszMnts[OK])
        {
            LOG("%s::%s !!! (1) An invalid mount point for %s  !!!\n", TAG, __func__, BSD(inou_pEntry));
            r -= EBADSLT;
            break;
        }
        // 2-nd space is bound
        pszMnte = strchr(pszMnts, ' ');
        LOG("%s::%s (3)\n", TAG, __func__);
        if(NULL == pszMnte)
        {
            LOG("%s::%s !!! (2) An invalid mount point for %s  !!!\n", TAG, __func__, BSD(inou_pEntry));
            r -= EBADR;
            break;
        }
        if(SLEN < (int)(pszMnte - pszMnts))
        {
            LOG("%s::%s !!! (4) An invalid mount point for %s  !!!\n", TAG, __func__, BSD(inou_pEntry));
            r -=ENAMETOOLONG;
            break;
        }
        MNT(inou_pEntry) = KALLOC((int)(pszMnte - pszMnts) + TRAILS);
        if(NULL == MNT(inou_pEntry))
        {
            LOG("%s::%s !!! memory allocation error !!!\n", TAG, __func__);
            r -= ENOMEM;
            break;
        }
        memset(MNT(inou_pEntry), 0, (int)(pszMnte - pszMnts) + TRAILS);
        memcpy(MNT(inou_pEntry), pszMnts, (int)(pszMnte - pszMnts));
    } while (FALSE);
    LOG("%s::%s --\n", TAG, __func__);
    return r;
}

static void zlck_obtain_mnt_points(struct ZLCK_DEV_STOR_s* inou_pDstor, char* in_pszMntList)
{
    int r = OK;
    struct ZLCK_STRING_ENTRY_s* mntentry = NULL;
    LOG("%s::%s ++\n", TAG, __func__);
    do
    {
        for(;;)
        {
            // some partitions may be dismounted manually, then we would had something like an iterator to skip them
            mntentry = zlck_dev_get_fse(&inou_pDstor->partlist, r);
            if(NULL == mntentry)
                break;
            if((NULL != BSD(mntentry)) && (NULL == MNT(mntentry)))
            {
                zlck_set_mntpoint(mntentry, in_pszMntList);
                if(NULL != MNT(mntentry))
                    LOG("%s::%s %s => %s\n", TAG, __func__, BSD(mntentry), MNT(mntentry));
            }
            r++;
        }
    } while (FALSE);
    LOG("%s::%s --\n", TAG, __func__);
}

// already inserted usb devices checking
static void zlck_devices_enumerate(void)
{
    struct usb_bus *bus;
    // External global kernel variables usb_bus_list and usb_bus_list_lock had been used here
    mutex_lock(&usb_bus_list_lock);
    list_for_each_entry(bus, &usb_bus_list, bus_list)
    {
        int chx = 0;
        struct usb_device* child = NULL;
        if(!bus_to_hcd(bus)->rh_registered)
            continue;
        zlck_usb_dev_display("hub", bus->root_hub);
        usb_hub_for_each_child(bus->root_hub, chx, child)
        {
            zlck_usb_dev_display("child", child);
            if(zlck_usb_is_device_necessary(child))
            {
                if(!IS_SUCCEEDED(Zlck_storage_add((void*)child)))
                    LOG("%s::%s !!! Zlck_storage_add error for device %llx !!!\n", TAG, __func__, (unsigned long long)child);
            }
            else
                LOG("%s::%s device %s %llx isn't necessary\n", TAG, __func__, NULL != child->product ? child->product : "NULL",
                    (unsigned long long)child);
        }
    }
    mutex_unlock(&usb_bus_list_lock);
}

static struct notifier_block gs_nblk =
{
    .notifier_call = zlck_usb_notify,
};

static int __init zlck_init(void)
{
    void Zlck_storage_init(void);
    int Zlck_is_storage_empty(void);
    int Zlck_read_file(char const* in_pszName, char** ppszData);
    struct ZLCK_DEV_STOR_s* Zlck_storage_get_by_idx(int in_Idx);
    char* pszFCont = NULL;
    do
    {
        LOG("%s::%s ++\n", TAG, __func__);
        Zlck_storage_init();
        zlck_devices_enumerate();
        usb_register_notify(&gs_nblk);
        if(!Zlck_is_storage_empty())
        {
            int i, r;
            struct ZLCK_DEV_STOR_s* devstor = NULL;
            for(i = 0;;i++)
            {
                devstor = Zlck_storage_get_by_idx(i);
                LOG("%s::%s [%d] %llx\n", TAG, __func__, i, (unsigned long long)devstor);
                if(NULL == devstor)
                    break;
                // TODO: more than single partition per device is mounted
                if(IS_SUCCEEDED(zlck_obtain_disk_name(devstor)))
                {
                    if(NULL == pszFCont)
                    {
                        r = Zlck_read_file(MNTTAB, &pszFCont);
                        if(!IS_SUCCEEDED(r) || (NULL == pszFCont))
                        {
                            DEREF(devstor);
                            break;
                        }
                    }
                    zlck_obtain_mnt_points(devstor, pszFCont);
                }
                DEREF(devstor);
                devstor = NULL;
            }
        }
    } while (FALSE);
    if(NULL != pszFCont)
        KFREE(pszFCont);
    LOG("%s::%s --\n", TAG, __func__);
    return 0;
}

static void __exit zlck_exit(void)
{
    void Zlck_storage_uninit(void);
    LOG("%s::%s ++\n", TAG, __func__);
    Zlck_storage_uninit();
    usb_unregister_notify(&gs_nblk);
    LOG("%s::%s --\n", TAG, __func__);
}

module_init(zlck_init);
module_exit(zlck_exit);