//**************************************************************************
// File: partmap.cpp
// Controlled device file systems storage class
// Viva, Caesar ! Morituri te salutant ! Implemented by Oleg Nikitenko.
// Copyright (C) SecurIT, 2014. All Rights reserved.
//**************************************************************************

#include "partmap.h"
#include <IOKit/IOLib.h>
#include "zlock_ui_iface.h"

#define super OSObject
OSDefineMetaClassAndStructors(ZPME, OSObject);

extern "C" void* kern_os_malloc(size_t size);
extern "C" void kern_os_free(void * addr);

void log(int in_Severity, const char *fmt, ...);

#define LOG log

// TODO: perhaps, lower implementation could be on __OSHashTable basis (IOSCSIArchitectureModelFamily)
// BTW, it must be carried into the top level driver class with the link to the ZFDE
// storage object for mapping between of BSD name and mount point
void ZPME::free()
{
    if(NULL != this->m_bsdName)
    {
        m_bsdName->release();
        m_bsdName = NULL;
    }
    if(NULL != this->m_mntPoint)
    {
        m_mntPoint->release();
        m_mntPoint = NULL;
    }
    super::free();
}

uint64_t ZPME::getPartSize() const { return m_PartSize; }
void ZPME::setPartSize(uint64_t in_Sz) { m_PartSize = in_Sz; }
bool ZPME::isFsRO() const { return NOTHING != (m_fsFalgs & MNT_RDONLY); }
void ZPME::setPartID(int in_Id) { m_Part = in_Id; }
bool ZPME::isPart() const { return NOTHING != m_Part; }
bool ZPME::isDrive() const { return !isPart(); }

int ZPME::vfsEnumCallback(mount_t mp, void* inout_pvArg)
{
    struct vfsstatfs* vfsstat = NULL;
    ZPME* me = NULL;
    int curhashe = NOTHING;
    ZLCK_HASHE_p_s phashe = NULL;
    
    //LOG(LOG_INF, "[ZPME]::%s\n", __func__);
    
    do
    {
        if(!IS_PTR_VALID(mp))
        {
            LOG(LOG_ERROR, "[ZPME]::%s !!! The mount point is invalid !!!\n", __func__);
            break;
        }
        me = OSDynamicCast(ZPME, (OSObject*)inout_pvArg);
        if(NULL == me)
        {
            LOG(LOG_ERROR, "[ZPME]::%s !!! An invalid argument !!!\n", __func__);
            break;
        }
        // nothing to be done, mount point kept already
        if(IS_PTR_VALID(me->m_pMntPoint))
            break;
        vfsstat = vfs_statfs(mp);
        if(NULL == vfsstat)
        {
            LOG(LOG_ERROR, "[ZPME]::%s !!! An invalid file system statistics !!!\n", __func__);
            break;
        }
        if(me->isMntHasheValid())
        {
            LOG(LOG_INF, "[ZPME]::%s %s -> %s\n", __func__, vfsstat->f_mntfromname, vfsstat->f_mntonname);
            phashe = me->getMntPtHashe();
            curhashe = hashe(vfsstat->f_mntonname, (int)strlen(vfsstat->f_mntonname));
            if(curhashe == phashe->h_data.h_val)
            {
                LOG(LOG_INF, "[ZPME]::%s FS %s is found\n", __func__, vfsstat->f_fstypename);
                me->m_pMntPoint = (void*)mp;
            }
        }
        else
            LOG(LOG_ERROR, "[ZPME]::%s !!! me->isMntHasheValid !!!\n", __func__);
    } while (false);
    return VFS_RETURNED;
}

void ZPME::setRO()
{
    mount_t pmnt = NULL;
    
    do
    {
        if(!IS_PTR_VALID(m_mntPoint))
            break;
        // alas, but it don't exported vfs_getvfs_by_mntonname((char*)m_bsdName->getCStringNoCopy());
        // The fsid_t usage at the such case is unhelpful, because OS will change that value on user relogon
        //pmnt = vfs_getvfs(&m_FsId);
        vfs_iterate(LK_NOWAIT, &ZPME::vfsEnumCallback, this);
        //LOG("[ZPME]::%s mount point %p\n", __func__, pmnt);
        pmnt = (mount_t)m_pMntPoint;
        if(!IS_PTR_VALID(pmnt))
        {
            LOG(LOG_ERROR, "[ZPME]::%s !!! couldn't retrieve the mount point !!!\n", __func__);
            break;
        }
        LOG(LOG_ERROR, "[ZPME]::%s set fs %s R/O access\n", __func__, getMNTPoint());
        //pmnt->mnt_flag |= MNT_RDONLY;
        vfs_setflags(pmnt, MNT_RDONLY);
        m_fsFalgs = vfs_flags(pmnt);
        // m_pMntPoint = NULL; // STUPID code !!!
    } while (false);
}

const char* ZPME::getBSDName() const
{
    //LOG(LOG_INF, "[ZPME]::%s name %s\n", __func__, m_bsdName ? m_bsdName->getCStringNoCopy() : "NULL");
    return NULL != this->m_bsdName ? this->m_bsdName->getCStringNoCopy() : NULL;
}

const char* ZPME::getMNTPoint() const
{
    return NULL != this->m_mntPoint ? this->m_mntPoint->getCStringNoCopy() : NULL;
}

bool ZPME::isSynced() const
{
    //LOG(LOG_INF, "[ZPME]::%s mntp %p bsd %p\n", __func__, m_mntPoint, m_bsdName);
    return NULL != m_mntPoint && NULL != m_bsdName;
}

// dismount handler
void ZPME::releaseMNTPoint()
{
    if(IS_PTR_VALID(m_mntPoint))
    {
        m_fsFalgs = 0;
        m_mntPoint->release();
        m_mntPoint = NULL;
        m_nMntPtHashe.common = 0;
    }
}

void ZPME::calcMntPtHashe()
{
    do
    {
        if(!IS_PTR_VALID(m_mntPoint))
            break;
        m_nMntPtHashe.h_data.h_len = m_mntPoint->getLength();
        m_nMntPtHashe.h_data.h_val = hashe(m_mntPoint->getCStringNoCopy(), m_nMntPtHashe.h_data.h_len);
    } while (false);
}

bool ZPME::isMntHasheValid() const
{
    if(!IS_PTR_VALID(m_mntPoint))
        return false;
    return NOTHING < m_nMntPtHashe.common;
}

ZLCK_HASHE_p_s ZPME::getMntPtHashe() const
{
    return (const ZLCK_HASHE_p_s)&m_nMntPtHashe;
}

void ZPME::setBSDName(const char* in_pszBSDName)
{
    size_t sz;
    char* pszPlaceHolder = NULL;
    m_fsFalgs = NOTHING;
    //LOG("[ZPME]::%s ++ %s\n", __func__, in_pszBSDName);
    if(IS_PTR_VALID(in_pszBSDName) && NOTHING < in_pszBSDName[0])
    {
        if(IS_PTR_VALID(m_bsdName))
        {
            m_bsdName->release();
            m_bsdName = NULL;
        }
        sz = strlen(BSD_PREFIX) + strlen(in_pszBSDName) + 1;
        pszPlaceHolder = TYPE(char*, kern_os_malloc(sz));
        if(!IS_PTR_VALID(pszPlaceHolder))
        {
            LOG(LOG_ERROR, "[ZPME]::%s !!! memory allocation error !!!\n", __func__);
            return;
        }
        bzero(pszPlaceHolder, sz);
        if(NOTHING != memcmp(in_pszBSDName, BSD_PREFIX, strlen(BSD_PREFIX)))
           strncat(pszPlaceHolder, BSD_PREFIX, strlen(BSD_PREFIX));
        strncat(pszPlaceHolder, in_pszBSDName, strlen(in_pszBSDName));
        //LOG("[ZPME]::%s %s\n", __func__, pszPlaceHolder);
        m_bsdName = OSString::withCString(pszPlaceHolder);
        m_nMntPtHashe.common = NOTHING;
        m_pMntPoint = NULL;
        if(!IS_PTR_VALID(m_bsdName))
            LOG(LOG_ERROR, "[ZPME]::%s !!! the BSD name constructing error !!!\n", __func__);
        kern_os_free(pszPlaceHolder);
    }
    else
        LOG(LOG_ERROR, "[ZPME]::%s !!! invalid argument !!!\n", __func__);
    //LOG("[ZPME]::%s %s --\n", __func__, m_bsdName->getCStringNoCopy());
}

void ZPME::setMNTPoint(const char* in_pszMNTPoint)
{
    if(IS_PTR_VALID(in_pszMNTPoint) && NOTHING < in_pszMNTPoint[0])
    {
        if(IS_PTR_VALID(m_mntPoint))
        {
            m_mntPoint->release();
            m_mntPoint = NULL;
            m_nMntPtHashe.common = 0;
        }
        m_mntPoint = OSString::withCString(in_pszMNTPoint);
        if(!IS_PTR_VALID(m_mntPoint))
            LOG(LOG_ERROR, "[ZPME]::%s !!! the mount point setting error !!!\n", __func__);
        else
            calcMntPtHashe();
    }
}
