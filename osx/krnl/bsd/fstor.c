//**************************************************************************
// File: fstor.c
// File entities storage list manipulations
// If this code works, it was written by Oleg Nikitenko, else i don't know
// who wrote it. Copyright (C) SecurIT, 2015. All Rights reserved.
//**************************************************************************

#include <mach/kern_return.h>
#include <libkern/OSAtomic.h>
#include <sys/file.h>
#include "fstor.h"
#include "utils.h"
#include "fileops.h"
#include "deviface.h"

static lck_mtx_t* g_spLock = NULL;
static PZLFS g_apLFSTror[STORAGE_SIZE] = {0};

__private_extern__ int IsDisabled();

__private_extern__ const char* GetSCLocalPath();

__private_extern__ int Sref(PZLFS inou_pFS)
{
    if(NULL == inou_pFS)
        return EINVAL;
    OSIncrementAtomic(&inou_pFS->m_RefCnt);
    return 0;
}

__private_extern__ int Srel(PZLFS inou_pFS)
{
    if(NULL == inou_pFS)
        return EINVAL;
    OSDecrementAtomic(&inou_pFS->m_RefCnt);
    if(OSCompareAndSwap(0, 0, &inou_pFS->m_RefCnt))
    {
        // clean up only, don't release finally
        //LOG("[ZLFS]::%s {%d}\n", __func__, Where);
        LOG("[ZLFS]::%s %s\n", __func__, GetName(inou_pFS, MNTPIOINT));
        CleanUp(inou_pFS, FALSE);
    }
    return 0;
}

__private_extern__ int Scnt(const PZLFS in_pFS)
{
    if(NULL == in_pFS)
        return EINVAL;
    return in_pFS->m_RefCnt;
}

__private_extern__ PZLFS GetVolumeByName(const char* in_pszMntPt, int in_Id)
{
    int i;
    const char* psz = NULL;
    PZLFS p = NULL;

    do
    {
        if(NULL == in_pszMntPt)
            break;
        if(0 == in_pszMntPt[0])
            break;
        if(NULL != g_spLock)
        {
            //LOG("[ZLFS]::%s %s\n", __func__, in_pszMntPt);
            LockLock(g_spLock);
            for(i = 0; i < ARRAYSIZE(g_apLFSTror); i++)
            {
                if(NULL != g_apLFSTror[i])
                {
                    if(IsVolInit(g_apLFSTror[i]) && !IsUnmounted(g_apLFSTror[i]))
                    {
                        psz = GetName(g_apLFSTror[i], in_Id);
                        //LOG("[ZLFS]::%s %s %s\n", __func__, in_pszMntPt, NULL != psz ? psz : "NULL");
                        if(NULL == psz)
                        {
                            LOG("[ZLFS]::%s !!! A data %d doesn't assigned for the volume [%d] !!!\n", __func__, in_Id, i);
                            continue;
                        }
                        if(prefix(psz, in_pszMntPt))
                        {
                            //LOG("[ZLFS]::%s\n", __func__);
                            p = g_apLFSTror[i];
                            // implicit reference, a caller must release this link, when it's go out from scope vision
                            Sref(p);
                            break;
                        }
                    }
                }
            }
            LockUnlock(g_spLock);
        }
    } while (FALSE);
    return p;
}

__private_extern__ PZLFS GetVolumeByMntPoint(mount_t in_MntPt)
{
    int i;
    PZLFS p = NULL;
    if(NULL != g_spLock)
    {
        LockLock(g_spLock);
        for(i = 0; i < ARRAYSIZE(g_apLFSTror); i++)
        {
            if(NULL != g_apLFSTror[i])
            {
                if(IsVolInit(g_apLFSTror[i]) && !IsUnmounted(g_apLFSTror[i]))
                {
                    if(GetMountPoint(g_apLFSTror[i]) == in_MntPt)
                    {
                        p = g_apLFSTror[i];
                        // implicit reference, a caller must release this link, when it's go out from scope vision
                        Sref(p);
                        break;
                    }
                }
            }
        }
        LockUnlock(g_spLock);
    }
    return p;
}

// volume by file entry
static PZLFS lfseToLfs(PZLFSE in_pLfse)
{
    int ret = 0, locked = 0;
    PZLFS lfs = NULL;
    PZLFSE cur = NULL;

    do
    {
        if(NULL == in_pLfse)
            break;
        if(NULL == g_spLock)
            break;
        LockLock(g_spLock);
        locked++;
        for(ret = 0; ret < ARRAYSIZE(g_apLFSTror); ret++)
        {
            if(IsVolInit(g_apLFSTror[ret]) && !IsUnmounted(g_apLFSTror[ret]))
            {
                if(LIST_EMPTY(&g_apLFSTror[ret]->m_List))
                    continue;
                LIST_FOREACH(cur, &g_apLFSTror[ret]->m_List, m_Link)
                {
                    if(cur == in_pLfse)
                    {
                        lfs = g_apLFSTror[ret];
                        Sref(lfs);
                        break;
                    }
                }
                if(NULL != lfs)
                    break;
            }
        }
    } while (FALSE);
    if(NULL != g_spLock)
    {
        if(NOTHING < locked)
            LockUnlock(g_spLock);
    }
    return lfs;
}

// policy name by file entry
__private_extern__ const char* GetPolicyName(PZLFSE in_pLfse)
{
    PZLFS pvol = NULL;
    const char* pszPolN = NULL;
    
    do
    {
        if(NULL == in_pLfse)
            break;
        if(NULLVP == in_pLfse->m_pNode)
            break;
        pvol = lfseToLfs(in_pLfse);
    } while (FALSE);
    if(NULL != pvol)
    {
        pszPolN = GetName(pvol, POLNAME);
        Srel(pvol);
    }
    return pszPolN;
}

// device name by file entry
__private_extern__ const char* GetDeviceName(PZLFSE in_pLfse)
{
    PZLFS pvol = NULL;
    const char* pszDevN = NULL;
    
    do
    {
        if(NULL == in_pLfse)
            break;
        if(NULLVP == in_pLfse->m_pNode)
            break;
        pvol = lfseToLfs(in_pLfse);
    } while (FALSE);
    if(NULL != pvol)
    {
        pszDevN = GetName(pvol, DEVNAME);
        Srel(pvol);
    }
    return pszDevN;
}

__private_extern__ void DeInit(void)
{
    int i;
    for(i = 0; i < ARRAYSIZE(g_apLFSTror); i++)
    {
        if(NULL != g_apLFSTror[i])
        {
            CleanUp(g_apLFSTror[i], TRUE);
            g_apLFSTror[i] = NULL;
        }
    }
    if(NULL != g_spLock)
    {
        LockFree(g_spLock);
        g_spLock = NULL;
    }
}

__private_extern__ int Init(void)
{
    do
    {
        g_spLock = LockAlloc();
        if(NULL == g_spLock)
        {
            LOG("[ZLFS]::%s !!! memory allocation error !!!\n", __func__);
            return ENOMEM;
        }
        memset(g_apLFSTror, 0, sizeof(g_apLFSTror));
    } while (FALSE);
    return KERN_SUCCESS;
}

static int allocInit(ZLFS** inou_ppLfs, const char* in_pszMntPoint)
{
    ZLFS* plFs = NULL;
    if(NULL == inou_ppLfs)
        return KERN_INVALID_ARGUMENT;
    plFs = KMEMALLOC(sizeof(ZLFS));
    if(NULL == plFs)
    {
        LOG("[ZLFS]::%s !!! Storage allocation error !!!\n", __func__);
        return KERN_MEMORY_FAILURE;
    }
    memset(plFs, 0, sizeof(ZLFS));
    if(NULL != in_pszMntPoint)
    {
        if(!IS_SUCCEEDED(SetName(plFs, in_pszMntPoint, MNTPIOINT)))
        {
            KMEMFREE(plFs);
            return KERN_MEMORY_FAILURE;
        }
    }
    if(NULL == plFs->m_pLock)
    {
        plFs->m_pLock = LockAlloc();
        if(NULL == plFs->m_pLock)
        {
            LOG("[ZLFS]::%s !!! Lock allocation error !!!\n", __func__);
            if(NULL != plFs->m_pszMntPt)
                KMEMFREE(plFs->m_pszMntPt);
            KMEMFREE(plFs);
            return KERN_MEMORY_FAILURE;
        }
    }
    *inou_ppLfs = plFs;
    return KERN_SUCCESS;
}

__private_extern__ int InitVolume(struct zlck_ioctl_s* inpvPar)
{
    PZLFS pvol = NULL;
    int ret = KERN_SUCCESS, locked = 0, idx;
    mount_t mntpt = NULL;

    do
    {
        //LOG("[ZLFS]::%s (0)\n", __func__);
        if(NULL == g_spLock)
        {
            ret = ENOLCK;
            break;
        }
        // On idea, i can check is this string prefixed by "/Volumes/" token
        if(NOTHING == inpvPar->mnt[0] || '/' != inpvPar->bsd[0] || NOTHING == inpvPar->dev[0])
        {
            ret = EINVAL;
            break;
        }
        // try to obtain a referenced mount point
        mntpt = g_VfsGetVfsByMntPtr(inpvPar->mnt);
        LOG("[ZLFS]::%s A device %s volume %s -> mount point %x%x\n", __func__, inpvPar->dev, inpvPar->mnt, HIDWORD(mntpt), LODWORD(mntpt));
        if(NULL == mntpt)
        {
            LOG("[ZLFS]::%s !!! Can't obtain mount point for volume %s !!!\n", __func__, inpvPar->mnt);
            ret = EINVAL;
            break;
        }
        // mount point dereferencing
        g_MntIterDropPtr(mntpt);
        pvol = GetVolumeByMntPoint(mntpt);
        if(NULL != pvol)
        {
            LOG("[ZLFS]::%s A volume %s presented already\n", __func__, inpvPar->mnt);
            Srel(pvol);
            break;
        }
        LockLock(g_spLock);
        locked++;
        for(idx = 0; idx < ARRAYSIZE(g_apLFSTror); idx++)
        {
            if(IsVolInit(g_apLFSTror[idx]) && !IsUnmounted(g_apLFSTror[idx]))
                continue;
            break;
        }
        //LOG("[ZLFS]::%s (1) [%d]\n", __func__, idx);
        if(idx >= ARRAYSIZE(g_apLFSTror))
        {
            LOG("[ZLFS]::%s !!! All entries are busy !!!\n", __func__);
            ret = ENOENT;
            break;
        }
        // An empty volume storage presents already
        if(NULL == g_apLFSTror[idx])
        {
            // for the secondary code usage mount point will not be assigned here
            ret = allocInit(&g_apLFSTror[idx], NULL);
            if(!IS_SUCCEEDED(ret))
            {
                LOG("[ZLFS]::%s !!! allocInit error %d !!!\n", __func__, ret);
                break;
            }
        }
        g_apLFSTror[idx]->m_MntPoint = mntpt;
        ret = SetName(g_apLFSTror[idx], inpvPar->mnt, MNTPIOINT);
        if(!IS_SUCCEEDED(ret))
        {
            LOG("[ZLFS]::%s !!! SetName MNT error %d !!!\n", __func__, ret);
            break;
        }
        ret = SetName(g_apLFSTror[idx], inpvPar->bsd, BSDNAME);
        if(!IS_SUCCEEDED(ret))
        {
            LOG("[ZLFS]::%s !!! SetName BSD error %d !!!\n", __func__, ret);
            break;
        }
        ret = SetName(g_apLFSTror[idx], inpvPar->dev, DEVNAME);
        if(!IS_SUCCEEDED(ret))
        {
            LOG("[ZLFS]::%s !!! SetName DEV error %d !!!\n", __func__, ret);
            break;
        }
        ret = SetName(g_apLFSTror[idx], inpvPar->pol, POLNAME);
        if(!IS_SUCCEEDED(ret))
        {
            LOG("[ZLFS]::%s !!! SetName POL error %d !!!\n", __func__, ret);
            break;
        }
        Sref(g_apLFSTror[idx]);
        //LOG("[ZLFS]::%s (2) [%d]\n", __func__, ret);
    } while (FALSE);
    if(!IS_SUCCEEDED(ret) && (idx < ARRAYSIZE(g_apLFSTror)) && (NULL != g_apLFSTror[idx]))
    {
        CleanUp(g_apLFSTror[idx], 1);
        g_apLFSTror[idx] = NULL;
    }
    if(NULL != g_spLock && NOTHING < locked)
        LockUnlock(g_spLock);
    return ret;
}

__private_extern__ int UninitVolume(struct zlck_ioctl_s* inpvPar)
{
    int r = EINVAL;
    PZLFS pvol = NULL;
    
    do
    {
        if(NULL == inpvPar)
            break;
        if('/' == inpvPar->mnt[0])
            pvol = GetVolumeByName(inpvPar->mnt, MNTPIOINT);
        else if('/' == inpvPar->bsd[0])
            pvol = GetVolumeByName(inpvPar->bsd, BSDNAME);
        if(NULL != pvol)
        {
            // 1-st case for current get
            Srel(pvol);
            // final release
            Srel(pvol);
            r = KERN_SUCCESS;
            break;
        }
        LOG("[ZLFS]::%s !!! Can't find volume %s !!!\n", __func__, 0 < inpvPar->mnt[0] ? inpvPar->mnt : inpvPar->bsd);
    } while (FALSE);
    return r;
}

__private_extern__ void Lock(ZLFS* inou_p) { LockLock(inou_p->m_pLock); }

__private_extern__ void Unlock(ZLFS* inou_p) { LockUnlock(inou_p->m_pLock); }

static void RemoveEntry(ZLFS* inou_pLfs, PZLFSE in_Entry)
{
    do
    {
        if(!IsVolInit(inou_pLfs))
            break;
        Lock(inou_pLfs);
        LIST_REMOVE(in_Entry, m_Link);
        inou_pLfs->m_Cnt--;
        Unlock(inou_pLfs);
    } while(FALSE);
}

// non locked execution, with no lookup
static void ReleaseEntry(PZLFSE in_pEntry)
{
    do
    {
        if(NULL == in_pEntry)
            break;
        // TODO: insert entry into the deffered thread handler, or simple close/destroy
        KMEMFREE(in_pEntry);
    } while (FALSE);
}

__private_extern__ int FseCnt(const PZLFSE in_pFse)
{
    if(NULL == in_pFse)
        return EINVAL;
    return in_pFse->m_Cnt;
}

__private_extern__ int FseRef(PZLFSE inou_pFse)
{
    if(NULL == inou_pFse)
        return EINVAL;
    OSIncrementAtomic(&inou_pFse->m_Cnt);
    return KERN_SUCCESS;
}

__private_extern__ int FseRel(PZLFSE inou_pFse, PZLFS inou_pLfs)
{
    if(NULL == inou_pFse)
        return EINVAL;
    OSDecrementAtomic(&inou_pFse->m_Cnt);
    if(OSCompareAndSwap(0, 0, &inou_pFse->m_Cnt))
    {
        RemoveEntry(inou_pLfs, inou_pFse);
        ReleaseEntry(inou_pFse);
    }
    return KERN_SUCCESS;
}

static int setData(char** inou_ppData, const char* in_pszName)
{
    size_t len;
    int r = KERN_SUCCESS;
    
    do
    {
        if(NULL != *inou_ppData)
        {
            KMEMFREE(*inou_ppData);
            *inou_ppData = NULL;
        }
        len = strlen(in_pszName) + 1;
        *inou_ppData = KMEMALLOC(len);
        if(NULL == *inou_ppData)
        {
            LOG("[ZLFS]::%s !!! memory allocation error !!!\n", __func__);
            r = ENOMEM;
            break;
        }
        memset(*inou_ppData, 0, len);
        memcpy(*inou_ppData, in_pszName, len - 1);
    } while (FALSE);
    return r;
}

__private_extern__ int SetName(ZLFS* inou_pLfs, const char* in_pszName, int in_Id)
{
    char** pp = NULL;
    do
    {
        if(NULL == inou_pLfs || NULL == in_pszName)
            return EINVAL;
        if(NOTHING == in_pszName[0])
            return EINVAL;
        if(BSDNAME > in_Id || POLNAME < in_Id)
            return EINVAL;
        if(BSDNAME == in_Id)
            pp = &inou_pLfs->m_pszBsd;
        else if(MNTPIOINT == in_Id)
            pp = &inou_pLfs->m_pszMntPt;
        else if(DEVNAME == in_Id)
            pp = &inou_pLfs->m_pszDeviceName;
        else if(POLNAME == in_Id)
            pp = &inou_pLfs->m_pszPolName;
    } while (FALSE);
    return NULL != pp ? setData(pp, in_pszName) : EINVAL;
}

// unlocked version, bool type
static int lookupNoLock(ZLFS* in_pLfs, int in_Fd, proc_t in_Proc, PZLFSE* inou_ppEntry)
{
    PZLFSE cur;
    LIST_FOREACH(cur, &in_pLfs->m_List, m_Link)
    {
        if(cur->m_Fd == in_Fd && cur->m_pProc == in_Proc)
        {
            *inou_ppEntry = cur;
            break;
        }
    }
    return NULL != *inou_ppEntry;
}

// unlocked version, bool type
static int lookupNoLockbyVnode(ZLFS* in_pLfs, vnode_t in_pNode, PZLFSE* inou_ppEntry)
{
    PZLFSE cur;
    LIST_FOREACH(cur, &in_pLfs->m_List, m_Link)
    {
        if(cur->m_pNode == in_pNode)
        {
            *inou_ppEntry = cur;
            break;
        }
    }
    return NULL != *inou_ppEntry;
}

// locked version
__private_extern__ PZLFSE GetEntryByProcFd(ZLFS* in_pLfs, int in_Fd, proc_t in_Proc)
{
    PZLFSE entry = NULL;
    do
    {
        if(!IsVolInit(in_pLfs))
            break;
        Lock(in_pLfs);
        lookupNoLock(in_pLfs, in_Fd, in_Proc, &entry);
        Unlock(in_pLfs);
    } while(FALSE);
    if(NULL != entry)
        FseRef(entry);
    return entry;
}

// unlocked version
static int lookupByProcAddr(ZLFS* in_pLfs, uint64_t in_UsrAddr, proc_t in_Proc, PZLFSE* inou_ppEntry)
{
    PZLFSE cur;
    LIST_FOREACH(cur, &in_pLfs->m_List, m_Link)
    {
        if(cur->m_pUsrAddr == in_UsrAddr && cur->m_pProc == in_Proc)
        {
            *inou_ppEntry = cur;
            break;
        }
    }
    return NULL != *inou_ppEntry;
}

// locked version
__private_extern__ PZLFSE GetEntryByProcAddr(ZLFS* in_pLfs, uint64_t in_UsrAddr, proc_t in_Proc)
{
    PZLFSE entry = NULL;
    do
    {
        if(!IsVolInit(in_pLfs))
            break;
        Lock(in_pLfs);
        lookupByProcAddr(in_pLfs, in_UsrAddr, in_Proc, &entry);
        Unlock(in_pLfs);
    } while(FALSE);
    if(NULL != entry)
        FseRef(entry);
    return entry;
}

__private_extern__ PZLFSE AddEntry(ZLFS* inou_pLfs, int in_Fd, proc_t in_Proc, vnode_t in_pNode, int in_Flgs)
{
    PZLFSE entry = NULL;
    do
    {
        if(!IsVolInit(inou_pLfs) || IsUnmounted(inou_pLfs))
            break;
        if((NOTHING < GetCount(inou_pLfs)) && (NULL != (entry = GetEntryByProcFd(inou_pLfs, in_Fd, in_Proc))))
        {
            // it's unreal, but decency reason
            if(NULL != entry)
                FseRel(entry, inou_pLfs);
            break;
        }
        entry = KMEMALLOC(sizeof(struct ZLFSE));
        if(NULL == entry)
        {
            LOG("[ZLFS]::%s !!! entry allocation error !!!\n", __func__);
            break;
        }
        memset(entry, 0, sizeof(struct ZLFSE));
        entry->m_Fd = in_Fd;
        entry->m_pNode = in_pNode;
        entry->m_pProc = in_Proc;
        entry->m_Flags = in_Flgs;
        Lock(inou_pLfs);
        FseRef(entry);
        LIST_INSERT_HEAD(&inou_pLfs->m_List, entry, m_Link);
        inou_pLfs->m_Cnt++;
        Unlock(inou_pLfs);
    } while (FALSE);
    return entry;
}

// It doesn't very cool, there are some collisions can take place
__private_extern__ PZLFSE GetEntryByVnode(ZLFS* in_pLfs, vnode_t in_pNode)
{
    PZLFSE entry = NULL;
    do
    {
        if(!IsVolInit(in_pLfs))
            break;
        Lock(in_pLfs);
        lookupNoLockbyVnode(in_pLfs, in_pNode, &entry);
        Unlock(in_pLfs);
    } while(FALSE);
    if(NULL != entry)
        FseRef(entry);
    return entry;
}

// TODO: think about mmapped entries handling
static void CleanProcessEntries(ZLFS* inou_pLfs, proc_t in_Proc)
{
    char pr[MAXCOMLEN + 1];
    LIST_HEAD(, ZLFSE) llist = {0};
    PZLFSE cur = NULL;
    int cnt = 0;
    
    if(!IsVolInit(inou_pLfs))
        return;
    Lock(inou_pLfs);

    do
    {
        // locked entries enumeration
        LIST_FOREACH(cur, &inou_pLfs->m_List, m_Link)
        {
            if(cur->m_pProc == in_Proc)
                break;
        }
        if(NULL == cur)
            break;
        else if(cur->m_pProc == in_Proc)
        {
            // from the main list remove
            LIST_REMOVE(cur, m_Link);
            inou_pLfs->m_Cnt--;
            // insert into the temporary list
            LIST_INSERT_HEAD(&llist, cur, m_Link);
        }
    } while (TRUE);
    Unlock(inou_pLfs);
    // temporary list cleaning
    while(!LIST_EMPTY(&llist))
    {
        cnt++;
        cur = LIST_FIRST(&llist);
        if(NULL != cur && NULL != cur->m_pPairNode)
            VnZlkClose(cur, GetSCLocalPath());
        LIST_REMOVE(cur, m_Link);
        ReleaseEntry(cur);
    }
    if(NOTHING < cnt)
    {
        LOG("[MKZP]::%s (%s %x%x) %d entries\n", __func__, ZlkProcName(in_Proc, pr, sizeof(pr)), HIDWORD(in_Proc), LODWORD(in_Proc), cnt);
    }
}

// process is being exited, all entries must be cleaned, removed, released
__private_extern__ void CleanProcess(proc_t in_Proc)
{
    int i;
    if(NULL != g_spLock)
    {
        LockLock(g_spLock);
        for(i = 0; i < ARRAYSIZE(g_apLFSTror); i++)
        {
            if(NULL != g_apLFSTror[i])
            {
                if(IsVolInit(g_apLFSTror[i]) && !IsUnmounted(g_apLFSTror[i]))
                    CleanProcessEntries(g_apLFSTror[i], in_Proc);
            }
        }
        LockUnlock(g_spLock);
    }
}

// unconditional clean
__private_extern__ void CleanUp(ZLFS* inou_pLfs, int in_lRel)
{
    PZLFSE entry = NULL;
    if(!IsVolInit(inou_pLfs))
        return;
    LOG("[ZLFS]::%s volume %s is being released\n", __func__, GetName(inou_pLfs, MNTPIOINT));
    if(NOTHING < GetCount(inou_pLfs))
        LOG("[ZLFS]::%s entries %llu\n", __func__, GetCount(inou_pLfs));
    Lock(inou_pLfs);
    if(NULL != inou_pLfs->m_pszMntPt)
    {
        KMEMFREE(inou_pLfs->m_pszMntPt);
        inou_pLfs->m_pszMntPt = NULL;
    }
    if(NULL != inou_pLfs->m_pszBsd)
    {
        KMEMFREE(inou_pLfs->m_pszBsd);
        inou_pLfs->m_pszBsd = NULL;
    }
    if(NULL != inou_pLfs->m_pszDeviceName)
    {
        KMEMFREE(inou_pLfs->m_pszDeviceName);
        inou_pLfs->m_pszDeviceName = NULL;
    }
    if(NULL != inou_pLfs->m_pszPolName)
    {
        KMEMFREE(inou_pLfs->m_pszPolName);
        inou_pLfs->m_pszPolName = NULL;
    }
    while(!LIST_EMPTY(&inou_pLfs->m_List))
    {
        entry = LIST_FIRST(&inou_pLfs->m_List);
        LIST_REMOVE(entry, m_Link);
        ReleaseEntry(entry);
    }
    inou_pLfs->m_Cnt = 0;
    inou_pLfs->m_nUnmount = 0;
    Unlock(inou_pLfs);
    if(NOTHING != in_lRel)
    {
        LockFree(inou_pLfs->m_pLock);
        inou_pLfs->m_pLock = NULL;
        KMEMFREE(inou_pLfs);
    }
}

__private_extern__ void ClearAll(void)
{
    int i;
    if(IsDisabled())
    {
        LockLock(g_spLock);
        for(i = 0; i < ARRAYSIZE(g_apLFSTror); i++)
        {
            if(NULL != g_apLFSTror[i])
            {
                CleanUp(g_apLFSTror[i], 1);
            }
        }
        LockUnlock(g_spLock);
    }
}