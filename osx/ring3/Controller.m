//  Controller.m
//  UninstZlock
//  Created by oleshii on 12.09.14
//  Copyright (c) 2014 zecurion. All rights reserved.

#import "Controller.h"

#import <sys/stat.h>
#import <sys/sysctl.h>
#import <signal.h>

#define ZLOCK_TOKEN "zlock"
#define KEXTSTAT    "/usr/sbin/kextstat | grep [Zz]ecu > "
#define TMPFILE     "/tmp/zlcdrv.txt"
#define ZECURION    "zecurion"
#define PKGUTIL     "pkgutil --pkgs | grep [Zz]ecu > "
#define BUNDLE_FLR  "/Library/zlock/"
#define UNINSTALL   "/Library/zlock/uninstall.sh"
#define U_SHELL     "osascript -e \'do shell script \"/Library/zlock/uninstall.sh\" with administrator privileges\'"
#define LAUNCH_UNL  "launchctl unload -w "
#define AGENT_LIST  "/Library/LaunchDaemons/com.zecurion.agzlock.plist"
#define SERVC_LIST  "/Library/LaunchDaemons/com.zecurion.zlockservice.plist"

// check among of ran processes
static int CheckProcesses(int* inou_pnPids)
{
    int n, i;
    size_t bufSize = 0;
    int b = -1;
    struct kinfo_proc* kprocbuf = NULL, *cur = NULL;
    int mib[4] = { CTL_KERN, KERN_PROC, KERN_PROC_ALL, 0 };
    if (sysctl(mib, 4, NULL, &bufSize, NULL, 0) < 0)
    {
        NSLog(@"[%s] sysctl error %d %s\n", __func__, errno, strerror(errno));
        return b;
    }
    kprocbuf = (struct kinfo_proc *)malloc(bufSize);
    if(NULL == kprocbuf)
    {
        NSLog(@"[%s] memory allocation error %d %s\n", __func__, errno, strerror(errno));
        return b;
    }
    if(sysctl(mib, 4, kprocbuf, &bufSize, NULL, 0) < 0)
    {
        NSLog(@"[%s] sysctl error %d %s\n", __func__, errno, strerror(errno));
        free(kprocbuf);
        return b;
    }
    n = (int)bufSize / sizeof(struct kinfo_proc);
    cur = kprocbuf;
    for(i = 0; i < n; i++)
    {
        cur = kprocbuf + i;
        NSLog(@"[%s] [%d] pid %d %s\n", __func__, i, cur->kp_proc.p_pid, cur->kp_proc.p_comm);
        if(strstr(cur->kp_proc.p_comm, ZLOCK_TOKEN))
        {
            if(NULL == inou_pnPids)
            {
                b = 0;
                break;
            }
            else
            {
                if(0 == inou_pnPids[0])
                    inou_pnPids[0] = cur->kp_proc.p_pid;
                else
                    inou_pnPids[1] = cur->kp_proc.p_pid;
            }
        }
    }
    free(kprocbuf);
    return b;
}

static int GetFileContent(char* in_pszName, char* oupsz_Data, int in_Len)
{
    int r = -1, fd = 0, prst = 0;
    struct stat statbuf = {0};
    
    do
    {
        if(NULL == in_pszName || 0 == in_pszName[0] || NULL == oupsz_Data || in_Len <= 0)
            break;
        if(0 > access(in_pszName, R_OK))
        {
            NSLog(@"[%s] access error %d %s", __func__, errno, strerror(errno));
            break;
        }
        fd = open(in_pszName, O_RDONLY);
        if(0 > fd)
        {
            NSLog(@"[%s] open error %d %s", __func__, errno, strerror(errno));
            break;
        }
        if(0 > fstat(fd, &statbuf))
        {
            NSLog(@"[%s] fstat error %d %s", __func__, errno, strerror(errno));
            break;
        }
        if(S_IFREG != statbuf.st_mode || 0 == statbuf.st_size)
        {
            NSLog(@"[%s] invalid attributes or size", __func__);
            break;
        }
        r = (int)read(fd, oupsz_Data, statbuf.st_size < (size_t)in_Len ? statbuf.st_size : (size_t)(in_Len - 1));
        if(0 >= r || 0 == oupsz_Data[0])
        {
            NSLog(@"[%s] read error %d %s", __func__, errno, strerror(errno));
            break;
        }
    } while (false);
    if(0 < fd)
        close(fd);
    if(0 < prst)
       unlink(in_pszName);
    return r;
}

// check among of drivers or packager by external command line tool
static int sysCommand(const char* in_pszCmd, const char* in_pszRedir)
{
    int r = 0;
    char sCmd[0x80] = {0}, sContent[0x200] = {0};
    
    do
    {
        strcat(sCmd, in_pszCmd);
        if(NULL != in_pszRedir)
            strcat(sCmd, in_pszRedir);
        NSLog(@"[%s] %s\n", __func__, sCmd);
        r = system(sCmd);
        sync();
        NSLog(@"[%s] rezult code %d\n", __func__, r);
        if(0 > r || r == 0x7f)
        {
            NSLog(@"[%s] !!! system call error %d %s !!!\n", __func__, errno, strerror(errno));
            r = -1;
            break;
        }
        if(NULL != in_pszRedir)
        {
            if(0 > GetFileContent(TMPFILE, sContent, sizeof(sContent)))
            {
                NSLog(@"[%s] !!! GetFileContent error !!!", __func__);
                r = -2;
                break;
            }
            if(NULL == strstr(sContent, ZECURION))
            {
                NSLog(@"[%s] An entity isn't presented\n", __func__);
                r = -3;
                break;
            }
            else
                NSLog(@"[%s] An entity is presentd\n", __func__);
        }
    } while (false);
    return r;
}

int isZlockInstalled()
{
    int ret = 0 > CheckProcesses(NULL) ? 0 : 1;
    ret |= 0 > sysCommand(KEXTSTAT, TMPFILE) ? 0 : 1 << 1;
    ret |= 0 > sysCommand(PKGUTIL, TMPFILE) ? 0 : 1 << 2;
    // does the project installation folder exist ?
    ret |= 0 > access(BUNDLE_FLR, R_OK) ? 0 : 1 << 3;
    // does the uninstall script exist ?
    ret |= 0 > access(UNINSTALL, R_OK) ? 0 : 1 << 4;
    NSLog(@"[%s] %d", __func__, ret);
    return ret;
}

void unlinkApp()
{
    char wd[PATH_MAX] = {0};
    int r = 0;

    NSLog(@"[%s] %@", __func__, [[NSBundle mainBundle] bundlePath]);
    strcat(wd, "rm -rf ");
    strcat(&wd[7], [[[NSBundle mainBundle] bundlePath] UTF8String]);
    r = system(wd);
    NSLog(@"[%s] %s ret %d", __func__, wd, r);
    if(0 > r || r == 0x7f)
    {
        NSLog(@"[%s] !!! system call error %d %s !!!\n", __func__, errno, strerror(errno));
    }
    NSLog(@"[%s] Application deleted", __func__);
}

bool IsRebootNeeded()
{
    int fd = -1;
    do
    {
        fd = open("/dev/zlckmod0", O_RDWR);
        if(fd >= 0)
            close(fd);
    } while (false);
    return fd >= 0;
}

@implementation Controller
- (IBAction)Ok:(id)sender
{
    OSStatus status = noErr;

    do
    {
        NSLog(@"[%s]", __func__);
        [m_Ok setState:NSOffState];
        [m_Ok setEnabled:NO];
        [m_Cancel setEnabled:NO];
        [m_TextField setStringValue:@"Wait, please..."];
        if(!isZlockInstalled())
        {
            [m_TextField setStringValue:@"Zlock product don't installed"];
            break;
        }
        status = sysCommand(U_SHELL, NULL);
        if(noErr == status)
        {
            if(!IsRebootNeeded())
                [m_TextField setStringValue:@"Zlock uninstall process completed"];
            else
                [m_TextField setStringValue:@"Please, reboot your host to full uninstall completion"];
        }
        
    } while (false);
    [m_Ok setHidden:YES];
    [m_Cancel setHidden:YES];
    [m_Exit setHidden:NO];
}

- (IBAction)Cancel:(id)sender
{
    NSLog(@"[%s]", __func__);
    [[NSApp mainWindow] close];
    [NSApp terminate:0];
}

- (IBAction)ExitFromThere:(id)sender
{
    return [self Cancel:sender];
}
@end
