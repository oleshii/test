#include <vector>
#include <string>
#include <stdio.h>
#include <errno.h>
#include <assert.h>
#include <typeinfo>

#include <CoreFoundation/CoreFoundation.h>

#include <zlock_ui_iface.h>
#include "../mkplugin/mkpzlock/mkpzlock/deviface.h"

#if !defined(ZLCK_SVC)
#define	LOG	printf
#else
#include <zlib.h>
#define	LOG	ZTRACE
#endif
#define ASSERT assert

using namespace std;

// structures, which are contains an objects, must be constructed by new operator. when they're allocated by malloc, embedded members must be constructed by allocating operator new
typedef struct _PART_MAP_
{
    bool bMnted;	// mount identifier
    string sMntPt;	// mount point
    string sDrive;	// BSD /dev tree drive(partition)
} PART_MAP, *LPPART_MAP;

typedef vector<LPPART_MAP> PMVECTOR;

inline static bool StrCrtAndSetInto(const char* in_pszContent, const char* in_pszKey, CFMutableDictionaryRef inou_Ref)
{
    CFStringRef strRef = NULL, szKeyRef = 0;
    do
    {
	LOG("%s %s -> %s\n", __func__, in_pszKey, in_pszContent);
        strRef = CFStringCreateWithCString(NULL, in_pszContent, kCFStringEncodingASCII);
	if(NULL == strRef)
	{
    	    LOG("%s !!! CFStringCreateWithCString (0) error !!!\n", __func__);
    	    break;
	}
	szKeyRef = CFStringCreateWithCString(NULL, in_pszKey, kCFStringEncodingASCII);
	if(NULL == szKeyRef)
	{
    	    LOG("%s !!! CFStringCreateWithCString (1) error !!!\n", __func__);
    	    break;
	}
	CFDictionarySetValue(inou_Ref, szKeyRef, strRef);
    } while (false);
    if(NULL != strRef)
	CFRelease(strRef);
    if(NULL != szKeyRef)
	CFRelease(szKeyRef);
    return NULL != strRef;
}

// conversion from CFString to char*
class CString
{
    private:
	char* m_pBuf;
	size_t m_Size;
	CFStringRef m_Ref;
	bool m_bRel;
    private:
	CString();
	CString& operator=(char*);
	CString& operator=(const char*);
	CString& operator=(CFStringRef in_Ref);
	operator const char*();
    public:
	CString(CFStringRef in_Ref, bool in_bRel = false) : m_Ref(in_Ref), m_pBuf(NULL), m_Size(0), m_bRel(in_bRel) {}
	~CString()
	{
	    if(NULL != m_pBuf)
		free(m_pBuf);
	    if(m_bRel)
		CFRelease(m_Ref);
	}
	operator char*()
	{
	    CFIndex len;
	    do
	    {
		if(NULL == m_pBuf)
		{
		    if((NULL != m_Ref) && CFStringGetTypeID() == CFGetTypeID(m_Ref))
    		    {
    			len = CFStringGetLength(m_Ref);
    			m_Size = CFStringGetMaximumSizeForEncoding(len, kCFStringEncodingUTF8);
    			if(0 < m_Size && m_Size < 0x1000)
    			{
    			    m_pBuf = (char*)malloc(m_Size + 1);
    			    if(NULL != m_pBuf)
    			    {
    				memset(m_pBuf, 0, m_Size + 1);
    				CFStringGetCString(m_Ref, m_pBuf, m_Size, kCFStringEncodingUTF8);
    			    }
    			    else
    				LOG("%s::%s !!! memory allocation error %d %s !!!\n", typeid(this).name(), __func__, errno, strerror(errno));
    			}
    		    }
		}
	    } while (false);
	    return m_pBuf;
	}
};

typedef vector<string> STRVECTOR;

class CDevice
{
    private:
	// device name
	string m_sName;
	// policy name
	string m_sPol;
	// SC identifier
	int m_nSc;
	// partitions vector
	PMVECTOR m_Parts;
    private:
	void cp(const CDevice& in_d)
	{
	    m_sName = in_d.m_sName;
	    m_sPol = in_d.m_sPol;
	    m_nSc = in_d.m_nSc;
	    if(!m_Parts.empty())
		ReleaseParts();
	    // copy will not work when there are not enough room, i.e. simple iterator is a parameter
	    if(0 < in_d.m_Parts.size())
		copy(in_d.m_Parts.begin(), in_d.m_Parts.end(), back_inserter(m_Parts));
	}
    public:
	CDevice() {};
	CDevice(const char* in_pszName, const char* in_pszPol, int in_Sc = 0) : m_sName(in_pszName), m_sPol(in_pszPol), m_nSc(in_Sc){}
	CDevice(const CDevice& in_d) { cp(in_d); }
	void SetName(char* in_sName) { m_sName = in_sName; }
	void SetPol(char* in_sPol) { m_sPol = in_sPol; }
	void SetSC(int in_Sc) { m_nSc = in_Sc; }
	int Mount(const string& in_s) const
	{
	    LPPART_MAP map = NULL;
	    do
	    {
		map = operator[](in_s);
		if(NULL == map)
		    break;
		if(!map->bMnted)
		{
		    LOG("%s::%s %s %s\n", typeid(this).name(), __func__, Name().c_str(), in_s.c_str());
		    map->bMnted = true;
		}
	    } while (false);
	    return 0;
	}
	int Unmount(const string& in_s) const
	{
	    LPPART_MAP map = NULL;
	    do
	    {
		map = operator[](in_s);
		if(NULL == map)
		    break;
		if(map->bMnted)
		{
		    LOG("%s::%s %s %s\n", typeid(this).name(), __func__, Name().c_str(), in_s.c_str());
		    map->bMnted = false;
		}
	    } while (false);
	    return 0;
	}
	CDevice& operator=(const CDevice& in_d)
	{
	    if(this != &in_d)
		cp(in_d);
	    return *this;
	}
	void ReleaseParts()
	{
	    LPPART_MAP mp = NULL;
	    while(0 < m_Parts.size())
	    {
	        mp = m_Parts.back();
	        m_Parts.pop_back();
	        delete mp;
	    }
	}
	~CDevice()
	{
	    if(!m_Parts.empty())
		ReleaseParts();
	    m_sName.clear();
	    m_sPol.clear();
	}
	// must be released by caller
	operator CFDictionaryRef() const
	{
	    CFMutableDictionaryRef ref = NULL;
	    CFMutableArrayRef arrRef = NULL;
    	    CFNumberRef numRef = NULL;
    	    CFStringRef strRef = NULL;
	    bool b = true;
	    int i;
	    do
	    {
		ref = CFDictionaryCreateMutable(NULL, 0, &kCFTypeDictionaryKeyCallBacks, &kCFTypeDictionaryValueCallBacks);
		if(NULL == ref)
		{
		    LOG("%s::%s !!! CFDictionaryCreateMutable error !!!\n", typeid(this).name(), __func__);
		    break;
		}
		b = StrCrtAndSetInto(m_sName.c_str(), DEVICE, ref);
		if(!b)
		    break;
		b = StrCrtAndSetInto(m_sPol.c_str(), POLICY, ref);
		if(!b)
		    break;
		numRef = CFNumberCreate(NULL, kCFNumberSInt32Type, &m_nSc);
		if(NULL == numRef)
		{
		    LOG("%s::%s !!! CFNumberCreate error !!!\n", typeid(this).name(), __func__);
		    b = false;
		    break;
		}
		CFDictionarySetValue(ref, CFSTR(SC), numRef);
		CFRelease(numRef);
		if(0 < m_Parts.size())
		{
		    arrRef = CFArrayCreateMutable(NULL, 0, &kCFTypeArrayCallBacks);
		    if(NULL == arrRef)
		    {
			LOG("%s !!! CFArrayCreateMutable error !!!\n", __func__);
			break;
		    }
		    for(i = 0; i < (int)m_Parts.size(); i++)
		    {
			strRef = CFStringCreateWithCString(NULL, m_Parts[i]->sMntPt.c_str(), kCFStringEncodingASCII);
			if(NULL == strRef)
			{
    			    LOG("%s::%s !!! (0) CFStringCreateWithCString error !!!\n", typeid(this).name(), __func__);
    			    b = false;
    			    break;
			}
			CFArrayAppendValue(arrRef, (void*)strRef);
			CFRelease(strRef);
			strRef = CFStringCreateWithCString(NULL, m_Parts[i]->sDrive.c_str(), kCFStringEncodingASCII);
			if(NULL == strRef)
			{
    			    LOG("%s::%s !!! (1) CFStringCreateWithCString error !!!\n", typeid(this).name(), __func__);
    			    b = false;
    			    break;
			}
			CFArrayAppendValue(arrRef, (void*)strRef);
			CFRelease(strRef);
		    }
		    if(!b)
			CFRelease(arrRef);
		    else
			CFDictionarySetValue(ref, CFSTR(PARTITIONS), arrRef);
		}
	    } while(false);
	    if(!b && ref)
	    {
		CFRelease(ref);
		ref = NULL;
	    }
	    return ref;
	}
	bool operator==(const CDevice& in_d) const
	{
	    return 0 == strcmp(m_sName.c_str(), in_d.m_sName.c_str());
	}
	bool operator<(const CDevice& in_d) const
	{
	    return 0 > strcmp(m_sName.c_str(), in_d.m_sName.c_str());
	}
	operator int() const { return (int)m_Parts.size(); }
	operator STRVECTOR*() const	// must be released by caller
	{
	    int i;
	    string str;
	    STRVECTOR* svr = new (nothrow) STRVECTOR;
	    if(NULL == svr)
	    {
		LOG("%s::%s !!! memory allocation error %d %s !!!\n", typeid(this).name(), __func__, errno, strerror(errno));
		return NULL;
	    }
	    for(i = 0; i < (int)m_Parts.size(); i++)
	    {
		if(0 < m_Parts[i]->sMntPt.length() && 0 != m_Parts[i]->sMntPt.c_str()[0])
		{
		    str = m_Parts[i]->sMntPt;
		    svr->push_back(str);
		}
	    }
	    if(svr->empty())
	    {
		delete svr;
		svr = NULL;
	    }
	    return svr;
	}
	// it's not safely use a direct typecasting
	operator bool() const { return m_nSc != 0; }
	string Name() const { return m_sName; }
	string Policy() const { return m_sPol; }
	CDevice& operator -= (int i)
	{
	    LPPART_MAP map;
	    if(i < (int)m_Parts.size())
	    {
		map = m_Parts[i];
		LOG("%s::%s %llx\n", typeid(this).name(), __func__, (uint64_t)map);
		remove(m_Parts.begin(), m_Parts.end(), m_Parts[i]);
		map->sMntPt.clear();
		map->sDrive.clear();
		delete map;
	    }
	    return *this;
	}
	CDevice& operator -= (const string& s)
	{
	    LPPART_MAP map;
	    do
	    {
		map = operator[](s);
		if(NULL != map)
		{
		    LOG("%s::%s %llx\n", typeid(this).name(), __func__, (uint64_t)map);
		    remove(m_Parts.begin(), m_Parts.end(), map);
		    map->sMntPt.clear();
		    map->sDrive.clear();
		    delete map;
		}
	    } while (false);
	    return *this;
	}
	CDevice& operator -= (LPPART_MAP map)
	{
	    PMVECTOR::iterator it;
	    it = find(m_Parts.begin(), m_Parts.end(), map);
	    if(it != m_Parts.end())
	    {
		LOG("%s::%s %llx\n", typeid(this).name(), __func__, (uint64_t)map);
		remove(m_Parts.begin(), m_Parts.end(), *it);
		delete map;
	    }
	    return *this;
	}
	LPPART_MAP operator[](int in_idx) const
	{
	    if(m_Parts.empty())
		return NULL;
	    if(in_idx < 0 || in_idx >= (int)m_Parts.size())
		return NULL;
	    return m_Parts[in_idx];
	}
	LPPART_MAP operator[](const string& in_s) const
	{
	    int i;
	    LPPART_MAP map = NULL;
	    for(i = 0; i < (int)m_Parts.size(); i++)
	    {
		if(0 < m_Parts[i]->sMntPt.length() && 0 != m_Parts[i]->sMntPt.c_str()[0])
		{
		    if(0 == strcmp(m_Parts[i]->sMntPt.c_str(), in_s.c_str()))
		    {
			map = m_Parts[i];
			break;
		    }
		    else if(0 < m_Parts[i]->sDrive.length() && 0 != m_Parts[i]->sDrive.c_str()[0])
		    {
			if(0 == strcmp(m_Parts[i]->sDrive.c_str(), in_s.c_str()))
			{
			    map = m_Parts[i];
			    break;
			}
		    }
		}
	    }
	    return map;
	}
	bool IsMpPresented(const STRVECTOR* in_pvSv, const string& in_s)
	{
	    int i;
	    for(i = 0; i < (int)in_pvSv->size(); i++)
	    {
		if(0 == strcmp((*in_pvSv)[i].c_str(), in_s.c_str()))
		{
		    return true;
		}
	    }
	    return false;
	}
	// partitions merging with the analyzing
	CDevice& operator += (const CDevice& in_d)
	{
	    int i;
	    STRVECTOR* prev, *post;
	    bool bMnt = this->operator bool();
	    LPPART_MAP map;
	    do
	    {
		// both must be SC-ed, or un-SC-ed
		LOG("%s::%s %s %s\n", typeid(this).name(), __func__, m_sName.c_str(), in_d.m_sName.c_str());
		ASSERT(bMnt == in_d.operator bool());
		prev = operator STRVECTOR*();
		if(NULL == prev)
		{
		    LOG("%s::%s !!! (0) operator STRVECTOR error !!!\n", typeid(this).name(), __func__);
		    break;
		}
		post = in_d.operator STRVECTOR*();
		if(NULL == post)
		{
		    LOG("%s::%s !!! (1) operator STRVECTOR error !!!\n", typeid(this).name(), __func__);
		    break;
		}
		// dismount partition, when it's needed
		for(i = 0; i < (int)prev->size(); i++)
		{
		    if(!IsMpPresented(post, (*prev)[i]))
		    {
			if(bMnt)
			    Unmount((*prev)[i]);
			// remove partition
			*this -= (*prev)[i];
		    }
		}
		// mount partition, when it's needed
		for(i = 0; i < (int)post->size(); i++)
		{
		    if(!IsMpPresented(prev, (*post)[i]))
		    {
			// insert partition
			map = in_d.operator []((*post)[i]);
			ASSERT(NULL != map);
			m_Parts.push_back(map);
			// 2-ry mount is excluded
			if(bMnt)
			    Mount((*post)[i]);
		    }
		}
	    } while (false);
	    if(NULL != prev)
	    {
		prev->clear();
		delete prev;
	    }
	    if(NULL != post)
	    {
		post->clear();
		delete post;
	    }
	    return *this;
	}
	bool SetPartMap(const char* in_pszMnt, const char* in_pszDrv)
	{
	    LPPART_MAP map = new (nothrow) PART_MAP;
	    //LOG("%s::%s map %llx\n", typeid(this).name(), __func__, (uint64_t)map);
	    if(NULL == map)
	    {
		LOG("%s::%s !!! memory allocation error %d %s\n", typeid(this).name(), __func__, errno, strerror(errno));
		return false;
	    }
	    map->sMntPt = in_pszMnt;
	    map->sDrive = in_pszDrv;
	    m_Parts.push_back(map);
	    return true;
	}
};

typedef vector<CDevice*> DEVVECTOR;
typedef pair<CDevice*, CDevice*> DEVPAIR;
typedef vector<DEVPAIR> PAIRDVEC;

// handler entity
class CHandler
{
    private:
	DEVVECTOR m_vDevs;
	CFArrayRef m_Arr;
    private:
	CHandler();
	CHandler& operator=(const CHandler&);
	CHandler(const CHandler&);
	CDevice* genDevice(CFDictionaryRef in_Ref)
	{
	    int m;
	    CFStringRef sName = NULL, sPol = NULL;
	    CFArrayRef parts = NULL;
	    CFNumberRef nNum = NULL;
	    CDevice* dev = NULL;
	    CFStringRef sDisk = NULL, sMnt = NULL, stDscr = NULL;
	    if((NULL != in_Ref) && (CFDictionaryGetTypeID() == CFGetTypeID(in_Ref)))
	    {
		// device name
		sName = (CFStringRef)CFDictionaryGetValue(in_Ref, CFSTR(DEVICE));
		if(NULL != sName && CFStringGetTypeID() == CFGetTypeID(sName))
		    LOG("%s::%s %s %s\n", typeid(this).name(), __func__, DEVICE, (char*)CString(sName));
		else
		{
		    LOG("%s::%s !!! No device name %llx !!!\n", typeid(this).name(), __func__, (uint64_t)sName);
		    return dev;
		}
		// policy name
		sPol = (CFStringRef)CFDictionaryGetValue(in_Ref, CFSTR(POLICY));
		if(NULL != sPol && CFStringGetTypeID() == CFGetTypeID(sPol))
		    LOG("%s::%s %s %s\n", typeid(this).name(), __func__, POLICY, (char*)CString(sPol));
		else
		{
		    LOG("%s::%s !!! No policy %llx !!!\n", typeid(this).name(), __func__, (uint64_t)sPol);
		    return dev;
		}
		// SC identifier
		nNum = (CFNumberRef)CFDictionaryGetValue(in_Ref, CFSTR(SC));
		if(NULL != nNum && CFNumberGetTypeID() == CFGetTypeID(nNum))
		{
		    CFNumberGetValue(nNum, CFNumberGetType(nNum), &m);
	    	    LOG("%s::%s %s %d\n", typeid(this).name(), __func__, SC, m);
		}
	        dev = new (nothrow) CDevice((char*)CString(sName), (char*)CString(sPol), 0 < m);
	        if(NULL == dev)
	        {
	    	    LOG("%s::%s !!! device allocation error !!!\n", typeid(this).name(), __func__);
	    	    return NULL;
	        }
	        // partitions
	        parts = (CFArrayRef)CFDictionaryGetValue(in_Ref, CFSTR(PARTITIONS));
		if(NULL != parts && CFArrayGetTypeID() == CFGetTypeID(parts))
                {
                    for(m = 0; m < CFArrayGetCount(parts); m+=2)
                    {
                        sMnt = (CFStringRef)CFArrayGetValueAtIndex(parts, m);
                	sDisk = (CFStringRef)CFArrayGetValueAtIndex(parts, m + 1);
                	LOG("%s::%s %s %s\n", typeid(this).name(), __func__, (char*)CString(sDisk), (char*)CString(sMnt));
                	if(!dev->SetPartMap((char*)CString(sMnt), (char*)CString(sDisk)))
                	{
                	    LOG("%s::%s !!! SetPartMap for device %s [%d] error !!!\n", typeid(this).name(), __func__, dev->Name().c_str(), m);
                	    delete dev;
                	    dev = NULL;
                	    break;
                	}
                    }
                }
                else
            	    LOG("%s::%s device %s has no partitions\n", typeid(this).name(), __func__, dev->Name().c_str());
	    }
	    return dev;
	}
	CDevice* getDevice(const DEVVECTOR& in_DevV, const string& in_s) const
	{
	    int i, j;
	    CDevice* dev;
	    LPPART_MAP map;
	    if(in_DevV.empty())
		return NULL;
	    if(0 == in_s.length())
		return NULL;
	    for(i = 0; i < (int)in_DevV.size(); i++)
	    {
		dev = in_DevV[i];
		for(j = 0; j < dev->operator int(); j++)
		{
		    map = dev->operator[](j);
		    if(0 < map->sMntPt.length())
		    {
			if(0 == strcmp(map->sMntPt.c_str(), in_s.c_str()))
			    return dev;
		    }
		    if(0 < map->sDrive.length())
		    {
			if(0 == strcmp(map->sDrive.c_str(), in_s.c_str()))
			    return dev;
		    }
		}
	    }
	    return NULL;
	}
	void fromArray(CFArrayRef in_Arr, DEVVECTOR& inou_dV)
	{
	    int i;
	    CDevice* dev = NULL;
	    do
	    {
		if((NULL != in_Arr) && (CFArrayGetTypeID() == CFGetTypeID(in_Arr)))
		{
		    LOG("%s::%s %d\n", typeid(this).name(), __func__, (int)CFArrayGetCount(in_Arr));
		    for(i = 0; i < (int)CFArrayGetCount(in_Arr); i++)
		    {
			dev = genDevice((CFDictionaryRef)CFArrayGetValueAtIndex(in_Arr, i));
			if(NULL == dev)
			    break;
			inou_dV.push_back(dev);
		    }
		}
		else
		    LOG("%s::%s %s\n", typeid(this).name(), __func__, NULL != in_Arr ? "!!! An invalid object inheritance !!!" : "Zero or empty dictionary");
	    } while(false);
	}
	CDevice* operator[](const string& in_s) const
	{
	    return getDevice(m_vDevs, in_s);
	}
	void mountFromDevices(STRVECTOR& inou_MntV, const DEVVECTOR& in_DevV)
	{
	    int i;
	    STRVECTOR* tmp = NULL;
	    for(i = 0; i < (int)in_DevV.size(); i++)
	    {
		tmp = in_DevV[i]->operator STRVECTOR*();
		if(NULL != tmp)
		{
		    if(!tmp->empty())
		    {
			copy(tmp->begin(), tmp->end(), back_inserter(inou_MntV));
		    	tmp->clear();
		    }
		    delete tmp;
		    tmp = NULL;
		}
	    }
	}
	// must be released by caller
	operator STRVECTOR*()
	{
	    STRVECTOR* pvret = NULL;
	    do
	    {
		pvret = new (nothrow) STRVECTOR;
		if(NULL == pvret)
		{
		    LOG("%s::%s !!! Object allocation error %d %s !!!\n", typeid(this).name(), __func__, errno, strerror(errno));
		    break;
		}
		mountFromDevices(*pvret, m_vDevs);
	    } while (false);
	    return pvret;
	}
	void mountAll(STRVECTOR* in_pVec) const
	{
	    int i;
	    string s;
	    CDevice* dev = NULL;
	    for(i = 0; i < (int)in_pVec->size(); i++)
	    {
		// with no range checking, at(i) do it
	        s = (*in_pVec)[i];
	        // project syntax agreement: with this operation our devices only can be obtained
	        dev = operator [](s);
	        if(NULL == dev)
	        {
	    	    LOG("%s::%s !!! Can't find device for mount point %s !!!\n", typeid(this).name(), __func__, s.c_str());
	    	    continue;
	        }
	        // is the SC assigned for our device ?
	        if(dev->operator bool())
	        {
	    	    LOG("%s::%s mount %s\n", typeid(this).name(), __func__, s.c_str());
	    	    dev->Mount(s);
	    	}
	    	else
	    	    LOG("%s::%s %s SC doesn't assigned %s\n", typeid(this).name(), __func__, dev->Name().c_str(), s.c_str());
	    }
	}
	void clean(DEVVECTOR& inou_DevV, int Where)
	{
	    int i;
	    CDevice* dev;
	    for(i = 0; i < inou_DevV.size(); i++)
	    {
		dev = inou_DevV.back();
		inou_DevV.pop_back();
		delete dev;
	    }
	    inou_DevV.clear();
	}
	void unmountAll()
	{
	    int i, j;
	    STRVECTOR* svec = NULL;
	    for(i = 0; i < (int)m_vDevs.size(); i++)
	    {
		svec = m_vDevs[i]->operator STRVECTOR*();
		if(NULL != svec)
		{
		    if(!svec->empty())
		    {
			for(j = 0; j < (int)svec->size(); j++)
			{
			    m_vDevs[i]->Unmount((*svec)[j]);
			}
		    }
		    svec->clear();
		    delete svec;
		}
	    }
	}
	int findDev(const DEVVECTOR& in_Dv, const CDevice* in_pDev) const
	{
	    int i;
	    for(i = 0; i < (int)in_Dv.size(); i++)
	    {
		if(*in_Dv[i] == *in_pDev)
		    return i;
	    }
	    return -1;
	}
	// find can't work with the const references, and works too slowly
	void makePairs(PAIRDVEC& inou_PrVec, DEVVECTOR& in_Dv0, DEVVECTOR& in_Dv1)
	{
	    int i, j;
	    // 1-st pass, 0-th vector handling
	    for(i = 0; i < (int)in_Dv0.size(); i++)
	    {
		j = findDev(in_Dv1, in_Dv0[i]);
		// real/real devices pair
		if(0 >= j)
		    inou_PrVec.push_back(make_pair(in_Dv0[i], in_Dv1[j]));
		else	// real/fake devices pair
		    inou_PrVec.push_back(make_pair(in_Dv0[i], (CDevice*)NULL));
	    }
	    // 2-nd pass, only absending devices into the 1-st vector
	    for(i = 0; i < (int)in_Dv1.size(); i++)
	    {
		j = findDev(in_Dv0, in_Dv1[i]);
		if(0 > j)	// fake/real devices pair
		    inou_PrVec.push_back(make_pair((CDevice*)NULL, in_Dv1[i]));
	    }
	    ASSERT(0 < inou_PrVec.size());
	}
	void unmount(CDevice* inou_Dev)
	{
	    int i, cnt;
	    LPPART_MAP map;
	    cnt = inou_Dev->operator int();
	    for(i = 0; i < cnt; i++)
	    {
	        map = inou_Dev->operator[](i);
	        if(!map)
	    	    continue;
	    	inou_Dev->Unmount(map->sMntPt);
	    }
	    inou_Dev->ReleaseParts();
	}
	void mount(CDevice* inou_Dev)
	{
	    int i, cnt;
	    LPPART_MAP map;
	    cnt = inou_Dev->operator int();
	    for(i = 0; i < cnt; i++)
	    {
	        map = inou_Dev->operator[](i);
	        if(!map)
	    	    continue;
	    	inou_Dev->Mount(map->sMntPt);
	    }
	}
	// It's a valid case, on start device is SC relates, in future partitions are kept mounted, but SC identifier dropped into the policy
	void compare(CDevice* inou_Dev0, CDevice* inou_Dev1)
	{
	    // SC identifier is off newly, unmount needed
	    if(inou_Dev0->operator bool() && !inou_Dev1->operator bool())
	    {
		unmount(inou_Dev0);
	    }
	    // SC identifier is on newly, mount needed, mount points set invalidation needed
	    else if(!inou_Dev0->operator bool() && inou_Dev1->operator bool())
	    {
		// partitions set is being substituted fully
		*inou_Dev0 = *inou_Dev1;
		mount(inou_Dev0);
	    }
	    // SC is kept for both, or non kept for both, mount points set invalidation needed
	    else
	    {
		// partitions merging
		*inou_Dev0 += *inou_Dev1;
	    }
	}
	void doCompare(DEVVECTOR& ext_V)
	{
	    int i;
	    PAIRDVEC pav;
	    DEVVECTOR rmv;
	    CDevice* dev;
	    ASSERT(0 < m_vDevs.size() && 0 < ext_V.size());
	    makePairs(pav, m_vDevs, ext_V);
	    ASSERT(0 < pav.size());
	    for(i = 0; i < (int)pav.size(); i++)
	    {
		// device is kept, SC identifier checking needed, mount points checking needed
		ASSERT(NULL != pav[i].first || NULL != pav[i].second);
		if(NULL != pav[i].first && NULL != pav[i].second)
		{
		    compare(pav[i].first, pav[i].second);
		}
		// device is inserted
		else if(NULL == pav[i].first && NULL != pav[i].second)
		{
		    m_vDevs.push_back(pav[i].second);
		    if(pav[i].second->operator bool())
		    {
			mount(pav[i].second);
		    }
		}
		// device is ejected
		else if(NULL != pav[i].first && NULL == pav[i].second)
		{
		    // unmount when it's needed
		    if(pav[i].first->operator bool())
			unmount(pav[i].first);
		    // insert into the removed devices list
		    rmv.push_back(pav[i].first);
		}
	    }
	    LOG("%s::%s removed count %d\n", typeid(this).name(), __func__, (int)rmv.size());
	    while(!rmv.empty())
	    {
		dev = rmv.back();
		rmv.pop_back();
		remove(m_vDevs.begin(), m_vDevs.end(), dev);
		delete dev;
	    }
	    pav.clear();
	    LOG("%s::%s --\n", typeid(this).name(), __func__);
	}
    public:
	CHandler(CFArrayRef in_Arr) : m_Arr(in_Arr)
	{
	    LOG("%s::%s\n", typeid(this).name(), __func__);
	    fromArray(m_Arr, m_vDevs);
	    if(0 < m_vDevs.size())
		Handle((CFArrayRef)NULL);
	}
	~CHandler()
	{
	    clean(m_vDevs, 0);
	    if(NULL != m_Arr)
		CFRelease(m_Arr);
	}
	void Handle(CFArrayRef in_nArr)
	{
	    STRVECTOR* mntV_our = NULL, *mntV_the = NULL;
	    DEVVECTOR devs_the;

	    do
	    {
		// mount handler only, when partitions are presented
		if(in_nArr == m_Arr || NULL == in_nArr)
		{
		    mntV_our = operator STRVECTOR*();
		    if(NULL == mntV_our)
		    {
			LOG("%s::%s !!! STRVECTOR error !!!\n", typeid(this).name(), __func__);
			break;
		    }
		    ASSERT(NULL != m_Arr);
		    // for now it's a stub only
		    mountAll(mntV_our);
		    mntV_our->clear();
		    delete mntV_our;
		    return;
		}
		// new devices vector generating
		fromArray(in_nArr, devs_the);
		if(!devs_the.empty())
		{
		    if(!m_vDevs.empty())
		    {
			// set_intersection and set_difference are expensive currently, because data sets are small
			doCompare(devs_the);
		    }
		    // the singular case, all devices must be mounted
		    else
		    {
			LOG("%s::%s device storage is empty\n", typeid(this).name(), __func__);
			copy(devs_the.begin(), devs_the.end(), back_inserter(m_vDevs));
			mntV_the = operator STRVECTOR*();
			if(NULL == mntV_the)
			{
			    LOG("%s::%s !!! STRVECTOR error !!!\n", typeid(this).name(), __func__);
			    break;
			}
			mountAll(mntV_the);
			break;
		    }
		}
		// the singular case, all devices must be dismounted and released
		else
		{
		    LOG("%s::%s there are no new devices\n", typeid(this).name(), __func__);
		    if(!m_vDevs.empty())
		    {
			unmountAll();
			clean(m_vDevs, 1);
		    }
		}
	    } while (false);
	    if(NULL != mntV_our)
	    {
		if(!mntV_our->empty())
		    mntV_our->clear();
		delete mntV_our;
	    }
	    if(NULL != mntV_the)
	    {
		if(!mntV_the->empty())
		    mntV_the->clear();
		delete mntV_the;
	    }
	    // instead of clean, becouse all contents will be into the class storage
	    devs_the.clear();
	    // dictionaries array invalidation
	    if(in_nArr != m_Arr && NULL != in_nArr)
	    {
		if(NULL != m_Arr)
		    CFRelease(m_Arr);
		m_Arr = in_nArr;
	    }
	    LOG("%s::%s --\n", typeid(this).name(), __func__);
	}
};

CFArrayRef GetArray(CDevice* in_pDev)
{
    CFDictionaryRef dict = NULL;
    CFMutableArrayRef arr = CFArrayCreateMutable(NULL, 0, &kCFTypeArrayCallBacks);
    if(NULL == arr)
    {
	LOG("%s !!! CFArrayCreateMutable error !!!\n", __func__);
	return NULL;
    }
    dict = (CFDictionaryRef)(*in_pDev);
    ASSERT(NULL != dict);
    CFArrayAppendValue(arr, (void*)dict);
    CFRelease(dict);
    return arr;
}

CFArrayRef GetArray(DEVVECTOR& in_arDev)
{
    CFDictionaryRef dict = NULL;
    CFMutableArrayRef arr = NULL;
    int i;
    do
    {
	arr = CFArrayCreateMutable(NULL, 0, &kCFTypeArrayCallBacks);
	if(NULL == arr)
	{
	    LOG("%s !!! CFArrayCreateMutable error !!!\n", __func__);
	    return NULL;
	}
	for(i = 0; i < (int)in_arDev.size(); i++)
	{
	    dict = (CFDictionaryRef)(*in_arDev[i]);
	    ASSERT(NULL != dict);
	    CFArrayAppendValue(arr, (void*)dict);
	    CFRelease(dict);
	}
    } while (false);
    return arr;
}

int main(int argc, char* argv[])
{
    DEVVECTOR dvec;
    CFMutableArrayRef arr = NULL, arrn = NULL;
    CDevice dev("YUH", "New_Policy", 0);
    dev.SetPartMap("/Volumes/NO NAME", "/dev/disk0s1");
    //arr = (CFMutableArrayRef)GetArray(&dev);
    //ASSERT(NULL != arr);
    //CHandler hdl(NULL); // arr
    //dev.SetPartMap("/Volumes/NO HERE", "/dev/disk0s2");
    //dev.Unmount("/Volumes/NO HERE");
    dvec.push_back(&dev);
    dev.SetSC(1);
    CDevice dn("HUI", "New_Policy", 0);
    dn.SetPartMap("/Volumes/VOILE", "/dev/disk1s1");
    dvec.push_back(&dn);
    //LOG("%s %llx\n", __func__, (uint64_t)&dn);
    arrn = (CFMutableArrayRef)GetArray(dvec);
    ASSERT(NULL != arrn);
    dvec.clear();
    CHandler hdl(arrn);
    //hdl.Handle(arrn);
    return 0;
}
