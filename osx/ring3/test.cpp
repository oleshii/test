#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <netdb.h>
#include <ifaddrs.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <iconv.h>
#include <assert.h>
#include <sys/stat.h>
#include <ntsid.h>
#include <membership.h>
#include <pwd.h>
#include <tzfile.h>
#include <fcntl.h>

#include <vector>
#include <time.h>
#include <typeinfo>

#include <guid.h>
#include <platform.h>
#include <zlck_common.h>

#include <stdafx.h>
#include <zlib_dll.h>
#include <ZLib_String.h>
#include <zlib_buffer.h>
#include <zlib_value.h>
#include <ZLib_File.h>

#include <zws_open_ssl.h>
#include <zws_client.h>
#include <zntf_protocol.h>

#define LOG	printf
#include <debug.h>

#include <xmlParser.h>

#include "../../diff_stuff/zlib_std/zlib/zlib.h"

#define	LOCAL_ADDR	0x100007f
#define	INVALID_ADDR	INADDR_ANY

#define ZERO_BYTE(v)		((v & 0xff000000) >> 24)
#define FIRST_BYTE(v)		((v & 0xff0000) >> 16)
#define SECOND_BYTE(v)		((v & 0xff00) >> 8)
#define THIRD_BYTE(v)		((v & 0xff))

#define GET_SOCK_TYPE(t)\
t == 0 ? "Unspecified":\
t == SOCK_STREAM ? "SOCK_STREAM":\
t == SOCK_DGRAM ? "SOCK_DGRAM":\
t == SOCK_RAW ? "SOCK_RAW":\
t == SOCK_RDM ? "SOCK_RDM":\
t == SOCK_SEQPACKET ? "SOCK_SEQPACKET" : "UNKNOWN"

#define GET_PROTO(p)\
p == 0 ? "Unspecified":\
p == IPPROTO_TCP ? "TCP":\
p == IPPROTO_UDP ? "UDP": "UNKNOWN"

#define GET_FAMILY(f)\
f == AF_UNSPEC ? "AF_UNSPEC":\
f == AF_UNIX ? "AF_UNIX | AF_LOCAL":\
f == AF_INET ? "AF_INET":\
f == AF_IMPLINK ? "AF_IMPLINK":\
f == AF_PUP ? "AF_PUP":\
f == AF_CHAOS ? "AF_CHAOS":\
f == AF_NS ? "AF_NS":\
f == AF_OSI ? "AF_OSI | AF_ISO":\
f == AF_ECMA ? "AF_ECMA":\
f == AF_DATAKIT ? "AF_DATAKIT":\
f == AF_CCITT ? "AF_CCITT":\
f == AF_SNA ? "AF_SNA":\
f == AF_DECnet ? "AF_DECnet":\
f == AF_DLI ? "AF_DLI":\
f == AF_LAT ? "AF_LAT":\
f == AF_HYLINK ? "AF_HYLINK":\
f == AF_APPLETALK ? "AF_APPLETALK":\
f == AF_ROUTE ? "AF_ROUTE":\
f == AF_LINK ? "AF_LINK":\
f == pseudo_AF_XTP ? "pseudo_AF_XTP":\
f == AF_COIP ? "AF_COIP":\
f == AF_CNT ? "AF_CNT":\
f == pseudo_AF_RTIP ? "pseudo_AF_RTIP":\
f == AF_IPX ? "AF_IPX":\
f == AF_SIP ? "AF_SIP":\
f == pseudo_AF_PIP ? "pseudo_AF_PIP":\
f == AF_NDRV ? "AF_NDRV":\
f == AF_ISDN ? "AF_ISDN | AF_E164":\
f == pseudo_AF_KEY ? "pseudo_AF_KEY":\
f == AF_INET6 ? "AF_INET6":\
f == AF_NATM ? "AF_NATM":\
f == AF_SYSTEM ? "AF_SYSTEM | AF_NETGRAPH":\
f == AF_NETBIOS ? "AF_NETBIOS":\
f == AF_PPP ? "AF_PPP":\
f == pseudo_AF_HDRCMPLT ? "pseudo_AF_HDRCMPLT":\
f == AF_RESERVED_36 ? "AF_RESERVED_36":\
f == AF_IEEE80211 ? "AF_IEEE80211":\
f == AF_UTUN ? "AF_UTUN":\
f == AF_MAX ? "AF_MAX": "UNKNOWN"

#define	ASSERT	assert

int get_contents(const char* in_fname, char** ppdata, size_t* plen)
{
    int fd;
    size_t len;
    char* psz;
    struct stat statbuf = {0};

    fd = open(in_fname, O_RDONLY);
    if(0 > fd)
    {
        LOG("%s open file %s error %d %s\n", __func__, in_fname, errno, strerror(errno));
        return -1;
    }
    if(0 > fstat(fd, &statbuf))
    {
        LOG("%s fstat error %d %s\n", __func__, errno, strerror(errno));
        close(fd);
        return -2;
    }
    if((0 == (statbuf.st_mode & S_IFREG)) || (0 == statbuf.st_size))
    {
        LOG("%s an file %s is invalid\n", __func__, in_fname);
        close(fd);
        return -3;
    }
    psz = (char*)malloc(statbuf.st_size);
    if(NULL == psz)
    {
        LOG("%s memory allocation error %d %s\n", __func__, errno, strerror(errno));
        close(fd);
        return -4;
    }
    memset(psz, 0, statbuf.st_size);
    len = read(fd, psz, statbuf.st_size);
    if((int)len != statbuf.st_size)
    {
        LOG("%s read error %d %s\n", __func__, errno, strerror(errno));
        free(psz);
	close(fd);
	return -5;
    }
    close(fd);
    *plen = len;
    *ppdata = psz;
    return 0;
}

#ifndef GZ_SUFFIX
#  define GZ_SUFFIX ".gz"
#endif
#define SUFFIX_LEN (sizeof(GZ_SUFFIX)-1)

#define BUFLEN      16384
#define MAX_NAME_LEN 1024

int gz_compress(FILE* in, gzFile out)
{
    char buf[BUFLEN];
    int len;
    int err;
    for (;;) {
	len = (int)fread(buf, 1, sizeof(buf), in);
        if (ferror(in)) {
    	    LOG("%s !!! read error %d %s !!!\n", __func__, errno, strerror(errno));
            return -1;
        }
	if (len == 0)
	    break;
        if (gzwrite(out, buf, (unsigned)len) != len)
    	    LOG("%s !!! %s !!!\n", __func__, gzerror(out, &err) ? gzerror(out, &err) : strerror(errno));
    }
    fclose(in);
    if (gzclose(out) != Z_OK)
    {
	LOG("%s failed gzclose\n", __func__);
	return -2;
    }
    return 0;
}

int file_compress(char* file, char* mode)
{
    char outfile[MAX_NAME_LEN];
    FILE  *in;
    gzFile out;

    if (strlen(file) + strlen(GZ_SUFFIX) >= sizeof(outfile)) {
	LOG("%s !!! filename too long !!!\n", __func__);
        return -1;
    }
    strcpy(outfile, file);
    strcat(outfile, GZ_SUFFIX);
    in = fopen(file, "rb");
    if (in == NULL) {
	LOG("%s !!! open file %s error %d %s !!!\n", __func__, file, errno, strerror(errno));
        return -2;
    }
    out = gzopen(outfile, mode);
    if (out == NULL) {
        LOG("%s !!! can't gzopen %s !!!\n", __func__, outfile);
        return -3;
    }
    if(gz_compress(in, out))
	return -4;
    return 0;
}

#define	CTTYPE	"Content-Type: multipart/form-data; boundary="
#define	HTOK	"http://"
#define	METAEXT	".info"
#define HTTP	"http://%s"
#define	HOST	"Host: "
#define	CONNCT	"Connection: Keep-Alive\r\n"
#define	CTYPEF	"Content-Type: multipart/form-data; boundary=---------------------------%llu\r\n"
#define	CLENGTH	"Content-Length: %d\r\n\r\n"
#define	CLEN	"Content-Length: %d\r\n"			// in multipart
#define	FLEVEL	"-----------------------------%llu\r\n"
#define	ILEVEL	"-----------------------------%llu--\r\n"	// boundary in multipart
#define	MPLEVEL	"-----------------------------%llu----\r\n"	// last in multipart
#define	LLEVEL	"\r\n-----------------------------%llu--\r\n"
#define	CDISP	"Content-Disposition: form-data; name=\"file1\"; filename=\"%s\"\r\n"
#define	CDISP1	"Content-Disposition: form-data; name=\"FILEDATA\"; filename=\"%s\"\r\n"
#define	REFER	"Referer: "
#define	CTYPEL	"Content-Type: application/octet-stream\r\n\r\n"
#define	ACCENC	"Accept-Encoding: gzip, deflate\r\n"
#define	SR	"\r\n"
#define	EOR	SRSR
#define	CTTENC	"Content-Transfer-Encoding: binary\r\n"
#define	CTTXT	"Content-Type: text/plain\r\n"
#define	UAGENT	"User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0\r\n"
#define	AGENT	"User-Agent: Apache-HttpClient/4.2.6 (java 1.5)\r\n"

// reading meta and data files, data compression, compressed data reading
class	CFileHandler
{
#define	META_STRINGS	8
private:
    CFileHandler();
    CFileHandler(CFileHandler&);
    CFileHandler(const CFileHandler&);
    CFileHandler& operator=(const CFileHandler&);
    CFileHandler& operator=(CFileHandler);
private:
    ZLib_StringA m_sMeta;
    ZLib_StringA m_sCompess;
    ZLib_StringA m_DataName;
    ZLib_StringA m_MetaName;
private:
    static ZLib_StringA getHost()
    {
	ZLib_StringA sret;
	char asHostName[0x40] = {0};
	do
	{
	    if(0 > gethostname(&asHostName[0], sizeof(asHostName)))
	    {
		LOG("%s gethostname error %d %s\n", __func__, errno, strerror(errno));
		break;
	    }
	    sret = asHostName;
	} while (false);
	return sret;
    }
    // does file exist
    bool isExist(const ZLib_StringA& in_sName) const { return 0 == access(in_sName.Get_Data(), O_RDONLY); }
    void getDateTime(ZLib_StringA& Date, ZLib_StringA& Time, time_t in_DateTime)
    {
	char szTime[0x20] = {0};
	struct tm* timeptr = localtime((const time_t*)&in_DateTime);
	ASSERT(NULL != timeptr && "localtime error\n");
	sprintf(szTime, "%02d.%02d.%d", timeptr->tm_mday, 1 + timeptr->tm_mon, TM_YEAR_BASE + timeptr->tm_year);
	Date = szTime;
	bzero(szTime, sizeof(szTime));
	sprintf(szTime, "%02d:%02d:%02d", timeptr->tm_hour, timeptr->tm_min, timeptr->tm_sec);
	Time = szTime;
	//LOG("%s %s %s\n", __func__, Time.Get_Data(), Date.Get_Data());
    }
    int compressData(char* psz, size_t sz, ZLib_StringA& inou_sData)
    {
	ZLib_StringA stmp;
	psz = NULL;
	sz = 0;
    	// file compression
	if(file_compress(const_cast<char*>((const char*)m_DataName), const_cast<char*>("wb 6 ")))
	{
	    LOG("%s !!! compression error !!!\n", __func__);
	    return -1;
	}
	stmp = m_DataName + GZ_SUFFIX;
	// compressed file reading
	if(0 != get_contents((LPCSTR)stmp, &psz, &sz))
	    return -2;
	ASSERT(NULL != psz && 0 < sz && "Invalid reading");
	inou_sData = ZLib_StringA(psz, sz);
	free(psz);
	return 0;
    }
    // meta file handling, convert time to string, user id to string
    int parseMeta(char* in_psz, size_t in_sz, ZLib_StringA& sMeta)
    {
	int r = 0, i;
	char* tmp;
	time_t time;
	struct passwd* pass = NULL;
	ZLib_StringA sdate, stime;
	do
	{
	    for(i = 0, tmp = in_psz; i < META_STRINGS; ++i)
	    {
		char* psz = strchr(tmp, '\n');
		if(NULL == psz)
		{
		    LOG("%s !!! Invalid meta file format !!!\n", __func__);
		    r = -1;
		    break;
		}
		// saved operation date/time
		if(2 == i)
		{
		    tmp = const_cast<char*>("\n");
		    time = strtoul(psz + 1, &tmp, 10);
		    //LOG("%s localtime %lu\n", __func__, time);
		    getDateTime(sdate, stime, time);
		    //LOG("%s %s %s\n", __func__, (const char*)sdate, (const char*)stime);
		    sMeta = ZLib_StringA(in_psz, (int)(psz - in_psz + 1)); // EOL taken in attention
		    sMeta += sdate + " " + stime + '\n';
		    //LOG("%s %s", __func__, (const char*)sMeta);
		    stime.Empty();
		    sdate.Empty();
		}
		// user id
		else if(3 == i)
		{
		    tmp = const_cast<char*>("\n");
		    time = strtoul(psz + 1, &tmp, 10);
		    //LOG("%s uid %lu\n", __func__, time);
		    pass = getpwuid((int)time);
		    if(NULL == pass)
		    {
			LOG("%s !!! user id %d search error %d %s\n", __func__, (int)time, errno, strerror(errno));
			r = -2;
			break;
		    }
		    stime.Format("%s", pass->pw_name);
		    sMeta += stime;
		    stime.Empty();
		    // fill data until end
		    sMeta += strchr(++psz, '\n');
		    //LOG("%s", (const char*)sMeta);
		    break;
		}
		tmp = ++psz;
	    }
	} while (false);
	return r;
    }
    int metaToXML(ZLib_StringA& inou_sMeta)
    {
	int ret = 0, i, j;
	char* psz = NULL, *tmp, *cur = NULL;
	XMLNode xMainNode, xNode, xmsgNode;
	ZLib_StringA host, sdate, stime;
	ZLibT_Array<ZLib_StringA> arzStr;

	do
	{
	    host = getHost();
	    if(host.Is_Empty())
	    {
		ret = -1;
		break;
	    }
	    host += SR;
	    arzStr.Add(host);
	    //LOG("%s\n", __func__);
	    for(i = 0, j = 0, tmp = const_cast<char*>(inou_sMeta.Get_Data()); i < META_STRINGS; i++)
	    {
		if(2 == i)
		{
		    host = "";
		    while('\n' != tmp[j])
			host += tmp[j++];
		    host += SR;
		    //LOG("YUH %s", (const char*)host);
		    arzStr.Add(host);
		}
		cur = strchr(tmp, '\n');
		//LOG("%s [%d] %llx\n", __func__, i, (unsigned long long)cur);
		if(NULL == cur)
		{
		    LOG("%s !!! invalid meta data !!!\n", __func__);
		    ret = -2;
		    break;
		}
		// saved operation date/time
		if(2 == i)
		{
		    char* p = strchr(cur, ' ');
		    ASSERT(NULL != p && "Bad date/time format");
		    sdate = ZLib_StringA(cur + 1, (int)(p - cur));
		    sdate += SR;
		    while(*(++p) != '\n')
			stime += *p;
		    stime += SR;
		    //LOG("date %s", (const char*)sdate);
		    //LOG("time %s",(const char*)stime);
		    arzStr.Add(sdate);
		    arzStr.Add(stime);
		    cur = p - 1;
		}
		else
		{
		    if(3 != i)
		    {
			host = ZLib_StringA(tmp, (int)(cur + 1 - tmp));
			arzStr.Add(host);
		    }
		}
		cur++;
		tmp = cur;
	    }
	    if(0 == ret)
	    {
		//for(i = 0; i < (int)arzStr.Get_Num_Elements(); i++)
		    //LOG("[%d] %s", i, (const char*)arzStr.Get_At(i));
	    	xMainNode = XMLNode::createXMLTopNode("xml", TRUE);
		xMainNode.addAttribute("version", "1.0");
		xMainNode.addAttribute("encoding", "UTF-8");
		xMainNode.addAttribute("standalone", "yes");
		xNode = xMainNode.addChild("Bunch");
		xmsgNode = xNode.addChild("Message");
		xmsgNode.addAttribute(ZNTF_PID_str_path, arzStr.Get_At(1));
		xmsgNode.addAttribute(ZNTF_PID_str_file_size, arzStr.Get_At(3));
		xmsgNode.addAttribute(ZNTF_PID_str_computer, arzStr.Get_At(0));
		xmsgNode.addAttribute(ZNTF_PID_str_date, arzStr.Get_At(4));
		// file name in shadow copy isn't required
		xmsgNode.addAttribute(ZNTF_PID_str_user, arzStr.Get_At(6));
		xmsgNode.addAttribute(ZNTF_PID_str_time, arzStr.Get_At(5));
		xmsgNode.addAttribute(ZNTF_PID_str_device, arzStr.Get_At(8));
		xmsgNode.addAttribute(ZNTF_PID_str_policy, arzStr.Get_At(9));
		xmsgNode.addAttribute(ZNTF_PID_str_proccess, arzStr.Get_At(7));
		psz = xMainNode.createXMLString(false);
		ASSERT(NULL != psz && "createXMLString error\n");
		inou_sMeta = psz;
	    }
	} while (false);
	arzStr.Clear();
	if(NULL != psz)
	    free(psz);
	if(XMLNode::emptyNode() != xMainNode)
	    xMainNode.deleteNodeContent();
	//LOG("%s --\n", __func__);
	return ret;
    }
public:
    explicit CFileHandler(const char* in_pszName) { Reinit(in_pszName); }
    void Reinit(const char* in_pszName)
    {
	m_DataName = in_pszName;
	// get a file extension and metafile name build
	const char* tmp = strrchr(in_pszName, '.');
	if(NULL != tmp)
	    m_MetaName = ZLib_StringA(in_pszName, (int)(tmp - in_pszName));	// last symbol taken in attention
	else
	    m_MetaName = in_pszName;
	m_MetaName += METAEXT;
	//LOG("%s %s\n", __func__, (const char*)m_MetaName);
    }
    void Clean()
    {
	if(!m_MetaName.Is_Empty())
	{
	    if(isExist(m_MetaName))
		unlink((const char*)m_MetaName);
	    m_MetaName.Empty();
	}
	if(!m_DataName.Is_Empty())
	{
	    ZLib_StringA stmp = m_DataName + GZ_SUFFIX;
	    if(isExist(m_DataName))
		unlink((const char*)m_DataName);
	    if(isExist(stmp))
		unlink((const char*)stmp);
	    stmp.Empty();
	    m_DataName.Empty();
	}
	if(!m_sMeta.Is_Empty())
	    m_sMeta.Empty();
	if(!m_sCompess.Is_Empty())
	    m_sCompess.Empty();
    }
    ~CFileHandler() { Clean(); }
    int DoHandle()
    {
	int ret = 0;
	char* psz;
	size_t sz;
	do
	{
	    //DumpLaddrMemory(0x300, const_cast<char*>(m_DataName.Get_Data()), m_DataName.Get_Length() + 0x10, ALLOC_TPC(malloc), FREE_TPC(free));
	    if(!isExist(m_DataName))
	    {
		LOG("%s !!! file %s access error %d %s !!!\n", __func__, (const char*)m_DataName, errno, strerror(errno));
		ret = -1;
		break;
	    }
	    if(!isExist(m_MetaName))
	    {
		LOG("%s !!! file %s access error %d %s !!!\n", __func__, (const char*)m_MetaName, errno, strerror(errno));
		ret = -1;
		break;
	    }
	    // meta file reading
	    if(0 != get_contents((LPCSTR)m_MetaName, &psz, &sz))
	    {
		ret = -2;
		break;
	    }
	    ASSERT((sz > 0 && NULL != psz) && "Invalid reading");
	    if(0 != parseMeta(psz, sz, m_sMeta))
	    {
		ret = -3;
		break;
	    }
	    if(0 != metaToXML(m_sMeta))
	    {
		ret = -5;
		break;
	    }
	    if(0 != compressData(psz, sz, m_sCompess))
	    {
		ret = -6;
		break;
	    }
	} while (false);
	return ret;
    }
    const ZLib_StringA& Meta() const { return m_sMeta; }
    const ZLib_StringA& Data() const { return m_sCompess; }
    const ZLib_StringA& Name() const { return m_DataName; }
};

#define	ZLCKURI	"POST /zntf_file HTTP/1.1\r\n"
#define	PORT	1258
class CFilePost
{
private:
    CFilePost();
    CFilePost(CFilePost&);
    CFilePost(const CFilePost&);
    CFilePost& operator=(const CFilePost&);
    CFilePost& operator=(CFilePost);
    CFilePost& operator=(CFilePost&);
private:
    static ZLib_StringA& metaName(const ZLib_StringA& in_sName, ZLib_StringA& inou_sMeta)
    {
	char* tmp = strrchr(in_sName, '.');
	if(NULL == tmp)
	    inou_sMeta = in_sName;
	else
	    inou_sMeta = ZLib_StringA(in_sName.Get_Data(), (int)(tmp - in_sName.Get_Data()));
	inou_sMeta += METAEXT;
	//LOG("%s\n", (const char*)inou_sMeta);
	return name(inou_sMeta);
    }
    static ZLib_StringA& name(ZLib_StringA& inou_Name)
    {
	ZLib_StringA stmp;
    	char* tmp = strrchr(inou_Name.Get_Data(), '/');
	if(NULL != tmp)
	    inou_Name = ++tmp;
	return inou_Name;
    }
    static ZLib_StringA& getHost(const ZLib_StringA& in_sHost, ZLib_StringA& inou_s)
    {
	char* tmp = strstr(in_sHost, HTOK);
	ASSERT(NULL != tmp && "Invalid URL");
	inou_s = tmp + sizeof(HTOK) - 1;
	return inou_s;
    }
    static ZLib_StringA& getBoundary(bool in_bReinit)
    {
	static ZLib_StringA inou_S = "4dazNGAQAn-JUPAq8zYGH22dVzsFI0";
	return inou_S;
    }
private:
    ZLib_StringA m_sMeta;
    ZLib_StringA m_sCprCts;
    ZLib_StringA m_sName;
    ZLib_StringA m_sUrl;
    ZLib_StringA m_sReq;
    bool m_bInited;
public:
    explicit CFilePost(const ZLib_StringA& in_sMeta, const ZLib_StringA& in_sCompr, const ZLib_StringA& in_sName, const char* in_sUrl) :
	m_sMeta(in_sMeta), m_sCprCts(in_sCompr), m_sName(in_sName), m_sUrl(""), m_sReq("")
    {
	// random values generator initialization
	if(!m_bInited)
	{
	    srand(time(NULL));
	    m_bInited = true;
	}
	// url build
	ZLib_StringA s = HTTP;
	s += ":%d";
	m_sUrl.Format(s, in_sUrl, (int)PORT);
	//LOG("%s %s\n", __func__, (const char*)m_sUrl);
    }
    void Reinit(const ZLib_StringA& in_sMeta, const ZLib_StringA& in_sCompr, const ZLib_StringA& in_sName)
    {
	Clean(false);
	m_sMeta = in_sMeta;
	m_sCprCts = in_sCompr;
	m_sName = in_sName;
    }
    void Clean(bool in_bFinish)
    {
	if(!m_sMeta.Is_Empty())
	    m_sMeta.Empty();
	if(!m_sCprCts.Is_Empty())
	    m_sCprCts.Empty();
	if(!m_sReq.Is_Empty())
	    m_sReq.Empty();
	if(!m_sName.Is_Empty())
	    m_sName.Empty();
	if(in_bFinish)
	{
	    if(!m_sUrl.Is_Empty())
		m_sUrl.Empty();
	}
    }
    // build request
    void Prepare()
    {
	ZLib_StringA stmp, sttmp;
	// URI and Connection filelds build
	m_sReq.Format("%s%s", ZLCKURI, CONNCT);
	// request mime part build, 1-st boundary
	stmp.Format("--%s%s", (const char*)getBoundary(true), SR);
	// Content-Disposition: form-data + meta file name
	sttmp.Format(CDISP, metaName(m_sName, sttmp).Get_Data());
	stmp += sttmp;
	// Content-Type: text/plain
	stmp += CTTXT;
	// Content-Transfer-Encoding:
	stmp += CTTENC;
	// packet mime-part 1-st header end
	stmp += SR;
	// meta file contents
	stmp += m_sMeta;
	// packet mime-part 1-st chunk end
	stmp += SR;
	// packet mime part 2-nd boundary
	stmp = stmp + "--" + getBoundary(false) + SR;
	sttmp.Empty();
	// Content-Disposition: form-data + data file name
	sttmp.Format(CDISP, (name(m_sName) + GZ_SUFFIX).Get_Data());
	// Content-Type: text/plain
	sttmp += CTTXT;
	// Content-Transfer-Encoding:
	sttmp += CTTENC;
	stmp += sttmp + SR;	// it's a mandatory condition, 2-nd chunk start
	//LOG("%s", (const char*)stmp);
	// data file contents
	stmp = stmp + m_sCprCts;
	// last boundary range
	sttmp.Format("%s--%s--%s", SR, (const char*)getBoundary(false), SR);
	stmp = stmp + sttmp;
	// Content-Length: (into the 1-st header)
	sttmp.Format(CLEN, stmp.Get_Length());
	// build full request
	m_sReq += sttmp;
	sttmp.Empty();
	// Host:
	m_sReq += HOST;
	m_sReq += getHost(m_sUrl, sttmp) + SR;
	// Content-type: multipart; boundary
	sttmp.Format("%s%s%s", CTTYPE, (const char*)getBoundary(false), SR);
	m_sReq += sttmp;
	m_sReq += SR;
	m_sReq = m_sReq + stmp;
	sttmp.Empty();
	stmp.Empty();
    }
    int Request() const
    {
	ZLib_StringA stmp;
	int ret;
	do
	{
	    ret = ZWS_Request_Simple2((LPCSTR)m_sUrl, (LPCSTR)m_sReq, m_sReq.Get_Length(), &stmp);
	    LOG("\n%s res %d resp length %d\n", __func__, ret, (int)stmp.Get_Length());
	    if(200 != ret)
	    {
		LOG("%s !!! ZWS_Request_Simple error !!!\n", __func__);
		ret = -1;
		//DumpLaddrMemory(9, const_cast<char*>(m_sReq.Get_Data()), m_sReq.Get_Length(), ALLOC_TPC(malloc), FREE_TPC(free));
		break;
	    }
	    LOG("%s %s\n", __func__, (LPCSTR)stmp);
	} while (false);
	return ret;
    }
    ~CFilePost() { Clean(true); }
};

// sending 2 files to the Zlock server inside single request POST HTML
int SendFilePostToZlock(int argc, char* argv[])
{
    int ret = 0;
    do
    {
	if(argc != 3)
	{
	    LOG("%s Usage: %s <server | ip> <filename>\n", __func__, argv[0]);
	    ret = -1;
	    break;
	}
	CFileHandler ent(argv[2]);
	if(0 != ent.DoHandle())
	{
	    ret = -2;
	    break;
	}
	CFilePost pst(ent.Meta(), ent.Data(), ent.Name(), argv[1]);
	pst.Prepare();
	ret = pst.Request();
    } while (false);
    return ret;
}

int main(int argc, char* argv[])
{
    SendFilePostToZlock(argc, argv);
    return 0;
}
