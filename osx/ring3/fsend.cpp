// fsend.cpp
// zlockservice
// Created by oleshii on 9/10/15.
// Copyright (c) 2015 zecurion. All rights reserved.
// Shadow Copied files transfer to server
// 01/31/17 Transfer protocol and data format refactoring to the zincients, XML write library dependence excluded

#include <assert.h>
#include <unistd.h>
#include <pwd.h>
#include <fcntl.h>
#include <errno.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <pthread.h>

// standart STL types identification
#include <vector>
#include <string>
using std::vector;
using std::string;
#include <typeinfo>

// zecurion common library support
#include <stdafx.h>
#include <zlib.h>
#include <zlib_string.h>
#include <zlib_array.h>

#include <guid.h>

// remote SC settings
#include <policyparser.h>
// project own interfaces support
#include "macsvc.h"

__private_extern__ ZLib_StringA GetSCLocalPath(void);
__private_extern__ ZLCK_SVC_DATA_p_s GetTLS(int in_ID);
__private_extern__ int ChgWD(const ZLib_StringA& in_sCD);
__private_extern__ bool GetWD(ZLib_String& inou_szfName);
__private_extern__ void Clean(const ZLib_StringA& in_sFolderName);
__private_extern__ ZLib_StringA GetCfgIPAddress(const char* in_pszServer);
__private_extern__ void GetName(const ZLib_StringA& in_s, ZLib_StringA& out_s);
__private_extern__ void GetPath(const ZLib_StringA& in_s, ZLib_StringA& out_s);
__private_extern__ void GetContents(const char* in_pszName, ZLib_StringA& inou_sCont);
__private_extern__ void GetData(ZLib_StringA& inou_sData, const ZLib_StringA& in_sFolderName);
__private_extern__ int WriteIn(const ZLib_StringA& in_sName, const ZLib_StringA& in_sContent);
__private_extern__ void GetDateTime(ZLib_StringA& Date, ZLib_StringA& Time, time_t in_DateTime);
__private_extern__ int SendRequest(const ZLib_StringA& in_sUrl, const ZLib_StringA& in_sRequest);
__private_extern__ ZLib_String GetFileContent(const ZLib_String& in_szFname, bool bCmpLen = false);
__private_extern__ int Compress(const ZLib_StringA& in_sMeta, const ZLib_StringA& in_sGuid, const ZLib_StringA& in_sFile);
__private_extern__ void MetaToMsgParams(const ZLib_StringA& in_sGuid, ZLibT_Array<ZLib_StringA>& in_arS, const vector<string>& in_arXMLIds, ZLib_StringA& ou_s);

#ifdef DEBUG
#define ASSERT assert
#else
#define ASSERT
#endif
#define LOG ZTRACE
#include <debug.h>

#define METAEXT ".info"
#define DELAY   10
#define SLPTMKS 1000000 * 3600  // 1 hour in microseconds

static ZLib_StringA getSCFileName(const ZLib_StringA& in_psDir)
{
    struct stat statbuf;
    ZLib_StringA s_ret;
    struct dirent* dp = nullptr;
    DIR* dirp = nullptr;

    do
    {
        dirp = opendir((const char*)in_psDir);
        if(nullptr == dirp)
        {
            LOG("%s !!! opendir %s error %d %s\n", __func__, (LPCSTR)in_psDir, errno, strerror(errno));
            break;
        }
        while(nullptr != (dp = readdir(dirp)))
        {
            //ZTRACE("%s %s\n", __func__, dp->d_name);
            if(NOTHING == strcmp(dp->d_name, ".") || NOTHING == strcmp(dp->d_name, ".."))
                continue;
            s_ret = in_psDir + '/' + dp->d_name;
            // meta files are ignored
            if(nullptr != strstr((LPCSTR)s_ret, METAEXT))
            {
                s_ret = "";
                continue;
            }
            if(!IS_SUCCEEDED(stat((LPCSTR)s_ret, &statbuf)))
            {
                LOG("%s !!! stat %s error %d %s !!!\n", __func__, (LPCSTR)s_ret, errno, strerror(errno));
                s_ret = "";
                continue;
            }
            if(NOTHING == statbuf.st_size || !IS_FLG(statbuf.st_mode, S_IFREG))
            {
                LOG("%s an inappropriate entity %s\n", __func__, (LPCSTR)s_ret);
                s_ret = "";
                continue;
            }
            break;
        }
        if(nullptr != dirp)
            closedir(dirp);
    } while (false);
    return s_ret;
}

static void dataToMeta(const char* in_pszName, ZLib_StringA& ou_s)
{
    // get a file extension and metafile name build
    const char* tmp = strrchr(in_pszName, '.');
    if(nullptr != tmp)
        ou_s = ZLib_StringA(in_pszName, (int)(tmp - in_pszName));   // last symbol taken in attention
    else
        ou_s = in_pszName;
    ou_s += METAEXT;
}

class __attribute__ ((visibility("hidden"))) CFPoster
{
private:
    enum class METAIDENTS { TRUTHNAME, INSCNAME, SIZE, DATETIME, USERID, PROCESS, DEVICE, POLICY, META_STRINGS };
    ZLib_StringA m_MetaName;
    ZLib_StringA m_FolderName;
    ZLib_StringA m_Guid;
    ZLib_StringA m_sFile;
private:
    void folderName(const ZLib_StringA& in_s)
    {
        do
        {
            //LOG("%s::%s ++\n", typeid(this).name(), __func__);
            char* psz1 = strrchr(in_s.Get_Data(), '/');
            if(nullptr == psz1)
                psz1 = const_cast<char*>(in_s.Get_Data());
            else
                psz1++;
            //LOG("%s::%s (1) psz1 %s\n", typeid(this).name(), __func__, psz1);
            char* psz = strchr(psz1, '.');
            if(nullptr == psz)
                break;
            ZLib_StringA s(psz1, (size_t)(psz - psz1));
            //LOG("%s::%s (2) %s\n", typeid(this).name(), __func__, s.Get_Data());
            m_FolderName += "{" + s + "}";
            s = m_FolderName;
            GUID g;
            if(!get_GUIDFromString(m_FolderName.Get_Data(), &g, (int)m_FolderName.Get_Length()))
                break;
            //LOG("%s::%s (3)\n", typeid(this).name(), __func__);
            //m_FolderName = ZLib_StringA(in_s.Get_Data(), (size_t)(psz1 - in_s.Get_Data())) + m_FolderName;
            m_FolderName = joinStr(STORPATH, s, "");
            LOG("%s::%s %s\n", typeid(this).name(), __func__, m_FolderName.Get_Data());
            m_Guid = s;
        } while (false);
    }
    // meta file handling, convert time to string, user id to string
    int parseMeta(char* in_psz, size_t in_sz, ZLibT_Array<ZLib_StringA>& inou_arStr) const
    {
        int r = 0, i;
        char* tmp;
        time_t time;
        struct passwd* pass = nullptr;
        ZLib_StringA sdate, stime, sMeta;
        do
        {
            for(i = 0, tmp = in_psz; i < static_cast<int>(METAIDENTS::META_STRINGS); ++i)
            {
                char* psz = strchr(tmp, '\n');
                if(nullptr == psz)
                {
                    LOG("%s !!! Invalid meta file format !!!\n", __func__);
                    r = -1;
                    break;
                }
                if(static_cast<int>(METAIDENTS::USERID) == i)
                {
                    //LOG("%s::%s [%d] %s\n", typeid(this).name(), __func__, i, sMeta);
                    char* pch = const_cast<char*>("\n");
                    time = strtoul(tmp, &pch, 10);
                    //LOG("%s::%s uid %lu\n", typeid(this).name(), __func__, time);
                    pass = getpwuid((int)time);
                    if(nullptr == pass)
                    {
                        LOG("%s !!! user id %d search error %d %s\n", __func__, (int)time, errno, strerror(errno));
                        r = -2;
                        break;
                    }
                    sMeta = pass->pw_name;
                    stime.Empty();
                }
                else
                    sMeta = ZLib_StringA(tmp, (size_t)(psz - tmp));
                inou_arStr.Add(sMeta);
                tmp = ++psz;
            }
        } while (false);
        return r;
    }
    void cleanup(const ZLib_StringA& in_sMeta, const ZLib_StringA& in_sData) const
    {
        do
        {
            ZLib_StringA sExt, s = in_sMeta;
            getExt(in_sData, sExt);
            cutExt(s);
            if(!IS_SUCCEEDED(unlink((LPCTSTR)in_sMeta)))
                LOG("%s::%s !!! delete %s error %d %s !!!\n", typeid(this).name(), __func__, (LPCTSTR)in_sMeta, errno, strerror(errno));
            s += sExt;
            if(!IS_SUCCEEDED(unlink((LPCTSTR)s)))
                LOG("%s::%s !!! delete %s error %d %s !!!\n", typeid(this).name(), __func__, (LPCTSTR)s, errno, strerror(errno));
        } while (false);
    }
    void getExt(const ZLib_StringA& in_s, ZLib_StringA& out_s) const
    {
        do
        {
            char* psz = strrchr(in_s.Get_Data(), '.');
            if(nullptr == psz)
                break;
            out_s = psz;
        } while(false);
    }
    void cutExt(ZLib_StringA& inou_s) const
    {
        do
        {
            char* psz = strrchr(inou_s.Get_Data(), '.');
            if(nullptr == psz)
                break;
            inou_s = ZLib_StringA(inou_s.Get_Data(), (size_t)(psz - inou_s.Get_Data()));
        } while(false);
    }
    int saveAs(const ZLib_StringA& in_sNewName, const ZLib_StringA& in_sPath, const ZLib_StringA& in_sOrigPath)
    {
        // const ZLib_StringA& in_sName, const ZLib_StringA& in_sContent
        int r = NOTHING;
        ZLib_StringA sExt, sName;
        do
        {
            getExt(in_sNewName, sExt);
            GetName(in_sOrigPath, sName);
            cutExt(sName);
            sName += sExt;
            //LOG("%s::%s (1) %s\n", typeid(this).name(), __func__, (const char*)sName);
            GetPath(in_sOrigPath, sExt);
            ASSERT(!sExt.Is_Empty() && "!!! An invalid file path !!!");
            //LOG("%s::%s (2) %s\n", typeid(this).name(), __func__, (const char*)sExt);
            sExt += '/' + sName;
            sName.Empty();
            //LOG("%s::%s (3) %s\n", typeid(this).name(), __func__, (const char*)sExt);
            GetContents(sExt, sName);
            r = WriteIn(in_sPath + in_sNewName, sName);
        } while(false);
        return r;
    }
    int metaToMsgParams(const ZLib_StringA& in_sMsgInfoPath, const ZLib_StringA& in_sGuid, ZLibT_Array<ZLib_StringA>& in_arS) const
    {
        ZLib_StringA sCommon;
        // C++11/14
        vector<string> ar_sxml = { "attachments", "fs_local_path", "attachmentsSize", "date", "time", "user", "process", "device", "attribs" };
        MetaToMsgParams(in_sGuid, in_arS, ar_sxml, sCommon);
        return WriteIn(in_sMsgInfoPath, sCommon);
    }
    ZLib_StringA joinStr(const char* in_psz, const ZLib_StringA& in_sFolder, const ZLib_StringA& in_sName) const
    {
        return ZLib_StringA(in_psz) + in_sFolder + in_sName;
    }
public:
    CFPoster(void) = delete;
    CFPoster(CFPoster&) = delete;
    CFPoster(const CFPoster&) = delete;
    CFPoster& operator=(const CFPoster&) = delete;
    CFPoster& operator=(CFPoster) = delete;
    // .info file path must be an argument
    // {00F1324F-37D2-430E-AF45-96AED03CFF71} => Attachments => file
    // {00F1324F-37D2-430E-AF45-96AED03CFF71} => .MessageParameters (XML contents)
    explicit CFPoster(const char* in_pszName, const char* in_pszUrl) : m_MetaName(in_pszName)
    {
        do
        {
            ZLib_StringA sc, stmp;
            ZLibT_Array<ZLib_StringA> ar_s;
            ASSERT(!m_MetaName.Is_Empty() && "!!! An incorrect file name !!!");
            folderName(m_MetaName);
            ASSERT(!m_FolderName.Is_Empty() && "!!! An incorrect GUID in name !!!");
            GetContents(in_pszName, sc);
            //LOG("%s::%s (1)\n", typeid(this).name(), __func__);
            if(!IS_SUCCEEDED(parseMeta(const_cast<char*>(sc.Get_Data()), sc.Get_Length(), ar_s)))
                break;
            //LOG("%s::%s (2)\n", typeid(this).name(), __func__);
            sc.Empty();
            ASSERT(NOTHING < ar_s.Get_Num_Elements() && "!!! Empty string storage !!!");
            for(unsigned i = 0; i < ar_s.Get_Num_Elements(); i++)
                LOG("%s::%s [%d] %s\n", typeid(this).name(), __func__, i, ar_s.Get_At(i).Get_Data());
            if(!IS_SUCCEEDED(mkdir((const char*)joinStr(STORPATH, m_Guid, ""), 0777)))
            {
                LOG("%s::%s !!! mkdir %s error %d %s !!!\n", typeid(this).name(), __func__, (const char*)joinStr(STORPATH, m_Guid, ""), errno, strerror(errno));
                break;
            }
            if(!IS_SUCCEEDED(mkdir((const char*)joinStr(STORPATH, m_Guid, "/Attachments"), 0777)))
            {
                LOG("%s::%s !!! mkdir %s error %d %s !!!\n", typeid(this).name(), __func__, (const char*)joinStr(STORPATH, m_Guid, "/Attachments"), errno, strerror(errno));
                break;
            }
            GetName(ar_s.Get_At(static_cast<int>(METAIDENTS::TRUTHNAME)), m_sFile);
            ASSERT(!m_sFile.Is_Empty() && "!!! Empty file name !!!");
            //LOG("%s::%s %s %s\n", typeid(this).name(), __func__, (const char*)m_sFile, (const char*)m_MetaName);
            if(!IS_SUCCEEDED(saveAs(m_sFile, m_FolderName + "/Attachments/", m_MetaName)))
                break;
            if(!IS_SUCCEEDED(metaToMsgParams(m_FolderName + "/.MessageParameters", m_Guid, ar_s)))
                break;
            if(!IS_SUCCEEDED(Compress(m_MetaName, m_Guid, m_sFile)))
                break;
            GetData(sc, m_FolderName);
            stmp.Format(MSGBODY, (unsigned)sc.Get_Length(), in_pszUrl);
            stmp = stmp + sc;
            sc = HTOK;
            sc += in_pszUrl;
            if(nullptr == strchr(in_pszUrl, ':'))
                sc += PORT_TOKEN;
            SendRequest(sc, stmp);
        } while (false);
        Clean(m_FolderName);
        cleanup(m_MetaName, m_sFile);
    }
};

void* ZLCK_SVCM_Server_SC_Handler(void* in_pvData)
{
    typedef ZLibT_Array<ZLib_StringA> AddrArr;
    __private_extern__ bool GetFONSrvs(AddrArr& inou_Arr, const char* in_pszLSrv, BYTE in_uStT, const char* in_pszCfSrv);
    int r, rl;
    unsigned i;
    AddrArr arr;
    ZLib_StringA scFile, s;
    struct timeval tv = {0};
    struct timespec ts = {0};
    ZLCK_SC_REM_STS_p_s pSts = nullptr;
    ZLCK_SVC_DATA_p_s pData = nullptr;

    pData = (ZLCK_SVC_DATA_p_s)in_pvData;
    pSts = (ZLCK_SC_REM_STS_p_s)&pData->aszData[OK];
    ASSERT(NOTHING != pSts->szLogServer[OK] && "!!! Invalid target server address !!!");
    ZTRACE("%s ++ %s\n", __func__, NOTHING == pSts->szLogServer[OK] ? "NULL" : pSts->szLogServer);
    r = pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, nullptr); // cancel immediately
    if(!IS_SUCCEEDED(r))
    {
        ZTRACE("%s !!! pthread_setcanceltype error %d %s\n", __func__, r, strerror(r));
        return nullptr;
    }
    if(!GetFONSrvs(arr, pSts->szLogServer, pSts->StorageType, (const char*)GetTLS(GET_CONFIG)->aszData))
    {
        ZTRACE("%s !!! Can't obtain a servers list !!!\n", __func__);
        return nullptr;
    }
    if(NOTHING == arr.Get_Num_Elements())
    {
        ZTRACE("%s !!! The servers list is empty !!!\n", __func__);
        return nullptr;
    }
    do
    {
        // Does our job completed ?
        if(NOTHING != pData->ubyEOJ)
        {
            ZTRACE("%s End of job signal\n", __func__);
            break;
        }
        ASSERT(NOTHING < pSts->Enabled && "!!! remote SC disabled !!!");
        // try to obtain a file name from SC
        scFile = getSCFileName(GetSCLocalPath());
        // the SC storage doesn't have an appropriate data yet
        if(scFile.Is_Empty())
        {
            gettimeofday(&tv, nullptr);
            ts.tv_sec = tv.tv_sec + DELAY; // 10 seconds delay
            ts.tv_nsec = 0;
            r = pthread_mutex_lock(&pData->mutex);
            if(!IS_SUCCEEDED(r))
            {
                LOG("%s !!! pthread_mutex_lock error %d %s !!!\n", __func__, r, strerror(r));
                break;
            }
            rl = pthread_cond_timedwait(&pData->cond, &pData->mutex, &ts);
            r = pthread_mutex_unlock(&pData->mutex);
            if(!IS_SUCCEEDED(r))
            {
                LOG("%s !!! pthread_mutex_unlock error %d %s !!!\n", __func__, r, strerror(r));
                break;
            }
            ZTRACE("%s after waiting result %d\n", __func__, rl);
            if(ETIMEDOUT == rl)
                continue;
            else
            {
                ZTRACE("%s thread %p interrupt obtained\n", __func__, pthread_self());
                break;
            }
        }
        ZTRACE("%s %s\n", __func__, (LPCSTR)scFile);
        // propogate a file among servers
        for(i = 0; i < arr.Get_Num_Elements(); i++)
        {
            dataToMeta((LPCSTR)scFile, s);
            ASSERT(!s.Is_Empty() && "!!! An invalid file name !!!");
            CFPoster((LPCSTR)s, GetCfgIPAddress(arr.Get_At(i)));
        }
    } while (true);
    GetTLS(FLE_SENDER)->mself = nullptr;
    ZTRACE("%s --\n", __func__);
    return nullptr;
}
