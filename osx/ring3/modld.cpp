// drivers checker/loader
#include <assert.h>
#include <IOKit/kext/KextManager.h>
#include <CoreFoundation/CoreFoundation.h>

#include <stdafx.h>
#include <zlib.h>
#define LOG     ZTRACE
#include <zlib_string.h>

#if defined(DEBUG)
#define ASSERT assert
#else
#define ASSERT
#endif

#include "cstring.h"

#include <zlock_ui_iface.h>
#include <deviface.h>
#include "ptrs.h"

__private_extern__ ZLib_String GetLogPath();

// The direct OSKext.h inclusion or linking with the Kernel.framework from the ring3 application raices a
// linker error. Unfortunately, but this framework by Apple's meaning must be linked with the drivers only.
extern "C" CFDictionaryRef OSKextCopyLoadedKextInfo(CFArrayRef kextIdentifiers, CFArrayRef infoKeys) __OSX_AVAILABLE_STARTING(__MAC_10_7, __IPHONE_4_3);

static int compare(CString& in_Str, const char* in_psz)
{
    int r = 0;
    size_t l1, l2;
    do
    {
        l1 = strlen(in_psz);
        l2 = strlen(in_Str.C_str());
        r = ((l1 > l2) ? (NULL == strstr(in_psz, in_Str.C_str())) : (NULL == strstr(in_Str.C_str(), in_psz)));
    } while (false);
    return r;
}

class __attribute__((visibility("hidden"))) CModLoader
{
private:
    CModLoader();
    CModLoader(const CModLoader&);
    CModLoader& operator=(const CModLoader&);
    CModLoader(char* in_pszName);
    int isLoaded(const char* in_pszId) const;
    int modLoad(const char* in_pszPath) const;
public:
    CModLoader(const char* in_pszBundleID, const char* in_pszKextName, int& inou_Err);
    ~CModLoader() {}
};

// in_pszBundleID = Bundle identifier, in_pszKextName = kext name (with no path)
CModLoader::CModLoader(const char* in_pszBundleID, const char* in_pszKextName, int& inou_Err)
{
    inou_Err = isLoaded(in_pszBundleID);
    if(NOTHING == inou_Err)
    {
        ZLib_String szFile = GetLogPath();
        int pos = szFile.Reverse_Find('/');
        ASSERT(-1 != pos && "Invalid directory path");
        szFile = ZLib_StringA(szFile.Get_Data(), pos + 1);
        szFile += in_pszKextName;
        inou_Err = modLoad(szFile.Get_Data());
    }
}

// returns: < 0 = error, 0 = not loaded, > 0 = loaded
int CModLoader::isLoaded(const char* in_pszId) const
{
    CFDictionaryRef kextDict = NULL; // the matrix, a dictionary of dictionaries
    CFStringRef sRef = NULL;
    int cnt, i, ret = 0;
    const void** pvKeys = NULL, **pvValues = NULL;
    
    do
    {
        kextDict = OSKextCopyLoadedKextInfo(NULL, NULL);
        if(NULL == kextDict)
        {
            LOG("%s::%s !!! OSKextCopyLoadedKextInfo error !!!\n", typeid(this).name(), __func__);
            ret = -1;
            break;
        }
        cnt = (int)CFDictionaryGetCount(kextDict);
        if(0 == cnt)
        {
            LOG("%s::%s drivers dictionary count is zero\n", typeid(this).name(), __func__);
            ret = -2;
            break;
        }
        pvKeys = (const void**)malloc(cnt * (sizeof(void*)));
        if(NULL == pvKeys)
        {
            LOG("%s::%s !!! (0) memory allocation error %d %s!!!\n", typeid(this).name(), __func__, errno, strerror(errno));
            ret = -3;
            break;
        }
        pvValues = (const void**)malloc(cnt * (sizeof(void*)));
        if(NULL == pvValues)
        {
            LOG("%s::%s !!! (1) memory allocation error %d %s!!!\n", typeid(this).name(), __func__, errno, strerror(errno));
            ret = -4;
            break;
        }
        CFDictionaryGetKeysAndValues(kextDict, pvKeys, pvValues);
        for(i = 0; i < cnt; i++)
        {
            sRef = (CFStringRef)CFDictionaryGetValue((CFDictionaryRef)pvValues[i], CFSTR("CFBundleIdentifier"));
            if(NULL != sRef && CFStringGetTypeID() == CFGetTypeID(sRef))
            {
                CString s(sRef);
                if(NOTHING == compare(s, in_pszId))
                {
                    LOG("%s::%s %s\n", typeid(this).name(), __func__, s.C_str());
                    ret = 1;
                    break;
                }
            }
        }
    } while (FALSE);
    if(NULL != kextDict)
        CFRelease(kextDict);
    if(NULL != pvKeys)
        free(pvKeys);
    if(NULL != pvValues)
        free(pvValues);
    if(ret >= 0)
        LOG("%s::%s %s %s\n", typeid(this).name(), __func__, in_pszId, ret > 0 ? "loaded" : "not loaded");
    return ret;
}

int CModLoader::modLoad(const char* in_pszPath) const
{
    CFURLRef cfURL = NULL;
    CFStringRef sRef = NULL;
    int ret = KERN_SUCCESS;
    
    do
    {
        LOG("%s::%s %s\n", typeid(this).name(), __func__, in_pszPath);
        sRef = CFStringCreateWithCString(NULL, in_pszPath, kCFStringEncodingASCII);
        if(NULL == sRef)
        {
            LOG("%s::%s !!! CFStringCreateWithCString error !!!\n", typeid(this).name(), __func__);
            ret = -1;
            break;
        }
        // driver loading must be sudo-ed.
        cfURL = CFURLCreateWithFileSystemPath(kCFAllocatorDefault, sRef, kCFURLPOSIXPathStyle, TRUE);
        if(NULL == cfURL)
        {
            LOG("%s::%s !!! CFURLCreateWithFileSystemPath error !!!\n", typeid(this).name(), __func__);
            ret = -2;
            break;
        }
        ret = KextManagerLoadKextWithURL(cfURL, NULL);
        LOG("%s::%s KextManagerLoadKextWithURL rezult %x\n", typeid(this).name(), __func__, ret);
        if(IS_SUCCEEDED(ret))
            ret += 1;
    } while (false);
    if(NULL != cfURL)
        CFRelease(cfURL);
    if(NULL == sRef)
        CFRelease(sRef);
    return ret;
}

__private_extern__ int LoadEntity(const char* in_Id, const char* in_Name)
{
    int r = KERN_SUCCESS;
    MPTH<CModLoader, const char*, const char*, int> mpf1(in_Id, in_Name, r);
    return r;
}