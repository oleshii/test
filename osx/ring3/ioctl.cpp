#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/sysctl.h>
#include <sys/mount.h>
#include <libkern/OSReturn.h>
#include <CoreFoundation/CoreFoundation.h>
#include <IOKit/IOKItLib.h>
#include <IOKit/kext/KextManager.h>
#include <IOKit/usb/IOUSBLib.h>
#include "../mkplugin/mkpzlock/mkpzlock/deviface.h"

#define	LOG	printf

#define	SYSKEXTDIR	"/System/Library/Extensions/"
#define	KEXTPATH	"/tmp/mkpzlock.kext"

int getParent(io_registry_entry_t entry, struct zlck_ioctl_s* lpCtl)
{
    io_registry_entry_t parent = MACH_PORT_NULL;
    io_name_t cls = {0}, name = {0};
    int r = KERN_SUCCESS;

    for(;;)
    {
	r = IORegistryEntryGetParentEntry(entry, kIOServicePlane, &parent);
	if(0 != r)
	{
	    LOG("%s !!! IORegistryEntryGetParentEntry error %x !!!\n", __func__, r);
	    return -1;
	}
	if(MACH_PORT_NULL == parent)
	{
	    LOG("%s !!! No more objects !!!\n", __func__, r);
	    return -2;
	}
	entry = parent;
	IOObjectRelease(parent);
	r = IOObjectGetClass(entry, cls);
	if(0 != r)
	{
	    LOG("%s !!! IOObjectGetClass error %x !!!\n", __func__, r);
	    return -3;
	}
	if(strstr(cls, "IOUSBDevice"))
	{
	    IORegistryEntryGetName(entry, name);
	    strcpy(lpCtl->dev, (char*)cls);
	    LOG("%s %s\n", name, cls);
	    break;
	}
    }
    return 0;
}

int getDevProp(io_registry_entry_t service, struct zlck_ioctl_s* lpCtl)
{
    CFStringRef sRef = NULL;
    int r = KERN_SUCCESS;
    io_registry_entry_t child = MACH_PORT_NULL, childnext = MACH_PORT_NULL;
    io_iterator_t children = MACH_PORT_NULL;
    io_name_t name = {0}, cls = {0};
    char bsd[0x20] = {0};

    do
    {
	r = IORegistryEntryGetChildIterator(service, kIOServicePlane, &children);
	if(KERN_SUCCESS != r || MACH_PORT_NULL == children)
	{
	    LOG("%s !!! IORegistryEntryGetChildIterator error %d !!!\n", __func__, r);
	    break;
	}
	childnext = IOIteratorNext(children);
	while(childnext)
	{
	    child = childnext;
	    r = IORegistryEntryGetName(child, name);
	    r = IOObjectGetClass(child, cls);
	    if(KERN_SUCCESS == r && NULL != name)
	    {
		//LOG("%s %s\n", name, cls);
		if(0 == strcmp(cls, "IOMedia"))
		{
		    sRef = (CFStringRef)IORegistryEntrySearchCFProperty(child, kIOServicePlane, CFSTR("BSD Name"), NULL,
            		kIORegistryIterateRecursively | kIORegistryIterateParents);
            	    if(NULL != sRef)
            	    {
            		if(0 != bsd[0])
            		    memset(bsd, 0, sizeof(bsd));
            		CFStringGetCString(sRef, &bsd[0], sizeof(bsd), kCFStringEncodingUTF8);
            		CFRelease(sRef);
            		sRef = NULL;
            		// we'll start backward search a parent USB device from here
            		if(strstr(lpCtl->bsd, bsd))
            		{
            		    LOG("%s %s %s\n", name, cls, bsd);
            		    if(0 != getParent(child, lpCtl))
            		    {
            			r = -1;
            			LOG("%s !!! Can't find mounted USB device !!!\n", __func__);
            			break;
            		    }
            		}
            	    }
		}
	    }
	    if(0 != r)
		break;
	    r = getDevProp(childnext, lpCtl);
	    IOObjectRelease(child);
	    child = MACH_PORT_NULL;
	    if(0 != r)
		break;
	    childnext = IOIteratorNext(children);
	}
    } while (FALSE);
    if(MACH_PORT_NULL != children)
	IOObjectRelease(children);
    return r;
}

int getUSBBsdMountApriory(struct zlck_ioctl_s* lpCtl)
{
    int r = 0, mntsize;
    struct statfs* mntbuf = NULL;

    do
    {
	mntsize = getfsstat(NULL, 0, MNT_NOWAIT);
	if(0 > mntsize)
	{
	    LOG("%s !!! getfsstat (0) error %d %s !!!\n", __func__, errno, strerror(errno));
	    r = -1;
	    break;
	}
	mntbuf = (struct statfs*)malloc(mntsize * sizeof(struct statfs));
    	if(NULL == mntbuf)
        {
            LOG("%s !!! memory allocation error %d %s !!!\n", __func__, errno, strerror(errno));
            r = -2;
            break;
        }
        memset(mntbuf, 0, sizeof(struct statfs) * mntsize);
        if(0 >= getfsstat(mntbuf, sizeof(struct statfs) * mntsize, MNT_NOWAIT))
        {
            LOG("%s !!! getfsstat (1) error %d %s !!!\n", __func__, errno, strerror(errno));
            r = -3;
            break;
        }
        for(r = 0; r < mntsize; r++)
        {
    	    if(strstr(mntbuf[r].f_mntonname, "/Volumes/") && strstr(mntbuf[r].f_mntfromname, "/dev/disk"))
    	    {
    		LOG("%s %s -> %s\n", __func__, mntbuf[r].f_mntfromname, mntbuf[r].f_mntonname);
    		strcpy(lpCtl->mnt, mntbuf[r].f_mntonname);
    		strcpy(lpCtl->bsd, mntbuf[r].f_mntfromname);
    		r = 0;
    		break;
    	    }
    	}
    } while(FALSE);
    if(NULL != mntbuf)
	free(mntbuf);
    return r;
}

int main(int argc, char* argv[])
{
    int fd, r, ret = KERN_SUCCESS;
    char szName[0x20] = {0};
    struct zlck_ioctl_s ctls = {0};
    CFURLRef cfURL = NULL;
    io_registry_entry_t service = MACH_PORT_NULL;

    do
    {
	if(argc < 2)
	{
	    service = IORegistryGetRootEntry(kIOMasterPortDefault);
	    if(MACH_PORT_NULL == service)
	    {
		LOG("%s !!! IORegistryGetRootEntry error !!!\n", __func__);
		r = KERN_FAILURE;
		break;
	    }
	    if(0 != getUSBBsdMountApriory(&ctls))
	    {
		LOG("%s !!! Can't find any USB mount points apriori !!!\n", __func__);
		break;
	    }
	    if(0 != getDevProp(service, &ctls))
		break;
	    r = 1;
	    fd = sizeof(r);
	    r = sysctlbyname("debug.zlock.uint32", NULL, NULL, &r, fd);
	    LOG("%s sysctlbyname %d\n", __func__, r);
	    if(0 > r)
	    {
		LOG("%s !!! sysctlbyname error %d %s !!!\n", __func__, errno, strerror(errno));
		break;
	    }
	    sprintf(szName, "%s%s%d", ZLCK_KMOD_DEV_PREFIX, ZLCK_KMOD_BASE_NAME, ZDEVIDX);
	    //LOG("%s %s\n", __func__, szName);
	    fd = open(szName, O_RDWR | O_NONBLOCK | O_EXCL);
	    if(0 > fd)
	    {
		LOG("%s !!! open %s error %d %s !!!", __func__, szName, errno, strerror(errno));
		return -1;
	    }
	    LOG("%s %s opened ok\n", __func__, szName);
	    r = ioctl(fd, ZLCK_KMOD_IOCTL_MOUNT, &ctls);
	    if(0 > r)
	    {
		LOG("%s !!! ioctl error %d %s !!!\n", __func__, errno, strerror(errno));
		close(fd);
		return -2;
	    }
	    LOG("%s ioctl ok\n", __func__);
	    close(fd);
	}
	// it will be unmount apriori
	else
	{
	    sprintf(szName, "%s%s%d", ZLCK_KMOD_DEV_PREFIX, ZLCK_KMOD_BASE_NAME, ZDEVIDX);
	    //LOG("%s %s\n", __func__, szName);
	    fd = open(szName, O_RDWR | O_NONBLOCK | O_EXCL);
	    if(0 > fd)
	    {
		LOG("%s !!! open %s error %d %s !!!", __func__, szName, errno, strerror(errno));
		return -1;
	    }
	    LOG("%s %s opened ok\n", __func__, szName);
	    strcpy(ctls.mnt, argv[1]);
	    r = ioctl(fd, ZLCK_KMOD_IOCTL_UMOUNT, &ctls);
	    if(0 > r)
	    {
		LOG("%s !!! ioctl error %d %s !!!\n", __func__, errno, strerror(errno));
		close(fd);
		return -2;
	    }
	    LOG("%s ioctl ok\n", __func__);
	    close(fd);
	}
    } while(false);
    if(MACH_PORT_NULL != service)
	IOObjectRelease(service);
    if(NULL != cfURL)
	CFRelease(cfURL);
    return 0;
}