#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/sysctl.h>
#include <pthread.h>
#include <IOKit/IODataQueueClient.h>
#include <CoreFoundation/CoreFoundation.h>
#include <mach/mach.h>

#include <guid.h>
#include <platform.h>
#include <zlck_common.h>
#include <driver.h>
#include <zlock_ui_iface.h>

#define	LOG	printf

#include <debug.h>

typedef struct _ZLCK_CONN_s_
{
    io_service_t dev;
    io_connect_t conn;
} ZLCK_CONN_s, *ZLCK_CONN_p_s;

class CString
{
    private:
	CString();
	CString& operator=(char*);
	CString& operator=(const char*);
	CString& operator=(CFStringRef in_Ref);
	operator const char*();
    private:
	char* m_pBuf;
	size_t m_Size;
	CFStringRef m_Ref;
    public:
	CString(CFStringRef in_Ref) : m_Ref(in_Ref), m_pBuf(NULL), m_Size(0) {}
	~CString()
	{
	    if(NULL != m_pBuf)
		free(m_pBuf);
	}
	operator char*()
	{
	    CFIndex len;
	    do
	    {
	    	if(NULL == m_pBuf)
		{
		    if((NULL != m_Ref) && CFStringGetTypeID() == CFGetTypeID(m_Ref))
        	    {
        	        len = CFStringGetLength(m_Ref);
        	        m_Size = CFStringGetMaximumSizeForEncoding(len, kCFStringEncodingUTF8);
        	        if(0 < m_Size && m_Size < 0x1000)
        	        {
        		    m_pBuf = (char*)malloc(m_Size + 1);
        		    if(NULL != m_pBuf)
        		    {
        			memset(m_pBuf, 0, m_Size + 1);
        			CFStringGetCString(m_Ref, m_pBuf, m_Size, kCFStringEncodingUTF8);
        		    }
        	        }
        	    }
		}
	    } while (false);
	    return m_pBuf;
	}
};

static int NotifyHandler(ZLCK_CONN_p_s in_p)
{
    long i;
    int m = -1;
    kern_return_t kr = KERN_SUCCESS;
    ZLCK_MACOSX_IOCTL_s ioctl = {0};
    size_t ret_cnt = sizeof(CZ_UD_RSTUB_s);
    CZ_UD_RSTUB_s rts = {0};
    ZLCK_DEV_p_s pevs = NULL;
    mach_port_t port;
    unsigned int msgType = 1;
    IODataQueueMemory* queue = NULL;
    ZLCK_MPT_p_s pmpts = NULL;
    vm_address_t addr = 0;
    vm_size_t size = 0;
    uint32_t sz;
    CFArrayRef arr = NULL, parts = NULL;
    CFDictionaryRef dict = NULL;
    CFStringRef sDisk = NULL, sMnt = NULL, sName = NULL, sPol = NULL;
    CFNumberRef nNum = NULL;

    do
    {
        ioctl.wType = ZLCK_MACOSX_QUERY_INFO;
        ioctl.uLength = 0;	
	kr = ioctl_StructIStructO_Driver(in_p->conn, kMyUserClientIoctl, &ioctl, sizeof(ioctl), &rts, &ret_cnt);
	LOG("%s ioctl_StructIStructO_Driver (QDP) %x\n", __func__, kr);
	if(KERN_SUCCESS != kr)
	    break;
	port = IODataQueueAllocateNotificationPort();
	LOG("%s IODataQueueAllocateNotificationPort => %x\n", __func__, port);
	if(IO_OBJECT_NULL == port)
	{
	    LOG("%s !!! IODataQueueAllocateNotificationPort error !!!\n", __func__);
	    break;
	}
	kr = IOConnectSetNotificationPort(in_p->conn, msgType, port, 0);
	LOG("%s IOConnectSetNotificationPort => %x\n", __func__, kr);
	if(KERN_SUCCESS != kr)
	    break;
	kr = IOConnectMapMemory(in_p->conn, kIODefaultMemoryType, mach_task_self(), (mach_vm_address_t*)&addr, (mach_vm_size_t*)&size,
	    kIOMapAnywhere);
	LOG("%s IOConnectMapMemory => %llx\n", __func__, (unsigned long long)addr);
	if(KERN_SUCCESS != kr || 0 == addr)
	{
	    LOG("%s !!! IOConnectMapMemory error %x !!!\n", __func__, kr);
	    break;
	}
	queue = (IODataQueueMemory*)addr;
	while(TRUE)
	{
	    LOG("%s waiting for data\n", __func__);
	    kr = IODataQueueWaitForAvailableData(queue, port);
	    if(KERN_SUCCESS != kr)
	    {
		LOG("%s !!! IODataQueueWaitForAvailableData error %x !!!\n", __func__, kr);
		break;
	    }
	    while(IODataQueueDataAvailable(queue))
	    {
        	sz = sizeof(ioctl);
        	memset(&ioctl, 0, sizeof(ioctl));
        	kr = IODataQueueDequeue(queue, &ioctl, &sz);
        	LOG("%s data obtained\n", __func__);
        	if(KERN_SUCCESS != kr)
        	    LOG("%s !!! IODataQueueDequeue error %x !!!\n", __func__, kr);
        	else
        	{
		    do
		    {
        		arr = (CFArrayRef)IORegistryEntrySearchCFProperty(in_p->dev, kIOServicePlane, CFSTR(DEVICES), NULL,
        		    kIORegistryIterateRecursively | kIORegistryIterateParents);
        		LOG("%s arr %llx\n", __func__, (uint64_t)arr);
        		if((NULL != arr) && (CFArrayGetTypeID() == CFGetTypeID(arr)))
        		{
        		    LOG("%s %ld\n", __func__, CFArrayGetCount(arr));
        		    for(i = 0; i < CFArrayGetCount(arr); i++)
        		    {
        			dict = (CFDictionaryRef)CFArrayGetValueAtIndex(arr, i);
        			if((NULL != dict) && (CFDictionaryGetTypeID() == CFGetTypeID(dict)))
        			{
        			    sName = (CFStringRef)CFDictionaryGetValue(dict, CFSTR(DEVICE));
        			    LOG("%s %s\n", DEVICE, (char*)CString(sName));
        			    sPol = (CFStringRef)CFDictionaryGetValue(dict, CFSTR(POLICY));
        			    LOG("%s %s\n", POLICY, (char*)CString(sPol));
        			    nNum = (CFNumberRef)CFDictionaryGetValue(dict, CFSTR(SC));
        			    if(NULL != nNum && CFNumberGetTypeID() == CFGetTypeID(nNum))
        			    {
        				CFNumberGetValue(nNum, CFNumberGetType(nNum), &m);
        				LOG("%s %d\n", SC, m);
        			    }
				    parts = (CFArrayRef)CFDictionaryGetValue(dict, CFSTR(PARTITIONS));
				    if(NULL != parts && CFArrayGetTypeID() == CFGetTypeID(parts))
				    {
					for(m = 0; m < CFArrayGetCount(parts); m+=2)
					{
					    sMnt = (CFStringRef)CFArrayGetValueAtIndex(parts, m);
					    sDisk = (CFStringRef)CFArrayGetValueAtIndex(parts, m + 1);
					    LOG("%s %s\n", (char*)CString(sDisk), (char*)CString(sMnt));
					}
				    }
        			}
        			else
        			{
        			    LOG("%s !!! Invalid data in array !!!\n", __func__);
        			    continue;
        			}
        		    }
        		}
        		else
        		    LOG("%s !!! can't obtain a property !!!\n", __func__);
		    } while(false);
		    if(NULL != arr)
			CFRelease(arr);
        	}
	    }
	}
    } while (FALSE);
    if(0 < addr)
    {
	kr = IOConnectUnmapMemory(in_p->conn, kIODefaultMemoryType, mach_task_self(), addr);
	if(KERN_SUCCESS != kr)
	    LOG("%s !!! IOConnectUnmapMemory error %x !!!\n", __func__, kr);
    }
    if(IO_OBJECT_NULL != port)
    {
	kr = mach_port_destroy(mach_task_self(), port);
	if(KERN_SUCCESS != kr)
	    LOG("%s !!! mach_port_destroy error %x\n", __func__, kr);
    }
}

int main(int argc, char* argv[])
{
    typedef void* (*PTHREAD_FUNC)(void*);
    ZLCK_CONN_s Conn = {0};
    io_service_t zec_drv = IO_OBJECT_NULL;      // The XML registry driver descriptor
    io_connect_t conn_zec_drv = IO_OBJECT_NULL; // User Client/Driver connection descriptor
    int result = 0;
    uint64_t slide = 0;
    size_t size;
    int slide_enabled;
    pthread_t thread = NULL;

    do
    {
	LOG("%s %d\n", __func__, (int)sizeof(ZLCK_DEV_s));
	size = sizeof(slide_enabled);
	result = sysctlbyname("kern.slide", &slide_enabled, &size, NULL, 0);
	if (result == 0)
	{
	    printf("%s KALR %s\n", __func__, slide_enabled ? "enabled" : "disabled");
	    size = sizeof(slide);
	}
	else
	    printf("%s !!! sysctlbyname(\"kern.slide\") error %d %s !!!\n", __func__, errno, strerror(errno));
        zec_drv = find_Driver(ZECURION_USB_FILTER_DRIVER_NAME);
        if(IO_OBJECT_NULL == zec_drv)
        {
            LOG("%s !!! find_Driver error !!!\n", __func__);
            break;
        }
        conn_zec_drv = connect_Driver(zec_drv);
        if(IO_OBJECT_NULL == conn_zec_drv)
        {
            LOG("%s !!! connect_Driver error !!!\n", __func__);
            break;
        }
	Conn.dev = zec_drv;
	Conn.conn = conn_zec_drv;
	result = pthread_create(&thread, NULL, (PTHREAD_FUNC)NotifyHandler, (void*)&Conn);
	if(0 < result)
	{
	    LOG("%s !!! pthread_create error %d %s !!!\n", __func__, result, strerror(result));
	    break;
	}
	LOG("%s thread created\n", __func__);
	pthread_join(thread, NULL);
	disconnect_Driver(&conn_zec_drv);
    } while(false);
    return 0;
}