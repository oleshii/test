#!/bin/bash

rm -f evalexpr eval  eval.tab.c eval.tab.h lex.yy.c

# any parameter means clean and exit
if [ ! -z "$1" ]; then
    exit 0
fi

bison -d eval.y

flex eval.l

gcc -O0 -DDEBUG -Wall -Wno-unused-function -Wno-unneeded-internal-declaration -o evalexpr eval.tab.c lex.yy.c
