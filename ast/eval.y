%{

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <stdbool.h>

#define ASSERT assert
#define MAX_OP (7)

#if defined(DEBUG)
#define LOG    printf
#else
#define LOG(...)
#endif

int yyerror(const char *msg), yylex(), add_operation(char sig, int unary), add_operand(int val);

typedef struct _node {
    char sval[3];
    int ival;
    struct _node *next;     // list link
    struct _node *o_left;   // left operand for operation, for unary one it is alone
    struct _node *o_right;  // right operand for operation
    struct _node *owner;    // owner operation. may be unary for binary operation/binary or unary for digit/top priority operation in other cases/NULL for root
} node;

node *s_root = NULL, *s_last = NULL, // operations list
     *d_root = NULL, *d_last = NULL, // figures list
     *ast[MAX_OP] = {0};             // temporary operations stack, contains operations before reduce, on idea can't have more than 2 operation simultaneously

%}

%left '+' '-'
%left '*' '/' '%'
//%right '('
//%left  ')'

%precedence NEG

%token NUMBER

%%

E   : E '+' T     {
                        LOG("OPR %d + %d\n", $1, $3);
                        add_operation('+', 0);
                  }

    | E '-' T     {
                        LOG("OPR %d - %d\n", $1, $3);
                        add_operation('-', 0);
                  } //  2 - 3

    | T           {  }
    ;

T   : T '*' F     { LOG("OPR %d * %d\n", $1, $3); add_operation('*', 0); }
    | T '/' F     { LOG("OPR %d / %d\n", $1, $3); add_operation('/', 0); }
    | T '%' F     { LOG("OPR %d %% %d\n", $1, $3); add_operation('%', 0); }
    | F           {  }
    ;

F   : NUMBER      { add_operand($$); LOG("NUM %d\n", $$); } // add_operation($1, 0, 0);
    | '-' F       { LOG("OPR (-) %d\n", $2); add_operation('-', 1); } // VII
    | '+' F       { LOG("OPR (+) %d\n", $2); add_operation('+', 1); } // add_operand($2);
    | '('E')'     {  } // LOG("DBG (E) %d\n", $$);

%%

static inline bool is_owned(node *data)
{
    return data->owner != NULL;
}

static inline bool is_unary(node *oper)
{
    return oper->sval[0] == '(';
}

static inline bool is_oper_full(node *oper)
{
    ASSERT(oper->sval[0] != 0 && "!!! An incorrect node !!!");
    return is_unary(oper) ? oper->o_left != NULL : (oper->o_left != NULL && oper->o_right != NULL);
}

static node *allocate(void)
{
    node *tmp = malloc(sizeof(node));
    if (!tmp) {
        perror("!!! memory allocation error !!!");
        exit(-1);
    }
    return tmp;
}

static void fill(node *tmp, int val, char sig, int unary)
{
    memset(tmp, 0, sizeof(node));
    if (sig != 0) {
        if (unary != 0) {
            tmp->sval[0] = '(';
            tmp->sval[1] = sig;
            tmp->sval[2] = ')';
        }
        else
            tmp->sval[0] = sig;
    }
    else
        tmp->ival = val;
}

int add_operand(int val)
{
    node *n = allocate();
    //LOG("%lx\n", (unsigned long)n);
    fill(n, val, 0, 0);
    if (!d_root)
        d_root = n;
    else
        d_last->next = n;
    d_last = n;
    return 0;
}

static node *get_prev_leaf(node *rfig)
{
    node *p = d_root;
    ASSERT(d_last != d_root && "!!! A figures list is empty !!!");
    while(p->next != rfig && p->next != NULL)
        p = p->next;
    ASSERT(p != NULL && "!!! Couldn't find a left operand for binary peration !!!");
    return p;
}

static inline void figlist_display(void)
{
    node *tmp = d_root;
    while (tmp) {
        LOG("TREE (4) %d owned %s\n", tmp->ival, is_owned(tmp) ? "yes" : "no");
        tmp = tmp->next;
    }
}

static inline int ast_count(void)
{
    int top = 0;
    while (ast[top] != NULL) {
        ++top;
    }
    return top;
}

#define SET_LEFT(op, opd) { \
    op->o_left = opd;   \
    opd->owner = op;     \
}\

#define SET_RIGHT(op, opd) { \
    op->o_right = opd; \
    opd->owner = op;   \
}\

// operands handling
void operands_to_operation(int unary)
{
    node *tmp;
    ASSERT((s_last->o_right == NULL && s_last->o_left == NULL && !is_owned(s_last)) && "!!! Some data is absend !!!");
    ASSERT(d_last != NULL && "!!! Last figure does not exist !!!");
    do {
        if (unary != 0) { // unary operation has an one left operand only
            if (!is_owned(d_last))
                SET_LEFT(s_last, d_last); // unary operation should have an one left operand. It is last incoming figure
            break;
        }
        ASSERT(s_last->sval[1] == 0 && "!!! An incorrect operation !!!"); // a binary operation has both operands
        tmp = get_prev_leaf(d_last);
        ASSERT(tmp != NULL && "!!! no previous figure !!!");
        if (!is_owned(d_last) && !is_owned(tmp)) { // both operands are not owned
            SET_LEFT(s_last, d_last);
            SET_RIGHT(s_last, tmp);
            break;
        }
        if (!is_owned(d_last)) { // it the right one operand then
            SET_RIGHT(s_last, d_last);
            break;
        }
        if (!is_owned(tmp))
            SET_LEFT(s_last, tmp); // it is the left one operand then
    } while (false);
}

void operation_to_ast(node *op)
{
    int top = ast_count();
    ASSERT(op->sval[0] != 0 && "!!! An invalid operation node !!!");
    ASSERT(top < MAX_OP && "!!! AST stack full !!!");
    do {
        if (is_oper_full(op)) { // if an operation is full put it into the stack, because we can't reduce it
            LOG("TREE (1)\n");
            ast[top] = op;
            break;
        }
        ASSERT(top > 0 && "!!! Is not enough operations in the stack !!!");
        ASSERT(!is_owned(ast[top - 1]) && "!!! there is no operation in the stack !!!");
        if (is_unary(op)) { // unary operation pops from the stack an one operation only
            LOG("TREE (2)\n");
            SET_LEFT(op, ast[top - 1]);
            ast[top - 1] = op; // reduce stack in one operation
            break;
        }
        LOG("TREE (3) top %d right %s left %s\n", top, op->o_right ? "yes" : "no", op->o_left ? "yes" : "no");
        if (!op->o_right && !op->o_left) { // both operation are absent
            SET_RIGHT(op, ast[top - 1]);
            ast[top - 1] = NULL; // right reduce
            --top;
            ASSERT(!is_owned(ast[top - 1]) && "!!! pre last operation in the stack is owned !!!");
            SET_LEFT(op, ast[top - 1]);
            ast[top - 1] = op; // left reduce
            break;
        }
        if (!op->o_right) { // right operation is absent
            SET_RIGHT(op, ast[top - 1]);
            ast[top - 1] = op; // right reduce
            break;
        }
        ASSERT(!op->o_left && "!!! left operation non null !!!");
        SET_LEFT(op, ast[top - 1]);
        ast[top - 1] = op; // left reduce
    } while (false);
    ASSERT(op == ast[ast_count() - 1] && "!!! operation is not in the stack !!!");
    ASSERT(!is_owned(op) && "!!! operation top of stack is owned !!!");
    for (top = 0; top < ast_count(); ++top)
        LOG("TREE (5) ast[%d] owned %s\n", top, is_owned(ast[top]) ? "YES" : "no");
    LOG("TREE (6) %s full->%s %d\n", op->sval, is_oper_full(ast[ast_count() - 1]) ? "yes" : "no", ast_count());
}

int add_operation(char sig, int unary)
{
    node *n = allocate();
    //LOG("%lx\n", (unsigned long)n);
    fill(n, 0, sig, unary);
    if (!s_root)
        s_root = n;
    else
        s_last->next = n;
    s_last = n;
    operands_to_operation(unary);
    operation_to_ast(s_last);
    return 0;
}

static void release(node *n)
{
    while (n) {
        //LOG("%lx %lx\n", (unsigned long)n, (unsigned long)n->next);
        node* next = n->next;
        free(n);
        n = next;
    }
}

static inline void print_node(node *n)
{
    if (n->sval[0] == 0) // not a sign
        printf("%d\n", n->ival);
    else
        printf("%s\n", n->sval);
}

static void print_list(node* n)
{
    if (!n)
        return;
    print_node(n);
    print_list(n->next);
}

static void print_ast(node *top)
{
    if (!top)
        return;
    print_node(top);
    print_ast(top->o_left);
    print_ast(top->o_right);
}

int main(int argc, char* argv[])
{
    //ASSERT(false && "!!! TEST !!!");
    //ast[0] = (node*)0x1234567812345678;
    //LOG("test ast cnt %d\n", ast_count());
    //ast[0] = 0;
    //LOG("(- 11 + 7) * (31 - + 12)\n");
    yyparse();
    LOG("=========\n");
    //print_list(s_root);
    //print_list(d_root);
    //ASSERT(ast_count() == 1 && "!!! Not all the operations are reduced !!!");
    print_ast(ast[0]);
    release(s_root);
    release(d_root);
    return 0;
}

int yyerror(const char *msg)
{
    return fputs(msg, stderr);
}

#if 0
+ 25 * - (2 - 3)
===============
*
(+)
25
(-)
-
2
3
========================
(- 11 + 7) * (31 - + 12)
*                    *
+                    (-)
(-)                  11
11                   -
7                    +
-                    11
31                   7
(+)                  (+)
12                   12
========================

(- 11 + 7) * (31 - + 12)
NUM 11
OPR (-) 11
NUM 7
OPR 0 + 7          left_(BUSY) --- d_last(FREE)
NUM 31
NUM 12
OPR (+) 12         prev_(d_last(FREE)) --- d_last(BUSY)
OPR 31 - 31
OPR 0 * 7
=========
+ 25 * - (2 - 3)
NUM 25
OPR (+) 25
NUM 2
NUM 3
OPR 2 - 3
OPR (-) 25
OPR 0 * 25
=========
#endif