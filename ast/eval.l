%option noyywrap

%{
#include "eval.tab.h"
%}

digit [0-9]

space [ \t]

%%

"+"           { return '+'; }
"*"           { return '*'; }
"-"           { return '-'; }
"/"           { return '/'; }
"%"           { return '%'; }
"("           { return '('; }
")"           { return ')'; }

{digit}+      { yylval = atoi(yytext); return NUMBER; }
{space}+      { }
\n            { return 0; }

%%

//{alpha}+      { yylval.sval = yytext; return NAME; }
//alpha [a-zA-Z]
//int yywrap() { return 1; }
