#include <windows.h>
#include <fwpmu.h>
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include "zne_wfp_guids.h"
#include "wfp.h"

// #include <typeinfo> it's not necessary, the MS C++ compiler in __FUNCTION__ macro for class members dumps class name

#if defined(ASSERT)
#undef 	ASSERT
#endif

#define	ASSERT	assert

#define	LOG	log
#define	IS_SUCCEEDED(e)	(0 == e)
#define	INVALID_HANDLE	(HANDLE)-1

#ifndef	USHORT_MAX
#define USHORT_MAX      0xffff
#endif

#if !defined(FWPSX_H) 
typedef enum FWPS_BUILTIN_LAYERS_
{
   // Kernel-mode layers
   FWPS_LAYER_INBOUND_IPPACKET_V4,
   FWPS_LAYER_INBOUND_IPPACKET_V4_DISCARD,
   FWPS_LAYER_INBOUND_IPPACKET_V6,
   FWPS_LAYER_INBOUND_IPPACKET_V6_DISCARD,
   FWPS_LAYER_OUTBOUND_IPPACKET_V4,
   FWPS_LAYER_OUTBOUND_IPPACKET_V4_DISCARD,
   FWPS_LAYER_OUTBOUND_IPPACKET_V6,
   FWPS_LAYER_OUTBOUND_IPPACKET_V6_DISCARD,
   FWPS_LAYER_IPFORWARD_V4,
   FWPS_LAYER_IPFORWARD_V4_DISCARD,
   FWPS_LAYER_IPFORWARD_V6,
   FWPS_LAYER_IPFORWARD_V6_DISCARD,
   FWPS_LAYER_INBOUND_TRANSPORT_V4,
   FWPS_LAYER_INBOUND_TRANSPORT_V4_DISCARD,
   FWPS_LAYER_INBOUND_TRANSPORT_V6,
   FWPS_LAYER_INBOUND_TRANSPORT_V6_DISCARD,
   FWPS_LAYER_OUTBOUND_TRANSPORT_V4,
   FWPS_LAYER_OUTBOUND_TRANSPORT_V4_DISCARD,
   FWPS_LAYER_OUTBOUND_TRANSPORT_V6,
   FWPS_LAYER_OUTBOUND_TRANSPORT_V6_DISCARD,
   FWPS_LAYER_STREAM_V4,
   FWPS_LAYER_STREAM_V4_DISCARD,
   FWPS_LAYER_STREAM_V6,
   FWPS_LAYER_STREAM_V6_DISCARD,
   FWPS_LAYER_DATAGRAM_DATA_V4,
   FWPS_LAYER_DATAGRAM_DATA_V4_DISCARD,
   FWPS_LAYER_DATAGRAM_DATA_V6,
   FWPS_LAYER_DATAGRAM_DATA_V6_DISCARD,
   FWPS_LAYER_INBOUND_ICMP_ERROR_V4,
   FWPS_LAYER_INBOUND_ICMP_ERROR_V4_DISCARD,
   FWPS_LAYER_INBOUND_ICMP_ERROR_V6,
   FWPS_LAYER_INBOUND_ICMP_ERROR_V6_DISCARD,
   FWPS_LAYER_OUTBOUND_ICMP_ERROR_V4,
   FWPS_LAYER_OUTBOUND_ICMP_ERROR_V4_DISCARD,
   FWPS_LAYER_OUTBOUND_ICMP_ERROR_V6,
   FWPS_LAYER_OUTBOUND_ICMP_ERROR_V6_DISCARD,
   FWPS_LAYER_ALE_RESOURCE_ASSIGNMENT_V4,
   FWPS_LAYER_ALE_RESOURCE_ASSIGNMENT_V4_DISCARD,
   FWPS_LAYER_ALE_RESOURCE_ASSIGNMENT_V6,
   FWPS_LAYER_ALE_RESOURCE_ASSIGNMENT_V6_DISCARD,
   FWPS_LAYER_ALE_AUTH_LISTEN_V4,
   FWPS_LAYER_ALE_AUTH_LISTEN_V4_DISCARD,
   FWPS_LAYER_ALE_AUTH_LISTEN_V6,
   FWPS_LAYER_ALE_AUTH_LISTEN_V6_DISCARD,
   FWPS_LAYER_ALE_AUTH_RECV_ACCEPT_V4,
   FWPS_LAYER_ALE_AUTH_RECV_ACCEPT_V4_DISCARD,
   FWPS_LAYER_ALE_AUTH_RECV_ACCEPT_V6,
   FWPS_LAYER_ALE_AUTH_RECV_ACCEPT_V6_DISCARD,
   FWPS_LAYER_ALE_AUTH_CONNECT_V4,
   FWPS_LAYER_ALE_AUTH_CONNECT_V4_DISCARD,
   FWPS_LAYER_ALE_AUTH_CONNECT_V6,
   FWPS_LAYER_ALE_AUTH_CONNECT_V6_DISCARD,
   FWPS_LAYER_ALE_FLOW_ESTABLISHED_V4,
   FWPS_LAYER_ALE_FLOW_ESTABLISHED_V4_DISCARD,
   FWPS_LAYER_ALE_FLOW_ESTABLISHED_V6,
   FWPS_LAYER_ALE_FLOW_ESTABLISHED_V6_DISCARD,
#if (NTDDI_VERSION >= NTDDI_WIN7)
   FWPS_LAYER_INBOUND_MAC_FRAME_802_3,
   FWPS_LAYER_OUTBOUND_MAC_FRAME_802_3,
   FWPS_LAYER_RESERVED1_V4,
   FWPS_LAYER_RESERVED1_V6,
   FWPS_LAYER_NAME_RESOLUTION_CACHE_V4,
   FWPS_LAYER_NAME_RESOLUTION_CACHE_V6,   
   FWPS_LAYER_ALE_RESOURCE_RELEASE_V4,
   FWPS_LAYER_ALE_RESOURCE_RELEASE_V6,
   FWPS_LAYER_ALE_ENDPOINT_CLOSURE_V4,
   FWPS_LAYER_ALE_ENDPOINT_CLOSURE_V6,
   FWPS_LAYER_ALE_CONNECT_REDIRECT_V4,
   FWPS_LAYER_ALE_CONNECT_REDIRECT_V6,
   FWPS_LAYER_ALE_BIND_REDIRECT_V4,
   FWPS_LAYER_ALE_BIND_REDIRECT_V6,
   FWPS_LAYER_STREAM_PACKET_V4,
   FWPS_LAYER_STREAM_PACKET_V6,
#endif // (NTDDI_VERSION >= NTDDI_WIN7)

   // User-mode layers
   FWPS_LAYER_IPSEC_KM_DEMUX_V4,
   FWPS_LAYER_IPSEC_KM_DEMUX_V6,
   FWPS_LAYER_IPSEC_V4,
   FWPS_LAYER_IPSEC_V6,
   FWPS_LAYER_IKEEXT_V4,
   FWPS_LAYER_IKEEXT_V6,
   FWPS_LAYER_RPC_UM,
   FWPS_LAYER_RPC_EPMAP,
   FWPS_LAYER_RPC_EP_ADD,
   FWPS_LAYER_RPC_PROXY_CONN,
   FWPS_LAYER_RPC_PROXY_IF,
#if (NTDDI_VERSION >= NTDDI_WIN7)
   FWPS_LAYER_KM_AUTHORIZATION,
#endif // (NTDDI_VERSION >= NTDDI_WIN7)
   FWPS_BUILTIN_LAYER_MAX
} FWPS_BUILTIN_LAYERS;

// gotta fuck ODR for C++, make linker happy
namespace XGUIDS
{
// c6e63c8c-b784-4562-aa7d-0a67cfcaf9a3
GUID FWPM_LAYER_ALE_CONNECT_REDIRECT_V4 = {0xc6e63c8c, 0xb784, 0x4562, 0xaa, 0x7d, 0x0a, 0x67, 0xcf, 0xca, 0xf9, 0xa3};
// 3b89653c-c170-49e4-b1cd-e0eeeee19a3e
GUID FWPM_LAYER_STREAM_V4 = {0x3b89653c, 0xc170, 0x49e4, 0xb1, 0xcd, 0xe0, 0xee, 0xee, 0xe1, 0x9a, 0x3e};
// b4766427-e2a2-467a-bd7e-dbcd1bd85a09
GUID FWPM_LAYER_ALE_ENDPOINT_CLOSURE_V4 = {0xb4766427, 0xe2a2, 0x467a, 0xbd, 0x7e, 0xdb, 0xcd, 0x1b, 0xd8, 0x5a, 0x09};
// af80470a-5596-4c13-9992-539e6fe57967
GUID FWPM_LAYER_ALE_FLOW_ESTABLISHED_V4 = {0xaf80470a, 0x5596, 0x4c13, 0x99, 0x92, 0x53, 0x9e, 0x6f, 0xe5, 0x79, 0x67};
// 3d08bf4e-45f6-4930-a922-417098e20027
GUID FWPM_LAYER_DATAGRAM_DATA_V4 = {0x3d08bf4e, 0x45f6, 0x4930, 0xa9, 0x22, 0x41, 0x70, 0x98, 0xe2, 0x00, 0x27};
// c38d57d1-05a7-4c33-904f-7fbceee60e82
GUID FWPM_LAYER_ALE_AUTH_CONNECT_V4 = {0xc38d57d1, 0x05a7, 0x4c33, 0x90, 0x4f, 0x7f, 0xbc, 0xee, 0xe6, 0x0e, 0x82};
// 0c1ba1af-5765-453f-af22-a8f791ac775b
GUID FWPM_CONDITION_IP_LOCAL_PORT = {0x0c1ba1af, 0x5765, 0x453f, 0xaf, 0x22, 0xa8, 0xf7, 0x91, 0xac, 0x77, 0x5b};
// 1247d66d-0b60-4a15-8d44-7155d0f53a0c
GUID FWPM_LAYER_ALE_RESOURCE_ASSIGNMENT_V4 = {0x1247d66d, 0x0b60, 0x4a15, 0x8d, 0x44, 0x71, 0x55, 0xd0, 0xf5, 0x3a, 0x0c};
// c86fd1bf-21cd-497e-a0bb-17425c885c58
GUID FWPM_LAYER_INBOUND_IPPACKET_V4 = {0xc86fd1bf, 0x21cd, 0x497e, 0xa0, 0xbb, 0x17, 0x42, 0x5c, 0x88, 0x5c, 0x58};
// f52032cb-991c-46e7-971d-2601459a91ca
GUID FWPM_LAYER_INBOUND_IPPACKET_V6 = {0xf52032cb, 0x991c, 0x46e7, 0x97, 0x1d, 0x26, 0x01, 0x45, 0x9a, 0x91, 0xca};
// a3b3ab6b-3564-488c-9117-f34e82142763
GUID FWPM_LAYER_OUTBOUND_IPPACKET_V6 = {0xa3b3ab6b, 0x3564, 0x488c, 0x91, 0x17, 0xf3, 0x4e, 0x82, 0x14, 0x27, 0x63};
// 1e5c9fae-8a84-4135-a331-950b54229ecd
GUID FWPM_LAYER_OUTBOUND_IPPACKET_V4 = {0x1e5c9fae, 0x8a84, 0x4135, 0xa3, 0x31, 0x95, 0x0b, 0x54, 0x22, 0x9e, 0xcd};
// bb536ccd-4755-4ba9-9ff7-f9edf8699c7b
GUID FWPM_LAYER_ALE_ENDPOINT_CLOSURE_V6 = {0xbb536ccd, 0x4755, 0x4ba9, 0x9f, 0xf7, 0xf9, 0xed, 0xf8, 0x69, 0x9c, 0x7b};
// 55a650e1-5f0a-4eca-a653-88f53b26aa8c
GUID FWPM_LAYER_ALE_RESOURCE_ASSIGNMENT_V6 = {0x55a650e1, 0x5f0a, 0x4eca, 0xa6, 0x53, 0x88, 0xf5, 0x3b, 0x26, 0xaa, 0x8c};
// 4a72393b-319f-44bc-84c3-ba54dcb3b6b4
GUID FWPM_LAYER_ALE_AUTH_CONNECT_V6 = {0x4a72393b, 0x319f, 0x44bc, 0x84, 0xc3, 0xba, 0x54, 0xdc, 0xb3, 0xb6, 0xb4};
// e1cd9fe7-f4b5-4273-96c0-592e487b8650
GUID FWPM_LAYER_ALE_AUTH_RECV_ACCEPT_V4 = {0xe1cd9fe7, 0xf4b5, 0x4273, 0x96, 0xc0, 0x59, 0x2e, 0x48, 0x7b, 0x86, 0x50};
// a3b42c97-9f04-4672-b87e-cee9c483257f
GUID FWPM_LAYER_ALE_AUTH_RECV_ACCEPT_V6 = {0xa3b42c97, 0x9f04, 0x4672, 0xb8, 0x7e, 0xce, 0xe9, 0xc4, 0x83, 0x25, 0x7f};
// 5926dfc8-e3cf-4426-a283-dc393f5d0f9d
GUID FWPM_LAYER_INBOUND_TRANSPORT_V4 = {0x5926dfc8, 0xe3cf, 0x4426, 0xa2, 0x83, 0xdc, 0x39, 0x3f, 0x5d, 0x0f, 0x9d};
// 634a869f-fc23-4b90-b0c1-bf620a36ae6f
GUID FWPM_LAYER_INBOUND_TRANSPORT_V6 = {0x634a869f, 0xfc23, 0x4b90, 0xb0, 0xc1, 0xbf, 0x62, 0x0a, 0x36, 0xae, 0x6f};
// 09e61aea-d214-46e2-9b21-b26b0b2f28c8
GUID FWPM_LAYER_OUTBOUND_TRANSPORT_V4 = {0x09e61aea, 0xd214, 0x46e2, 0x9b, 0x21, 0xb2, 0x6b, 0x0b, 0x2f, 0x28, 0xc8};
// e1735bde-013f-4655-b351-a49e15762df0
GUID FWPM_LAYER_OUTBOUND_TRANSPORT_V6 = {0xe1735bde, 0x013f, 0x4655, 0xb3, 0x51, 0xa4, 0x9e, 0x15, 0x76, 0x2d, 0xf0};
// 587e54a7-8046-42ba-a0aa-b716250fc7fd
GUID FWPM_LAYER_ALE_CONNECT_REDIRECT_V6 = {0x587e54a7, 0x8046, 0x42ba, 0xa0, 0xaa, 0xb7, 0x16, 0x25, 0x0f, 0xc7, 0xfd};
// af52d8ec-cb2d-44e5-ad92-f8dc38d2eb29
GUID FWPM_LAYER_STREAM_PACKET_V4 = {0xaf52d8ec, 0xcb2d, 0x44e5, 0xad, 0x92, 0xf8, 0xdc, 0x38, 0xd2, 0xeb, 0x29};
// 779a8ca3-f099-468f-b5d4-83535c461c02
GUID FWPM_LAYER_STREAM_PACKET_V6 = {0x779a8ca3, 0xf099, 0x468f, 0xb5, 0xd4, 0x83, 0x53, 0x5c, 0x46, 0x1c, 0x02};
// 7021d2b3-dfa4-406e-afeb-6afaf7e70efd
GUID FWPM_LAYER_ALE_FLOW_ESTABLISHED_V6 = {0x7021d2b3, 0xdfa4, 0x406e, 0xaf, 0xeb, 0x6a, 0xfa, 0xf7, 0xe7, 0x0e, 0xfd};
// ac4a9833-f69d-4648-b261-6dc84835ef39
GUID FWPM_LAYER_INBOUND_TRANSPORT_V4_DISCARD = {0xac4a9833, 0xf69d, 0x4648, 0xb2, 0x61, 0x6d, 0xc8, 0x48, 0x35, 0xef, 0x39};
// 2a6ff955-3b2b-49d2-9848-ad9d72dcaab7
GUID FWPM_LAYER_INBOUND_TRANSPORT_V6_DISCARD = {0x2a6ff955, 0x3b2b, 0x49d2, 0x98, 0x48, 0xad, 0x9d, 0x72, 0xdc, 0xaa, 0xb7};
// c5f10551-bdb0-43d7-a313-50e211f4d68a
GUID FWPM_LAYER_OUTBOUND_TRANSPORT_V4_DISCARD = {0xc5f10551, 0xbdb0, 0x43d7, 0xa3, 0x13, 0x50, 0xe2, 0x11, 0xf4, 0xd6, 0x8a};
// f433df69-ccbd-482e-b9b2-57165658c3b3
GUID FWPM_LAYER_OUTBOUND_TRANSPORT_V6_DISCARD = {0xf433df69, 0xccbd, 0x482e, 0xb9, 0xb2, 0x57, 0x16, 0x56, 0x58, 0xc3, 0xb3};
// b5a230d0-a8c0-44f2-916e-991b53ded1f7
GUID FWPM_LAYER_INBOUND_IPPACKET_V4_DISCARD = {0xb5a230d0, 0xa8c0, 0x44f2, 0x91, 0x6e, 0x99, 0x1b, 0x53, 0xde, 0xd1, 0xf7};
// 08e4bcb5-b647-48f3-953c-e5ddbd03937e
GUID FWPM_LAYER_OUTBOUND_IPPACKET_V4_DISCARD = {0x08e4bcb5, 0xb647, 0x48f3, 0x95, 0x3c, 0xe5, 0xdd, 0xbd, 0x03, 0x93, 0x7e};
// bb24c279-93b4-47a2-83ad-ae1698b50885
GUID FWPM_LAYER_INBOUND_IPPACKET_V6_DISCARD = {0xbb24c279, 0x93b4, 0x47a2, 0x83, 0xad, 0xae, 0x16, 0x98, 0xb5, 0x08, 0x85};
// 9513d7c4-a934-49dc-91a7-6ccb80cc02e3
GUID FWPM_LAYER_OUTBOUND_IPPACKET_V6_DISCARD = {0x9513d7c4, 0xa934, 0x49dc, 0x91, 0xa7, 0x6c, 0xcb, 0x80, 0xcc, 0x02, 0xe3};
// 47c9137a-7ec4-46b3-b6e4-48e926b1eda4
GUID FWPM_LAYER_STREAM_V6 = {0x47c9137a, 0x7ec4, 0x46b3, 0xb6, 0xe4, 0x48, 0xe9, 0x26, 0xb1, 0xed, 0xa4};
// fa45fe2f-3cba-4427-87fc-57b9a4b10d00
GUID FWPM_LAYER_DATAGRAM_DATA_V6 = {0xfa45fe2f, 0x3cba, 0x4427, 0x87, 0xfc, 0x57, 0xb9, 0xa4, 0xb1, 0x0d, 0x00};
}; // XGUIDS
#endif // FWPSX_H

#define MEMCMP(a, b)	(memcmp(a, &b, sizeof(b)) == 0)
#define GUID2NAME(g)\
MEMCMP(g, XGUIDS::FWPM_LAYER_ALE_RESOURCE_ASSIGNMENT_V4) ? "ALE_RESOURCE_ASSIGNMENT_V4":\
MEMCMP(g, XGUIDS::FWPM_LAYER_ALE_RESOURCE_ASSIGNMENT_V6) ? "ALE_RESOURCE_ASSIGNMENT_V6":\
MEMCMP(g, XGUIDS::FWPM_LAYER_ALE_AUTH_CONNECT_V4) ? "ALE_AUTH_CONNECT_V4":\
MEMCMP(g, XGUIDS::FWPM_LAYER_ALE_AUTH_CONNECT_V6) ? "ALE_AUTH_CONNECT_V6":\
MEMCMP(g, XGUIDS::FWPM_LAYER_ALE_AUTH_RECV_ACCEPT_V4) ? "ALE_AUTH_RECV_ACCEPT_V4":\
MEMCMP(g, XGUIDS::FWPM_LAYER_ALE_AUTH_RECV_ACCEPT_V6) ? "ALE_AUTH_RECV_ACCEPT_V6":\
MEMCMP(g, XGUIDS::FWPM_LAYER_INBOUND_TRANSPORT_V4) ? "INBOUND_TRANSPORT_V4":\
MEMCMP(g, XGUIDS::FWPM_LAYER_INBOUND_TRANSPORT_V6) ? "INBOUND_TRANSPORT_V6":\
MEMCMP(g, XGUIDS::FWPM_LAYER_OUTBOUND_TRANSPORT_V4) ? "OUTBOUND_TRANSPORT_V4":\
MEMCMP(g, XGUIDS::FWPM_LAYER_OUTBOUND_TRANSPORT_V6) ? "OUTBOUND_TRANSPORT_V6":\
MEMCMP(g, XGUIDS::FWPM_LAYER_ALE_CONNECT_REDIRECT_V4) ? "ALE_CONNECT_REDIRECT_V4":\
MEMCMP(g, XGUIDS::FWPM_LAYER_ALE_CONNECT_REDIRECT_V6) ? "ALE_CONNECT_REDIRECT_V6":\
MEMCMP(g, XGUIDS::FWPM_LAYER_STREAM_PACKET_V4) ? "STREAM_PACKET_V4":\
MEMCMP(g, XGUIDS::FWPM_LAYER_STREAM_PACKET_V6) ? "STREAM_PACKET_V6":\
MEMCMP(g, XGUIDS::FWPM_LAYER_ALE_ENDPOINT_CLOSURE_V4) ? "ALE_ENDPOINT_CLOSURE_V4":\
MEMCMP(g, XGUIDS::FWPM_LAYER_ALE_ENDPOINT_CLOSURE_V6) ? "ALE_ENDPOINT_CLOSURE_V6":\
MEMCMP(g, XGUIDS::FWPM_LAYER_ALE_FLOW_ESTABLISHED_V4) ? "ALE_FLOW_ESTABLISHED_V4":\
MEMCMP(g, XGUIDS::FWPM_LAYER_ALE_FLOW_ESTABLISHED_V6) ? "ALE_FLOW_ESTABLISHED_V6":\
MEMCMP(g, XGUIDS::FWPM_LAYER_INBOUND_TRANSPORT_V4_DISCARD) ? "INBOUND_TRANSPORT_V4_DISCARD":\
MEMCMP(g, XGUIDS::FWPM_LAYER_INBOUND_TRANSPORT_V6_DISCARD) ? "INBOUND_TRANSPORT_V6_DISCARD":\
MEMCMP(g, XGUIDS::FWPM_LAYER_OUTBOUND_TRANSPORT_V4_DISCARD) ? "OUTBOUND_TRANSPORT_V4_DISCARD":\
MEMCMP(g, XGUIDS::FWPM_LAYER_OUTBOUND_TRANSPORT_V6_DISCARD) ? "OUTBOUND_TRANSPORT_V6_DISCARD":\
MEMCMP(g, XGUIDS::FWPM_LAYER_INBOUND_IPPACKET_V4) ? "INBOUND_IPPACKET_V4":\
MEMCMP(g, XGUIDS::FWPM_LAYER_OUTBOUND_IPPACKET_V4) ? "OUTBOUND_IPPACKET_V4":\
MEMCMP(g, XGUIDS::FWPM_LAYER_INBOUND_IPPACKET_V4_DISCARD) ? "INBOUND_IPPACKET_V4_DISCARD":\
MEMCMP(g, XGUIDS::FWPM_LAYER_OUTBOUND_IPPACKET_V4_DISCARD) ? "OUTBOUND_IPPACKET_V4_DISCARD":\
MEMCMP(g, XGUIDS::FWPM_LAYER_INBOUND_IPPACKET_V6) ? "INBOUND_IPPACKET_V6":\
MEMCMP(g, XGUIDS::FWPM_LAYER_OUTBOUND_IPPACKET_V6) ? "OUTBOUND_IPPACKET_V6":\
MEMCMP(g, XGUIDS::FWPM_LAYER_INBOUND_IPPACKET_V6_DISCARD) ? "INBOUND_IPPACKET_V6_DISCARD":\
MEMCMP(g, XGUIDS::FWPM_LAYER_OUTBOUND_IPPACKET_V6_DISCARD) ? "OUTBOUND_IPPACKET_V6_DISCARD":\
MEMCMP(g, XGUIDS::FWPM_LAYER_STREAM_V4) ? "STREAM_V4":\
MEMCMP(g, XGUIDS::FWPM_LAYER_STREAM_V6) ? "STREAM_V6":\
MEMCMP(g, XGUIDS::FWPM_LAYER_STREAM_PACKET_V4) ? "STREAM_PACKET_V4":\
MEMCMP(g, XGUIDS::FWPM_LAYER_STREAM_PACKET_V6) ? "STREAM_PACKET_V6":\
MEMCMP(g, XGUIDS::FWPM_LAYER_DATAGRAM_DATA_V4) ? "DATAGRAM_DATA_V4":\
MEMCMP(g, XGUIDS::FWPM_LAYER_DATAGRAM_DATA_V6) ? "DATAGRAM_DATA_V6":"UNKNOWN"

// more than 63 terra operators in macro don't supported by MS C++ compiler
// the volatile modifier don't supported by MS C++ compiler properly
#define	ID2GUIDN(id)	idToGuidName(id)
#define	IDTOERR(id)	errIdToText(id)
#define TRACE_GUID(g)   LOG("%s {%x-%x-%x-%x-%x-%x-%x-%x-%x-%x-%x}\n", __FUNCTION__,\
	g.Data1, g.Data2, g.Data3, g.Data4[0], g.Data4[1], g.Data4[2], g.Data4[3],\
	g.Data4[4], g.Data4[5], g.Data4[6], g.Data4[7])

__forceinline static const char* idToGuidName(unsigned short id)
{
    if(id == FWPS_LAYER_INBOUND_IPPACKET_V4)
	return "INBOUND_IPPACKET_V4";
    if(id == FWPS_LAYER_INBOUND_IPPACKET_V4_DISCARD)
	return "INBOUND_IPPACKET_V4_DISCARD";
    if(id == FWPS_LAYER_INBOUND_IPPACKET_V6)
	return "INBOUND_IPPACKET_V6";
    if(id == FWPS_LAYER_INBOUND_IPPACKET_V6_DISCARD)
	return "INBOUND_IPPACKET_V6_DISCARD";
    if(id == FWPS_LAYER_OUTBOUND_IPPACKET_V4)
	return "OUTBOUND_IPPACKET_V4";
    if(id == FWPS_LAYER_OUTBOUND_IPPACKET_V4_DISCARD)
	return "OUTBOUND_IPPACKET_V4_DISCARD";
    if(id == FWPS_LAYER_OUTBOUND_IPPACKET_V6)
	return "OUTBOUND_IPPACKET_V6";
    if(id == FWPS_LAYER_OUTBOUND_IPPACKET_V6_DISCARD)
	return "IPPACKET_V6_DISCARD";
    if(id == FWPS_LAYER_IPFORWARD_V4)
	return "IPFORWARD_V4";
    if(id == FWPS_LAYER_IPFORWARD_V4_DISCARD)
	return "IPFORWARD_V4_DISCARD";
    if(id == FWPS_LAYER_IPFORWARD_V6)
	return "IPFORWARD_V6";
    if(id == FWPS_LAYER_IPFORWARD_V6_DISCARD)
	return "IPFORWARD_V6_DISCARD";
    if(id == FWPS_LAYER_INBOUND_TRANSPORT_V4)
	return "INBOUND_TRANSPORT_V4";
    if(id == FWPS_LAYER_INBOUND_TRANSPORT_V4_DISCARD)
	return "INBOUND_TRANSPORT_V4_DISCARD";
    if(id == FWPS_LAYER_INBOUND_TRANSPORT_V6)
	return "INBOUND_TRANSPORT_V6";
    if(id == FWPS_LAYER_INBOUND_TRANSPORT_V6_DISCARD)
	return "INBOUND_TRANSPORT_V6_DISCARD";
    if(id == FWPS_LAYER_OUTBOUND_TRANSPORT_V4)
	return "OUTBOUND_TRANSPORT_V4";
    if(id == FWPS_LAYER_OUTBOUND_TRANSPORT_V4_DISCARD)
	return "OUTBOUND_TRANSPORT_V4_DISCARD";
    if(id == FWPS_LAYER_OUTBOUND_TRANSPORT_V6)
	return "OUTBOUND_TRANSPORT_V6";
    if(id == FWPS_LAYER_OUTBOUND_TRANSPORT_V6_DISCARD)
	return "OUTBOUND_TRANSPORT_V6_DISCARD";
    if(id == FWPS_LAYER_STREAM_V4)
	return "STREAM_V4";
    if(id == FWPS_LAYER_STREAM_V4_DISCARD)
	return "STREAM_V4_DISCARD";
    if(id == FWPS_LAYER_STREAM_V6)
	return "FWPS_LAYER_STREAM_V6";
    if(id == FWPS_LAYER_STREAM_V6_DISCARD)
	return "FWPS_LAYER_STREAM_V6_DISCARD";
    if(id == FWPS_LAYER_DATAGRAM_DATA_V4)
	return "DATAGRAM_DATA_V4";
    if(id == FWPS_LAYER_DATAGRAM_DATA_V4_DISCARD)
	return "DATAGRAM_DATA_V4_DISCARD";
    if(id == FWPS_LAYER_DATAGRAM_DATA_V6)
	return "DATAGRAM_DATA_V6";
    if(id == FWPS_LAYER_DATAGRAM_DATA_V6_DISCARD)
	return "DATAGRAM_DATA_V6_DISCARD";
    if(id == FWPS_LAYER_INBOUND_ICMP_ERROR_V4)
	return "INBOUND_ICMP_ERROR_V4";
    if(id == FWPS_LAYER_INBOUND_ICMP_ERROR_V4_DISCARD)
	return "INBOUND_ICMP_ERROR_V4_DISCARD";
    if(id == FWPS_LAYER_INBOUND_ICMP_ERROR_V6)
	return "INBOUND_ICMP_ERROR_V6";
    if(id == FWPS_LAYER_INBOUND_ICMP_ERROR_V6_DISCARD)
	return "INBOUND_ICMP_ERROR_V6_DISCARD";
    if(id == FWPS_LAYER_OUTBOUND_ICMP_ERROR_V4)
	return "OUTBOUND_ICMP_ERROR_V4";
    if(id == FWPS_LAYER_OUTBOUND_ICMP_ERROR_V4_DISCARD)
	return "OUTBOUND_ICMP_ERROR_V4_DISCARD";
    if(id == FWPS_LAYER_OUTBOUND_ICMP_ERROR_V6)
	return "OUTBOUND_ICMP_ERROR_V6";
    if(id == FWPS_LAYER_OUTBOUND_ICMP_ERROR_V6_DISCARD)
	return "OUTBOUND_ICMP_ERROR_V6_DISCARD";
    if(id == FWPS_LAYER_ALE_RESOURCE_ASSIGNMENT_V4)
	return "ALE_RESOURCE_ASSIGNMENT_V4";
    if(id == FWPS_LAYER_ALE_RESOURCE_ASSIGNMENT_V4_DISCARD)
	return "ALE_RESOURCE_ASSIGNMENT_V4_DISCARD";
    if(id == FWPS_LAYER_ALE_RESOURCE_ASSIGNMENT_V6)
	return "ALE_RESOURCE_ASSIGNMENT_V6";
    if(id == FWPS_LAYER_ALE_RESOURCE_ASSIGNMENT_V6_DISCARD)
	return "ALE_RESOURCE_ASSIGNMENT_V6_DISCARD";
    if(id == FWPS_LAYER_ALE_AUTH_LISTEN_V4)
	return "ALE_AUTH_LISTEN_V4";
    if(id == FWPS_LAYER_ALE_AUTH_LISTEN_V4_DISCARD)
	return "ALE_AUTH_LISTEN_V4_DISCARD";
    if(id == FWPS_LAYER_ALE_AUTH_LISTEN_V6)
	return "ALE_AUTH_LISTEN_V6";
    if(id == FWPS_LAYER_ALE_AUTH_LISTEN_V6_DISCARD)
	return "ALE_AUTH_LISTEN_V6_DISCARD";
    if(id == FWPS_LAYER_ALE_AUTH_RECV_ACCEPT_V4)
	return "ALE_AUTH_RECV_ACCEPT_V4";
    if(id == FWPS_LAYER_ALE_AUTH_RECV_ACCEPT_V4_DISCARD)
	return "ALE_AUTH_RECV_ACCEPT_V4_DISCARD";
    if(id == FWPS_LAYER_ALE_AUTH_RECV_ACCEPT_V6)
	return "ALE_AUTH_RECV_ACCEPT_V6";
    if(id == FWPS_LAYER_ALE_AUTH_RECV_ACCEPT_V6_DISCARD)
	return "ALE_AUTH_RECV_ACCEPT_V6_DISCARD";
    if(id == FWPS_LAYER_ALE_AUTH_CONNECT_V4)
	return "ALE_AUTH_CONNECT_V4";
    if(id == FWPS_LAYER_ALE_AUTH_CONNECT_V4_DISCARD)
	return "ALE_AUTH_CONNECT_V4_DISCARD";
    if(id == FWPS_LAYER_ALE_AUTH_CONNECT_V6)
	return "ALE_AUTH_CONNECT_V6";
    if(id == FWPS_LAYER_ALE_AUTH_CONNECT_V6_DISCARD)
	return "ALE_AUTH_CONNECT_V6_DISCARD";
    if(id == FWPS_LAYER_ALE_FLOW_ESTABLISHED_V4)
	return "ALE_FLOW_ESTABLISHED_V4";
    if(id == FWPS_LAYER_ALE_FLOW_ESTABLISHED_V4_DISCARD)
	return "ALE_FLOW_ESTABLISHED_V4_DISCARD";
    if(id == FWPS_LAYER_ALE_FLOW_ESTABLISHED_V6)
	return "ALE_FLOW_ESTABLISHED_V6";
    if(id == FWPS_LAYER_ALE_FLOW_ESTABLISHED_V6_DISCARD)
	return "ALE_FLOW_ESTABLISHED_V6_DISCARD";
    if(id == FWPS_LAYER_INBOUND_MAC_FRAME_802_3)
	return "INBOUND_MAC_FRAME_802_3";
    if(id == FWPS_LAYER_OUTBOUND_MAC_FRAME_802_3)
	return "UTBOUND_MAC_FRAME_802_3";
    if(id == FWPS_LAYER_RESERVED1_V4 || id == FWPS_LAYER_RESERVED1_V6)
	return "RESERVED_V4/V6";
    if(id == FWPS_LAYER_NAME_RESOLUTION_CACHE_V4)
	return "NAME_RESOLUTION_CACHE_V4";
    if(id == FWPS_LAYER_NAME_RESOLUTION_CACHE_V6)
	return "NAME_RESOLUTION_CACHE_V6";
    if(id == FWPS_LAYER_ALE_RESOURCE_RELEASE_V4)
	return "ALE_RESOURCE_RELEASE_V4";
    if(id == FWPS_LAYER_ALE_RESOURCE_RELEASE_V6)
	return "ALE_RESOURCE_RELEASE_V6";
    if(id == FWPS_LAYER_ALE_ENDPOINT_CLOSURE_V4)
	return "ALE_ENDPOINT_CLOSURE_V4";
    if(id == FWPS_LAYER_ALE_ENDPOINT_CLOSURE_V6)
	return "ALE_ENDPOINT_CLOSURE_V6";
    if(id == FWPS_LAYER_ALE_CONNECT_REDIRECT_V4)
	return "ALE_CONNECT_REDIRECT_V4";
    if(id == FWPS_LAYER_ALE_CONNECT_REDIRECT_V6)
	return "ALE_CONNECT_REDIRECT_V6";
    if(id == FWPS_LAYER_ALE_BIND_REDIRECT_V4)
	return "ALE_BIND_REDIRECT_V4";
    if(id == FWPS_LAYER_ALE_BIND_REDIRECT_V6)
	return "ALE_BIND_REDIRECT_V6";
    if(id == FWPS_LAYER_STREAM_PACKET_V4)
	return "STREAM_PACKET_V4";
    if(id == FWPS_LAYER_STREAM_PACKET_V6)
	return "STREAM_PACKET_V6";
    if(id == FWPS_LAYER_IPSEC_KM_DEMUX_V4)
	return "IPSEC_KM_DEMUX_V4";
    if(id == FWPS_LAYER_IPSEC_KM_DEMUX_V6)
	return "IPSEC_KM_DEMUX_V6";
    if(id == FWPS_LAYER_IPSEC_V4)
	return "IPSEC_V4";
    if(id == FWPS_LAYER_IPSEC_V6)
	return "IPSEC_V6";
    if(id == FWPS_LAYER_IKEEXT_V4)
	return "IKEEXT_V4";
    if(id == FWPS_LAYER_IKEEXT_V6)
	return "IKEEXT_V6";
    if(id == FWPS_LAYER_RPC_UM)
	return "RPC_UM";
    if(id == FWPS_LAYER_RPC_EPMAP)
	return "RPC_EPMAP";
    if(id == FWPS_LAYER_RPC_EP_ADD)
	return "RPC_EP_ADD";
    if(id == FWPS_LAYER_RPC_PROXY_CONN)
	return "RPC_PROXY_CONN";
    if(id == FWPS_LAYER_RPC_PROXY_IF)
	return "RPC_PROXY_IF";
    if(id == FWPS_LAYER_KM_AUTHORIZATION)
	return "KM_AUTHORIZATION";
    return "UNKNOWN";
}

__forceinline static char* errIdToText(int id)
{
    if(id == FWP_E_CALLOUT_NOT_FOUND)
	return "The callout does not exist";
    if(id == FWP_E_CONDITION_NOT_FOUND)
	return "The filter condition does not exist";
    if(id == FWP_E_FILTER_NOT_FOUND)
	return "The filter does not exist";
    if(id == FWP_E_LAYER_NOT_FOUND)
	return "The layer does not exist";
    if(id == FWP_E_PROVIDER_NOT_FOUND)
	return "The provider does not exist";
    if(id == FWP_E_PROVIDER_CONTEXT_NOT_FOUND)
	return "The provider context does not exist";
    if(id == FWP_E_SUBLAYER_NOT_FOUND)
	return "The sublayer does not exist";
    if(id == FWP_E_NOT_FOUND)
	return "The object does not exist";
    if(id == FWP_E_ALREADY_EXISTS)
	return "An object with that GUID or LUID already exists";
    if(id == FWP_E_IN_USE)
	return "The object is referenced by other objects so cannot be deleted";
    if(id == FWP_E_DYNAMIC_SESSION_IN_PROGRESS)
	return "The call is not allowed from within a dynamic session";
    if(id == FWP_E_WRONG_SESSION)
	return "The call was made from the wrong session so cannot be completed";
    if(id == FWP_E_NO_TXN_IN_PROGRESS)
	return "The call must be made from within an explicit transaction";
    if(id == FWP_E_TXN_IN_PROGRESS)
	return "The call is not allowed from within an explicit transaction";
    if(id == FWP_E_TXN_ABORTED)
	return "The explicit transaction has been forcibly cancelled";
    if(id == FWP_E_SESSION_ABORTED)
	return "The session has been cancelled";
    if(id == FWP_E_INCOMPATIBLE_TXN)
	return "The call is not allowed from within a read-only transaction";
    if(id == FWP_E_TIMEOUT)
	return "The call timed out while waiting to acquire the transaction lock";
    if(id == FWP_E_NET_EVENTS_DISABLED)
	return "Collection of network diagnostic events is disabled";
    if(id == FWP_E_INCOMPATIBLE_LAYER)
	return "The operation is not supported by the specified layer";
    if(id == FWP_E_KM_CLIENTS_ONLY)
	return "The call is allowed for kernel-mode callers only";
    if(id == FWP_E_LIFETIME_MISMATCH)
	return "The call tried to associate two objects with incompatible lifetimes";
    if(id == FWP_E_BUILTIN_OBJECT)
	return "The object is built in so cannot be deleted";
    if(id == FWP_E_TOO_MANY_CALLOUTS)
	return "The maximum number of callouts has been reached";
    if(id == FWP_E_NOTIFICATION_DROPPED)
	return "A notification could not be delivered because a message queue is at its maximum capacity";
    if(id == FWP_E_TRAFFIC_MISMATCH)
	return "The traffic parameters do not match those for the security association context";
    if(id == FWP_E_INCOMPATIBLE_SA_STATE)
	return "The call is not allowed for the current security association state";
    if(id == FWP_E_NULL_POINTER)
	return "A required pointer is null";
    if(id == FWP_E_INVALID_ENUMERATOR)
	return "An enumerator is not valid";
    if(id == FWP_E_INVALID_FLAGS)
	return "The flags field contains an invalid value";
    if(id == FWP_E_INVALID_NET_MASK)
	return "A network mask is not valid";
    if(id == FWP_E_INVALID_RANGE)
	return "The FWP_RANGE is not valid.";
    if(id == FWP_E_INVALID_INTERVAL)
	return "The time interval is not valid";
    if(id == FWP_E_ZERO_LENGTH_ARRAY)
	return "An array that must contain at least one element is zero length";
    if(id == FWP_E_NULL_DISPLAY_NAME)
	return "The displayData.name field cannot be null";
    if(id == FWP_E_INVALID_ACTION_TYPE)
	return "The action type is not one of the allowed action types for a filter";
    if(id == FWP_E_INVALID_WEIGHT)
	return "The filter weight is not valid";
    if(id == FWP_E_MATCH_TYPE_MISMATCH)
	return "A filter condition contains a match type that is not compatible with the operands";
    if(id == FWP_E_TYPE_MISMATCH)
	return "The FWP_VALUE or FWPM_CONDITION_VALUE is of the wrong type";
    if(id == FWP_E_OUT_OF_BOUNDS)
	return "An integer value is outside the allowed range";
    if(id == FWP_E_RESERVED)
	return "A reserved field is non-zero";
    if(id == FWP_E_DUPLICATE_CONDITION)
	return "A filter cannot contain multiple conditions operating on a single field";
    if(id == FWP_E_DUPLICATE_KEYMOD)
	return "A policy cannot contain the same keying module more than once";
    if(id == FWP_E_ACTION_INCOMPATIBLE_WITH_LAYER)
	return "The action type is not compatible with the layer";
    if(id == FWP_E_ACTION_INCOMPATIBLE_WITH_SUBLAYER)
	return "The action type is not compatible with the sublayer";
    if(id == FWP_E_CONTEXT_INCOMPATIBLE_WITH_LAYER)
	return "The raw context or the provider context is not compatible with the layer";
    if(id == FWP_E_CONTEXT_INCOMPATIBLE_WITH_CALLOUT)
	return "The raw context or the provider context is not compatible with the callout";
    if(id == FWP_E_INCOMPATIBLE_AUTH_METHOD)
	return "The authentication method is not compatible with the policy type";
    if(id == FWP_E_INCOMPATIBLE_DH_GROUP)
	return "An IKE policy cannot contain an Extended Mode policy";
    if(id == FWP_E_EM_NOT_SUPPORTED)
	return "An IKE policy cannot contain an Extended Mode policy";
    if(id == FWP_E_NEVER_MATCH)
	return "The enumeration template or subscription will never match any objects";
    if(id == FWP_E_PROVIDER_CONTEXT_MISMATCH)
	return "The provider context is of the wrong type";
    if(id == FWP_E_INVALID_PARAMETER)
	return "The parameter is incorrect";
    if(id == FWP_E_TOO_MANY_SUBLAYERS)
	return "The maximum number of sublayers has been reached";
    if(id == FWP_E_CALLOUT_NOTIFICATION_FAILED)
	return "The notification function for a callout returned an error";
    if(id == FWP_E_INVALID_AUTH_TRANSFORM)
	return "The IPsec authentication transform is not valid";
    if(id == FWP_E_INVALID_CIPHER_TRANSFORM)
	return "The IPsec cipher transform is not valid";
    if(id == FWP_E_DROP_NOICMP)
	return "The packet should be dropped, no ICMP should be sent";
    if(id == FWP_E_INCOMPATIBLE_CIPHER_TRANSFORM)
	return "The IPsec cipher transform is not compatible with the policy";
    if(id == FWP_E_INVALID_TRANSFORM_COMBINATION)
	return "The combination of IPsec transform types is not valid";
    if(id == FWP_E_DUPLICATE_AUTH_METHOD)
	return "A policy cannot contain the same auth method more than once";
    return "UNKNOWN";
}

#if 0
// callout
typedef struct PROXY_CALLOUT_ENTRY_USER_s_
{
    WCHAR* colloutName;
    WCHAR* colloutDescr;
    GUID* const layerKey;
    GUID* const calloutKey;
    UINT16 LayerId;
} PROXY_CALLOUT_ENTRY_USER_s, *PROXY_CALLOUT_ENTRY_USER_p_s;

// filter
typedef struct FILTER_PROP_s_
{
    WCHAR* pwsName;
    WCHAR* pwsDescr;
    GUID* const layerKey;
    GUID* const calloutKey;
    UINT64 id;
} FILTER_PROP_s, *FILTER_PROP_p_s;
#endif

// callouts table
static PROXY_CALLOUT_ENTRY_USER_s g_sRegisterTable[] = 
{
    {L"Zecurion ALE connect redirect Callout V4", L"Zecurion ALE connect redirect", &XGUIDS::FWPM_LAYER_ALE_CONNECT_REDIRECT_V4,
	&WFP_PROXY_ALE_CONNCECT_REDIRECT_CALLOUT_V4, FWPS_LAYER_ALE_CONNECT_REDIRECT_V4},
    {L"Zecurion Stream Callout V4", L"Zecurion Stream", &XGUIDS::FWPM_LAYER_STREAM_V4, &WFP_PROXY_STREAM_CALLOUT_V4,
	FWPS_LAYER_STREAM_V4},
    {L"Zecurion Endpoint closure Callout V4", L"Zecurion Intercepts endpoint closure layer", 
 	&XGUIDS::FWPM_LAYER_ALE_ENDPOINT_CLOSURE_V4, &WFP_PROXY_ENDPOINT_CLOSURE_CALLOUT_V4, FWPS_LAYER_ALE_ENDPOINT_CLOSURE_V4},
    {L"Zecurion Flow established Callout V4", L"Zecurion flow established layer", &XGUIDS::FWPM_LAYER_ALE_FLOW_ESTABLISHED_V4, 
	&WFP_PROXY_FLOW_ESTABLISHED_V4, FWPS_LAYER_ALE_FLOW_ESTABLISHED_V4},
    {L"Zecurion Datagram-Data for DNS Callout V4", L"Zecurion Datagram-Data layer for DNS", &XGUIDS::FWPM_LAYER_DATAGRAM_DATA_V4,
	&WFP_PROXY_DATAGRAM_DATA_V4, FWPS_LAYER_DATAGRAM_DATA_V4},
    {L"Zecurion Auth Connect V4", L"Zecurion Auth Connect V4 Layer", &XGUIDS::FWPM_LAYER_ALE_AUTH_CONNECT_V4, 
	&WFP_PROXY_AUTH_CONNECT_V4, FWPS_LAYER_ALE_AUTH_CONNECT_V4},
};

#if 0
// boot filtes table
static FILTER_PROP_s g_sBootFilterTable[] =
{
    {L"Zecurion ALE_CONNECT_REDIRECT_V4 boottime filter", L"Zecurion ALE_CONNECT_REDIRECT_V4 boottime filter", 
	&XGUIDS::FWPM_LAYER_ALE_CONNECT_REDIRECT_V4, &WFP_PROXY_ALE_CONNCECT_REDIRECT_CALLOUT_V4},
    {L"Zecurion STREAM_V4 boottime filter", L"Zecurion STREAM_V4 boottime filter", 
	&XGUIDS::FWPM_LAYER_STREAM_V4, &WFP_PROXY_STREAM_CALLOUT_V4},
    {L"Zecurion ALE_ENDPOINT_CLOSURE_V4 boottime filter", L"Zecurion ALE_ENDPOINT_CLOSURE_V4 boottime filter", 
 	&XGUIDS::FWPM_LAYER_ALE_ENDPOINT_CLOSURE_V4, &WFP_PROXY_ENDPOINT_CLOSURE_CALLOUT_V4},
    {L"Zecurion ALE_FLOW_ESTABLISHED_V4 boottime filter", L"Zecurion ALE_FLOW_ESTABLISHED_V4 boottime filter", 
	&XGUIDS::FWPM_LAYER_ALE_FLOW_ESTABLISHED_V4, &WFP_PROXY_FLOW_ESTABLISHED_V4},
    {L"Zecurion DATAGRAM_DATA_V4 boottime filter", L"Zecurion DATAGRAM_DATA_V4 boottime filter", 
	&XGUIDS::FWPM_LAYER_DATAGRAM_DATA_V4, &WFP_PROXY_DATAGRAM_DATA_V4},
    {L"Zecurion AUTH_CONNECT_V4 boottime filter", L"Zecurion AUTH_CONNECT_V4 boottime filter", 
	&XGUIDS::FWPM_LAYER_ALE_AUTH_CONNECT_V4, &WFP_PROXY_AUTH_CONNECT_V4},
};

// persistent filters table
static FILTER_PROP_s g_sPersFilterTable[] =
{
    {L"Zecurion ALE_CONNECT_REDIRECT_V4 persistent filter", L"Zecurion ALE_CONNECT_REDIRECT_V4 persistent filter",
	&XGUIDS::FWPM_LAYER_ALE_CONNECT_REDIRECT_V4, &WFP_PROXY_ALE_CONNCECT_REDIRECT_CALLOUT_V4},
    {L"Zecurion STREAM_V4 persistent filter", L"Zecurion STREAM_V4 persistent filter",
	&XGUIDS::FWPM_LAYER_STREAM_V4, &WFP_PROXY_STREAM_CALLOUT_V4},
    {L"Zecurion ALE_ENDPOINT_CLOSURE_V4 persistent filter", L"Zecurion ALE_ENDPOINT_CLOSURE_V4 persisent filter",
 	&XGUIDS::FWPM_LAYER_ALE_ENDPOINT_CLOSURE_V4, &WFP_PROXY_ENDPOINT_CLOSURE_CALLOUT_V4},
    {L"Zecurion ALE_FLOW_ESTABLISHED_V4 persistent filter", L"Zecurion ALE_FLOW_ESTABLISHED_V4 persistent filter",
	&XGUIDS::FWPM_LAYER_ALE_FLOW_ESTABLISHED_V4, &WFP_PROXY_FLOW_ESTABLISHED_V4},
    {L"Zecurion DATAGRAM_DATA_V4 persistent filter", L"Zecurion DATAGRAM_DATA_V4 persistent filter", 
	&XGUIDS::FWPM_LAYER_DATAGRAM_DATA_V4, &WFP_PROXY_DATAGRAM_DATA_V4},
    {L"Zecurion AUTH_CONNECT_V4 persistent filter", L"Zecurion AUTH_CONNECT_V4 persistent filter", 
	&XGUIDS::FWPM_LAYER_ALE_AUTH_CONNECT_V4, &WFP_PROXY_AUTH_CONNECT_V4},
};
#endif

static FILTER_PROP_s g_sEasyFilterTable[] =
{
    {L"Zecurion ALE_CONNECT_REDIRECT_V4 filter", L"Zecurion ALE_CONNECT_REDIRECT_V4 filter",
	&XGUIDS::FWPM_LAYER_ALE_CONNECT_REDIRECT_V4, &WFP_PROXY_ALE_CONNCECT_REDIRECT_CALLOUT_V4, 0},
    {L"Zecurion STREAM_V4 filter", L"Zecurion STREAM_V4 filter",
	&XGUIDS::FWPM_LAYER_STREAM_V4, &WFP_PROXY_STREAM_CALLOUT_V4, 0},
    {L"Zecurion ALE_ENDPOINT_CLOSURE_V4 filter", L"Zecurion ALE_ENDPOINT_CLOSURE_V4 filter",
 	&XGUIDS::FWPM_LAYER_ALE_ENDPOINT_CLOSURE_V4, &WFP_PROXY_ENDPOINT_CLOSURE_CALLOUT_V4, 0},
    {L"Zecurion ALE_FLOW_ESTABLISHED_V4 filter", L"Zecurion ALE_FLOW_ESTABLISHED_V4 filter",
	&XGUIDS::FWPM_LAYER_ALE_FLOW_ESTABLISHED_V4, &WFP_PROXY_FLOW_ESTABLISHED_V4, 0},
    {L"Zecurion DATAGRAM_DATA_V4 filter", L"Zecurion DATAGRAM_DATA_V4 filter", 
	&XGUIDS::FWPM_LAYER_DATAGRAM_DATA_V4, &WFP_PROXY_DATAGRAM_DATA_V4, 0},
    {L"Zecurion AUTH_CONNECT_V4 filter", L"Zecurion AUTH_CONNECT_V4 filter", 
	&XGUIDS::FWPM_LAYER_ALE_AUTH_CONNECT_V4, &WFP_PROXY_AUTH_CONNECT_V4, 0},
};

int __cdecl log(const char* in_Frm, ...)
{
    HANDLE h = NULL;
    char aszData[0x200] = {0};
    va_list arglist;
    int len;

    do
    {
	h = CreateFileA("c:\\Temp\\wfp.log", GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_ALWAYS,
	    FILE_ATTRIBUTE_NORMAL, NULL);
	if((HANDLE)-1 == h || NULL == h)
	{
	    fprintf(stderr, "%s open log error %d\n", __FUNCTION__, GetLastError());
	    break;
	}
	SetFilePointer(h, 0, NULL, FILE_END);
	va_start(arglist, in_Frm);
        len = vsprintf(aszData, in_Frm, arglist);
        WriteFile(h, aszData, strlen(aszData), (DWORD*)&len, NULL);
    } while(false);
    if((HANDLE)-1 != h && NULL != h)
	CloseHandle(h);
    return 0;
}

#if 0
class CWFPSession
{
    public:
	CWFPSession() : m_hEngine(NULL), m_bInTrans(false), m_Err(ERROR_SUCCESS) {};
	~CWFPSession() { cleanWFPSession(); }
	DWORD Register();
	DWORD Unregister();
    private:
        CWFPSession& operator=(const CWFPSession&);
    	CWFPSession& operator=(CWFPSession);
    	CWFPSession(const CWFPSession&);
    private:
        DWORD openWFPSession();
	DWORD cleanWFPSession();
	DWORD sublayerAdd();
	DWORD calloutAdd(PROXY_CALLOUT_ENTRY_USER_p_s in_Entry);
	DWORD filterAdd(FILTER_PROP_p_s in_pFprop, DWORD in_dwFlgs);
	DWORD filtersEnum();
    private:
	HANDLE m_hEngine;
	bool m_bInTrans;
	DWORD m_Err;
};
#endif

DWORD CWFPSession::sublayerAdd()
{
    FWPM_SUBLAYER0 WFP_Proxy_SubLayer;
    do
    {
	RtlZeroMemory(&WFP_Proxy_SubLayer, sizeof(FWPM_SUBLAYER0));
	memcpy(&WFP_Proxy_SubLayer.subLayerKey, &WFP_PROXY_SUBLAYER, sizeof(GUID));
	WFP_Proxy_SubLayer.displayData.name = L"Zecurion Sublayer";
	WFP_Proxy_SubLayer.displayData.description = L"Zecurion Sublayer for it's callouts";
	//WFP_Proxy_SubLayer.flags = FWPM_SUBLAYER_FLAG_PERSISTENT;
	// must be less than the weight of FWPM_SUBLAYER_UNIVERSAL to be compatible with Vista's IpSec implementation.
	WFP_Proxy_SubLayer.weight = 0;
	m_Err = FwpmSubLayerAdd0(m_hEngine, &WFP_Proxy_SubLayer, NULL);
	LOG("%s FwpmSubLayerAdd = %x\n", __FUNCTION__, m_Err);
	if(!IS_SUCCEEDED(m_Err))
	    LOG("%s !!! error %s !!!\n", __FUNCTION__, IDTOERR(m_Err));
    } while (FALSE);
    return m_Err;
}

DWORD CWFPSession::openWFPSession()
{
    do
    {
	m_Err = FwpmEngineOpen0(NULL, RPC_C_AUTHN_WINNT,  NULL, NULL /* must be NULL because session is peramnent*/, &m_hEngine);
	LOG("%s FwpmEngineOpen = %x\n", __FUNCTION__, m_Err);
	if(!IS_SUCCEEDED(m_Err) || (NULL == m_hEngine))
	{
	    LOG("%s !!! error %s !!!\n", __FUNCTION__, IDTOERR(m_Err));
	    break;
	}
	m_Err = FwpmTransactionBegin0(m_hEngine, 0);
	LOG("%s FwpmTransactionBegin = %x\n", __FUNCTION__, m_Err);
	if(!IS_SUCCEEDED(m_Err))
	{
	    LOG("%s !!! error %s !!!\n", __FUNCTION__, IDTOERR(m_Err));
	    break;
	}
	m_bInTrans = true;
    } while (FALSE);
    return m_Err;
}

DWORD CWFPSession::cleanWFPSession()
{
    do
    {
	if(m_bInTrans)
	{
	    ASSERT(NULL != m_hEngine && "Invalid handle value");
	    // early installed error instance checking
	    if(!IS_SUCCEEDED(m_Err))
	    {
		m_Err = FwpmTransactionAbort0(m_hEngine);
		LOG("%s FwpmTransactionAbort = %x\n", __FUNCTION__, m_Err);
		if(!IS_SUCCEEDED(m_Err))
		   LOG("%s !!! error %s !!!\n", __FUNCTION__, IDTOERR(m_Err));
	    }
	    else
	    {
	        m_Err = FwpmTransactionCommit0(m_hEngine);
		LOG("%s FwpmTransactionCommit = %x\n", __FUNCTION__, m_Err);
	    	if(!IS_SUCCEEDED(m_Err))
	    	    LOG("%s !!! error %s !!!\n", __FUNCTION__, IDTOERR(m_Err));
	    }
	    m_bInTrans = false;
 	}
	if(NULL != m_hEngine && INVALID_HANDLE != m_hEngine)
	{
	    m_Err = FwpmEngineClose0(m_hEngine);
	    LOG("%s FwpmEngineClose = %x\n", __FUNCTION__, m_Err);
	    if(!IS_SUCCEEDED(m_Err))
		LOG("%s !!! error %s !!!\n", __FUNCTION__, IDTOERR(m_Err));
	    m_hEngine = NULL;
	}
    } while (FALSE);
    return m_Err;
}

DWORD CWFPSession::calloutAdd(PROXY_CALLOUT_ENTRY_USER_p_s in_Entry)
{
    FWPM_CALLOUT0 mCallout = {0};

    do
    {
	mCallout.displayData.name = in_Entry->colloutName;
	mCallout.displayData.description = in_Entry->colloutDescr;
	mCallout.calloutKey = *(in_Entry->calloutKey);
	mCallout.applicableLayer = *(in_Entry->layerKey);
	//mCallout.flags = FWPM_CALLOUT_FLAG_PERSISTENT;
	m_Err = FwpmCalloutAdd0(m_hEngine, &mCallout, NULL, NULL);
	LOG("%s FwpmCalloutAdd = %x layer %d %s\n", __FUNCTION__, m_Err, in_Entry->LayerId, ID2GUIDN(in_Entry->LayerId));
	if(!IS_SUCCEEDED(m_Err))
	    LOG("%s !!! error %s !!!\n", __FUNCTION__, IDTOERR(m_Err));
    } while (FALSE);
    return m_Err;
}

DWORD CWFPSession::filterAdd(FILTER_PROP_p_s in_pFprop, DWORD in_dwFlgs)
{
    FWP_RANGE0 portRange;
    UINT conditionIndex = 0;
    FWPM_FILTER0 filter = {0};
    FWPM_FILTER_CONDITION0 filterConditions[2] = {0}; 

    do
    {
	filter.layerKey = *(in_pFprop->layerKey);
	filter.displayData.name = in_pFprop->pwsName;
	filter.displayData.description = in_pFprop->pwsDescr;
	filter.action.type = FWP_ACTION_CALLOUT_TERMINATING;
	filter.action.calloutKey = *(in_pFprop->calloutKey);
	filter.subLayerKey = WFP_PROXY_SUBLAYER;
	filter.weight.type = FWP_EMPTY; // auto-weight.
	filter.rawContext = NULL;
	filter.flags = in_dwFlgs;
#if 0
	if(!IsEqualGUID(*(in_pFprop->layerKey), FWPM_LAYER_ALE_AUTH_CONNECT_V6) && 
		!IsEqualGUID(*(in_pFprop->layerKey), FWPM_LAYER_ALE_AUTH_RECV_ACCEPT_V6) && 
		!IsEqualGUID(*(in_pFprop->layerKey), FWPM_LAYER_INBOUND_TRANSPORT_V6) &&
		!IsEqualGUID(*(in_pFprop->layerKey), FWPM_LAYER_OUTBOUND_TRANSPORT_V6) && 
		!IsEqualGUID(*(in_pFprop->layerKey), FWPM_LAYER_ALE_CONNECT_REDIRECT_V6) &&
		!IsEqualGUID(*(in_pFprop->layerKey), FWPM_LAYER_STREAM_PACKET_V6) &&
		!IsEqualGUID(*(in_pFprop->layerKey), FWPM_LAYER_ALE_ENDPOINT_CLOSURE_V6) &&
		!IsEqualGUID(*(in_pFprop->layerKey), FWPM_LAYER_ALE_FLOW_ESTABLISHED_V6) &&
		!IsEqualGUID(*(in_pFprop->layerKey), FWPM_LAYER_INBOUND_TRANSPORT_V6_DISCARD) &&
		!IsEqualGUID(*(in_pFprop->layerKey), FWPM_LAYER_STREAM_V6) &&
		!IsEqualGUID(*(in_pFprop->layerKey), FWPM_LAYER_INBOUND_TRANSPORT_V4) &&
		!IsEqualGUID(*(in_pFprop->layerKey), FWPM_LAYER_INBOUND_TRANSPORT_V4_DISCARD) &&
		!IsEqualGUID(*(in_pFprop->layerKey), FWPM_LAYER_OUTBOUND_TRANSPORT_V4) &&
		!IsEqualGUID(*(in_pFprop->layerKey), FWPM_LAYER_INBOUND_TRANSPORT_V4_DISCARD) &&
		!IsEqualGUID(*(in_pFprop->layerKey), FWPM_LAYER_INBOUND_TRANSPORT_V6_DISCARD) &&
		!IsEqualGUID(*(in_pFprop->layerKey), FWPM_LAYER_OUTBOUND_TRANSPORT_V4_DISCARD) &&
		!IsEqualGUID(*(in_pFprop->layerKey), FWPM_LAYER_OUTBOUND_TRANSPORT_V6_DISCARD) &&
		!IsEqualGUID(*(in_pFprop->layerKey), FWPM_LAYER_INBOUND_IPPACKET_V4) &&
		!IsEqualGUID(*(in_pFprop->layerKey), FWPM_LAYER_OUTBOUND_IPPACKET_V4) &&
		!IsEqualGUID(*(in_pFprop->layerKey), FWPM_LAYER_INBOUND_IPPACKET_V4_DISCARD) &&
		!IsEqualGUID(*(in_pFprop->layerKey), FWPM_LAYER_OUTBOUND_IPPACKET_V4_DISCARD) &&
		!IsEqualGUID(*(in_pFprop->layerKey), FWPM_LAYER_INBOUND_IPPACKET_V6) &&
		!IsEqualGUID(*(in_pFprop->layerKey), FWPM_LAYER_OUTBOUND_IPPACKET_V6) &&
		!IsEqualGUID(*(in_pFprop->layerKey), FWPM_LAYER_INBOUND_IPPACKET_V6_DISCARD) &&
		!IsEqualGUID(*(in_pFprop->layerKey), FWPM_LAYER_OUTBOUND_IPPACKET_V6_DISCARD))
	{
#endif
	portRange.valueLow.type = FWP_UINT16;
	portRange.valueLow.uint16 = 0;
	portRange.valueHigh.type = FWP_UINT16;
	portRange.valueHigh.uint16 = USHORT_MAX;
	filterConditions[0].fieldKey = XGUIDS::FWPM_CONDITION_IP_LOCAL_PORT;
	filterConditions[0].matchType = FWP_MATCH_RANGE;
	filterConditions[0].conditionValue.type = FWP_RANGE_TYPE;
	filterConditions[0].conditionValue.rangeValue = &portRange;
	conditionIndex++;
	filter.numFilterConditions = conditionIndex;
	filter.filterCondition = filterConditions;
	m_Err = FwpmFilterAdd0(m_hEngine, &filter, NULL, &in_pFprop->id);
	LOG("%s FwpmFilterAdd = %x %s\n", __FUNCTION__, m_Err, GUID2NAME(in_pFprop->layerKey));
	//TRACE_GUID(filter.layerKey);
    } while(FALSE);
    return m_Err;
}

DWORD CWFPSession::Register()
{
    // Common logical imagination: sublayer, callout, boot and persistent filters for callout
    int i;
    do
    {
	// BFE engine open
	if(!IS_SUCCEEDED(openWFPSession()))
	    break;
	// sublayer adding
	if(!IS_SUCCEEDED(sublayerAdd()))
	    break;
	for(i = 0; i < ARRAYSIZE(g_sRegisterTable); i++)
	{
	    // callouts adding
	    if(!IS_SUCCEEDED(calloutAdd(&g_sRegisterTable[i])))
		break;
#if 0
	    // boot filters adding
	    if(!IS_SUCCEEDED(filterAdd(&g_sBootFilterTable[i], FWPM_FILTER_FLAG_BOOTTIME)))
	    {
		LOG("%s !!! [%d] boot error %s !!!\n", __FUNCTION__, i, IDTOERR(m_Err));
		break;
     	    }
	    // persistent filters adding
	    if(!IS_SUCCEEDED(filterAdd(&g_sBootFilterTable[i], FWPM_FILTER_FLAG_PERSISTENT | 
		FWPM_FILTER_FLAG_PERMIT_IF_CALLOUT_UNREGISTERED)))
	    {
		LOG("%s !!! [%d] persistent error %s !!!\n", __FUNCTION__, i, IDTOERR(m_Err));
		break;
	    }
#endif
	    if(!IS_SUCCEEDED(filterAdd(&g_sEasyFilterTable[i], 0)))
	    {
		LOG("%s !!! [%d] error %s !!!\n", __FUNCTION__, i, IDTOERR(m_Err));
		break;
     	    }
	}
    } while (FALSE);
    // all cleaning will be done in dtor
    return m_Err;
}

DWORD CWFPSession::filtersEnum()
{
    DWORD i, dwEntries = 0, j;
    HANDLE hEnum = NULL;
    FWPM_FILTER0** ppFilters = NULL;
    FWPM_FILTER_ENUM_TEMPLATE0 ftrEnumTmpl = {0};

    do
    {
	for(i = 0; i < RTL_NUMBER_OF(g_sEasyFilterTable); i++)
	{
	    ftrEnumTmpl.layerKey = *(g_sEasyFilterTable[i].layerKey);
	    ftrEnumTmpl.calloutKey = const_cast<GUID*>(g_sEasyFilterTable[i].calloutKey);
	    ftrEnumTmpl.enumType = FWP_FILTER_ENUM_FULLY_CONTAINED;
	    ftrEnumTmpl.flags = FWP_FILTER_ENUM_FLAG_INCLUDE_BOOTTIME | FWP_FILTER_ENUM_FLAG_INCLUDE_DISABLED;
	    ftrEnumTmpl.actionMask  = 0xFFFFFFFF;
	    m_Err = FwpmFilterCreateEnumHandle0(m_hEngine, &ftrEnumTmpl, &hEnum);
	    if(!IS_SUCCEEDED(m_Err) || (NULL == hEnum))
	    {
		LOG("%s !!! FwpmFilterCreateEnumHandle [%d] error %x %s !!!\n", __FUNCTION__, i, m_Err, IDTOERR(m_Err));
	        break;
	    }
   	    m_Err = FwpmFilterEnum0(m_hEngine, hEnum, 0xFFFFFFFF, &ppFilters, (UINT32*)&dwEntries);
	    if(!IS_SUCCEEDED(m_Err))
	    {
		LOG("%s !!! FwpmFilterEnum [%d] error %x %s !!!\n", __FUNCTION__, i, m_Err, IDTOERR(m_Err));
		break;
	    }
      	    if(ppFilters && dwEntries)
      	    {
         	for(j = 0; j < dwEntries; j++)
         	{
		    m_Err = FwpmFilterDeleteByKey0(m_hEngine, &(ppFilters[j]->filterKey));
		    if(!IS_SUCCEEDED(m_Err))
			LOG("%s !!! FwpmFilterDeleteByKey [%d:%d] error %x %s !!!\n", __FUNCTION__, i, j, m_Err, IDTOERR(m_Err));
         	}
         	FwpmFreeMemory((void**)&ppFilters);
	    	FwpmFilterDestroyEnumHandle0(m_hEngine, hEnum);
		hEnum = NULL;
		ppFilters = NULL;
		dwEntries = 0;
      	    }
	}
    } while (FALSE);
    if(NULL != hEnum)
    {
	m_Err = FwpmFilterDestroyEnumHandle0(m_hEngine, hEnum);
	if(!IS_SUCCEEDED(m_Err))
	    LOG("%s !!! FwpmFilterDestroyEnumHandle error %x %s !!!\n", __FUNCTION__, m_Err, IDTOERR(m_Err));
    }
    if(NULL != ppFilters)
	FwpmFreeMemory((void**)&ppFilters);
    return m_Err;
}

DWORD CWFPSession::Unregister()
{
    int i;
    do
    {
	// BFE engine open
	if(!IS_SUCCEEDED(openWFPSession()))
	    break;
	// filters deregistering, filter IDs wasn't obtained early
	if(0 == g_sEasyFilterTable[0].id)
	{
	    if(!IS_SUCCEEDED(filtersEnum()))
	    {
		LOG("%s !!! error %x %s !!!\n", __FUNCTION__, m_Err, IDTOERR(m_Err));
		break;
	    }
	}
	else
	{
	    for(i = 0; i < RTL_NUMBER_OF(g_sEasyFilterTable); i++)
	    {
		m_Err = FwpmFilterDeleteById0(m_hEngine, g_sEasyFilterTable[i].id);
		if(!IS_SUCCEEDED(m_Err))
		    LOG("%s !!! FwpmFilterDeleteById [%d] error %x %s !!!\n", __FUNCTION__, i, m_Err, IDTOERR(m_Err));
	    }
	}
	// callouts deregistering
	for(i = 0; i < RTL_NUMBER_OF(g_sRegisterTable); i++)
	{
	    m_Err = FwpmCalloutDeleteByKey0(m_hEngine, g_sRegisterTable[i].calloutKey);
	    if(!IS_SUCCEEDED(m_Err))
		LOG("%s !!! FwpmCalloutDeleteByKey error [%d] %x %s !!!\n", __FUNCTION__, i, m_Err, IDTOERR(m_Err));
	}
	// sublayer clean
	m_Err = FwpmSubLayerDeleteByKey0(m_hEngine, &WFP_PROXY_SUBLAYER);
	if(!IS_SUCCEEDED(m_Err))
	    LOG("%s !!! FwpmSubLayerDeleteByKey error %x %s !!!\n", __FUNCTION__, m_Err, IDTOERR(m_Err));
    } while (FALSE);
    return m_Err;
}
#if 0
int main(int argc, char* argv[])
{
    LOG("%s\n", __FUNCTION__);
    CWFPSession swfp;
    if(FWP_E_ALREADY_EXISTS == swfp.Register())
	LOG("%s data presented already\n", __FUNCTION__);
    LOG("%s --\n", __FUNCTION__);
    return 0;
}
#endif