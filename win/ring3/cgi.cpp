#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <io.h>
#include <fcntl.h>

#include <vector>

using namespace std;

#if 0
HTTPS
HTTPS_KEYSIZE
HTTPS_SECRETKEYSIZE
REMOTE_USER
SERVER_URL
QUERY_STRING
HTTP_USER_AGENT
HTTP_UA_OS
HTTP_UA_COLOR
HTTP_UA_CPU
HTTP_UA_PIXELS
HTTP_PRAGMA
HTTP_ACCEPT
HTTP_CONNECTION
HTTP_REFERER
HTTP_ACCEPT_ENCODING
HTTP_ACCEPT_LANGUAGE
HTTP_IF_MODIFIED_SINCE
HTTP_FROM
HTTP_HOST
GATEWAY_INTERFACE
SERVER_NAME
SERVER_PORT
SERVER_PROTOCOL
SERVER_SOFTWARE
REMOTE_IDENT
HTTP_AUTHORIZATION
AUTH_TYPE
CONTENT_FILE
CONTENT_LENGTH
CONTENT_TYPE
PATH_INFO
PATH_TRANSLATED
REMOTE_ADDR
REMOTE_HOST
REQUEST_METHOD
REQUEST_LINE
SCRIPT_NAME
====SSI=========
DATE_GMT
DATE_LOCAL
DOCUMENT_NAME
DOCUMENT_URI
LAST_MODIFIED
#endif

int __cdecl Log(const char* in_Frm, ...)
{
    HANDLE h = NULL;
    char aszData[0x200] = {0};
    va_list arglist;
    int len;

    do
    {
	h = CreateFileA("c:\\temp\\cgi.log",  GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_ALWAYS,
	    FILE_ATTRIBUTE_NORMAL, NULL);
	if((HANDLE)-1 == h || NULL == h)
	{
	    fprintf(stderr, "%s open log error %d\n", __FUNCTION__, GetLastError());
	    break;
	}
	SetFilePointer(h, 0, NULL, FILE_END);
	va_start(arglist, in_Frm);
        len = vsprintf(aszData, in_Frm, arglist);
        WriteFile(h, aszData, strlen(aszData), (DWORD*)&len, NULL);
    } while(false);
    if((HANDLE)-1 != h && NULL != h)
	CloseHandle(h);
    return 0;
}

#define	LOG	Log

__forceinline int save(const char* name, const char* data, size_t sz)
{
    HANDLE h = NULL;
    int wr;
    h = CreateFileA(name, GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_ALWAYS,
	FILE_ATTRIBUTE_NORMAL, NULL);
    if((HANDLE)-1 == h || NULL == h)
    {
	LOG("%s open %s error %d\n", __FUNCTION__, name, GetLastError());
	return -1;
    }
    if(!WriteFile(h, data, sz, (DWORD*)&wr, NULL))
    {
	LOG("%s write error %d\n", __FUNCTION__, GetLastError());
	CloseHandle(h);
	return -2;
    }
    CloseHandle(h);
    return 0;
}

__forceinline VOID DumpLaddrMemory(DWORD in_id, VOID* in_buf, DWORD in_Len, int Where)
{
    UCHAR* wbuf = (UCHAR*)malloc(2048);
    DWORD  offset = 0;	// offset in output buffer
    UCHAR* cbuf;		// current buffer
    DWORD  boff = 0;	// offset in input buffer
    int	   i;			// Loop counter

    LOG("%s where %d\n", __FUNCTION__, Where);
    if (wbuf == NULL)
    {
	LOG("%s Failed to allocate buffer\n", __FUNCTION__);
	return;
    }
    if (!in_buf)
    {
	LOG("%s: Input buffer is NULL\n", __FUNCTION__);
	return;
    }
    if (in_Len > 0x10000)
    {
	LOG("%s: Len is too big and it is = %08x\n", __FUNCTION__, in_Len);
	return;
    } 
    else
    {
	LOG("%s: Len is %08x, Pointer = %p\n", __FUNCTION__, in_Len, in_buf);
	while(in_Len)
	{
	    *wbuf = 0;
	    offset = 0;
	    cbuf = wbuf + offset;
	    offset += sprintf((char*) cbuf, "[%08x] %04x: ", in_id, boff);
	    for (i = 0; i < 16; i++)
	    {
		if (i < in_Len)
		{
		    UCHAR b = *(((UCHAR*) in_buf) + boff + i);	
		    cbuf = wbuf + offset;
		    offset += sprintf((char*) cbuf, "%02x ", (DWORD) b);
		} 
		else
		{
		    cbuf = wbuf + offset;
		    offset += sprintf((char*) cbuf, "   ");
		}
	    }
	    cbuf = wbuf + offset;
	    offset += sprintf((char*)cbuf, " [");
	    for (i = 0; i < 16; i++)
	    {
		if (i < in_Len)
		{
		    UCHAR b = *(((UCHAR*) in_buf) + boff + i);
		    if (b < ' ')   b = '.';
		    if (b == '%')  b = '#';		// for protect printf from unexpected
		    if (b >= 0x7f)  b = '.';
		    cbuf = wbuf + offset;
		    *cbuf = b;
		    cbuf[1] = '\0';
		    offset++;
			
		}
		else
		{
		    cbuf = wbuf + offset;
		    *cbuf = ' ';
		    cbuf[1] = '\0';
		    offset++;
		}
	    }
	    cbuf = wbuf + offset;
	    offset += sprintf((char*)cbuf, "]\n");
	    LOG((PCH) wbuf);
	    in_Len = (in_Len >= 16)? in_Len - 16 : 0;
	    boff   += 16;
	}
    }
    free(wbuf);
}

class CSaveInto
{
#define	CTYPE	"multipart/form-data; boundary="
#define	FNTOKEN	"filename="
#define EOB	0xa
#define SPATH	"c:\\Temp\\"

typedef	struct _node_
{
    char* psz;
    int len;
    struct _node_* next;
} node, *pnode;

    private:
	char* m_pszData;
	char* m_pszBound;
	unsigned m_Size;
	char* m_pBuf;
	char* m_pszName;
	bool  m_bRel;
	pnode m_pTree;
    public:
	void dumpTree() const
	{
	    int i = 0;
	    if(m_pTree)
	    {
		for(pnode cur = m_pTree; cur; cur = (pnode)cur->next, ++i)
		{
		    LOG("%s [%d] len %d\n", __FUNCTION__, i, cur->len);
		    DumpLaddrMemory(i, cur->psz, 0x10, i);
		}
	    }
	}
	CSaveInto(char* pszData, char* pszBound, unsigned len) : m_pszData(pszData), m_pszBound(pszBound), m_Size(len), 
	    m_pBuf(NULL), m_bRel(false), m_pTree(NULL), m_pszName(NULL) {}
	~CSaveInto()
	{
	    LOG("%s ++\n", __FUNCTION__);
	    if(NULL != m_pBuf)
		free(m_pBuf);
	    if(m_bRel && m_pszBound)
		free(m_pszBound);
	    release();
	    if(m_pszName)
		free(m_pszName);
	    LOG("%s --\n", __FUNCTION__);
	}
	int RqHandler()
	{
	    int r = 0;
	    do
	    {
		r = getBoundary();
		if(0 != r)
		    break;
		r = getFileName();
		if(0 != r)
		    break;
		r = buildTree();
		if(0 != r)
		   break;
		dumpTree();
	        r = saveIn();
	    } while (false);
	    return r;
	}
    private:
	CSaveInto();
	CSaveInto(CSaveInto&);
	CSaveInto& operator=(CSaveInto&);
	CSaveInto& operator=(CSaveInto);
    private:
	int saveIn() const
	{
	    int r = 0, wr, t;
	    pnode cur;
	    HANDLE h = NULL;
	    do
	    {
		if(NULL == m_pTree || NULL == m_pszName)
		{
		    r = -1;
		    break;
		}
		h = CreateFileA(m_pszName,  GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_ALWAYS,
	    	    FILE_ATTRIBUTE_NORMAL, NULL);
		if((HANDLE)-1 == h || NULL == h)
		{
	    	    LOG("%s open %s error %d\n", __FUNCTION__, m_pszName, GetLastError());
		    r = -2;
	    	    break;
		}
		for(t = 0, cur = m_pTree; cur; cur = (pnode)cur->next, ++t)
		{
		    LOG("%s len [%d] %d \n", __FUNCTION__, t, cur->len);
		    if(!WriteFile(h, cur->psz, cur->len, (DWORD*)&wr, NULL))
		    {
			LOG("%s !!! WriteFile error %d !!!\n", __FUNCTION__, GetLastError());
			r = -3;
			break;
		    }
		}
	    } while (false);
	    if(NULL != h)
	 	CloseHandle(h);
	    if(0 > r)
		DeleteFileA(m_pszName);
	    return r;
	}
	int getFileName()
	{
	    int r = 0, rr = 0, len;
	    char* psz = NULL;
	    do
	    {
		if(NULL == m_pszData)
		{
		    r = -1;
		    break;
		}
		len = strlen(m_pszData);
		psz = strstr(m_pszData, FNTOKEN);
		if(NULL == psz)
		{
		    LOG("%s !!! No file name !!!\n", __FUNCTION__);
		    r = -2;
		    break;
		}
		// it's a fucking stuff, but '"' symbol don't take in attention properly
		psz += sizeof(FNTOKEN);
		while(psz[rr] != '"' && rr < len)
		    rr++;
		//LOG("%s %s %d\n", __FUNCTION__, psz, rr);
	   	if(0 == rr || rr >= len)
		{
		    LOG("%s !!! Incorrect file name !!!\n", __FUNCTION__);
		    r = -3;
		    break;
		}
		len = sizeof(SPATH) + rr + 1;
 		m_pszName = (char*)malloc(len);
		if(NULL == m_pszName)
		{
		    LOG("%s !!! Memory allocation error %d !!!\n", __FUNCTION__, GetLastError());
		    r = -4;
		    break;
		}
		memset(m_pszName, 0, len);
		strcat(m_pszName, SPATH);
		memcpy(m_pszName + strlen(SPATH), psz, rr);
		LOG("%s %s\n", __FUNCTION__, m_pszName);
	    } while(false);
	    return r;
	}
	void release()
	{
	    pnode cur = m_pTree;
	    do
	    {
		if(cur)
		{
		    pnode next = (pnode)cur->next;
		    free(cur);
		    cur = next;
		    continue;    
		}
		break;
	    } while (1);
	}
	// main CONTENT_TYPE multipart/form-data; boundary=---------------------------NNNNNNNNNNNNNNN
	int getBoundary()
	{
	    int r = 0, l;
	    char* psz = NULL, *tmp = NULL;
	    do
	    {
		if(NULL == m_pszBound)
		{
		    LOG("%s !!! No boundary !!!\n", __FUNCTION__);
		    r = -1;
		    break;
		}
		//LOG("%s size CTYPE %d len bound %d\n", __FUNCTION__, sizeof(CTYPE), getBufLen(m_pszBound));
		if(strlen(m_pszBound) <= sizeof(CTYPE))
		{
		    LOG("%s !!! Illegal boundary size !!!\n", __FUNCTION__);
		    r = -2;
		    break;
		}
		tmp = strstr(m_pszBound, CTYPE);
		//DumpLaddrMemory(100, m_pszBound, 0x100, 100);
		if(NULL == tmp)
		{
		    LOG("%s !!! Incorrect boundary !!!\n", __FUNCTION__);
		    r = -2;
		    break;
		}
		//LOG("%s psz %llx\n", __FUNCTION__, (__int64)psz);
		tmp += sizeof(CTYPE);
		l = strlen(tmp) + 1;
		psz = (char*)malloc(l);
		if(NULL == psz)
		{
		    LOG("%s !!! Memory allocation error !!!\n", __FUNCTION__);
		    r = -3;
		    break;
		}
		memset(psz, 0, l);
		memcpy(psz, tmp, l - 1);
		m_pszBound = psz;
		m_bRel = true;
		LOG("%s %s\n", __FUNCTION__, m_pszBound);
	    } while (false);
	    return r;
	}
	void insTree(pnode Node)
	{
	    if(NULL == m_pTree)
		m_pTree = Node;
	    else
	    {
		pnode cur = m_pTree;
		while(cur->next)
		    cur = cur->next;
		cur->next = Node;
	    }
	}
	int buildTree()
	{
	    int r = 0, cnt = 0;
	    pnode curnode = NULL;
	    char* cur = m_pszData, *prev = m_pszData;
	    int bndlen = strlen(m_pszBound), size = m_Size;
	    do
	    {
		char* p, *pp;
		cur = memStr(cur, m_pszBound, size);
		if(NULL != cur)
		{
		    // fix end of line range
		    cur += bndlen + 1;
		    // end of tree, we'll drop last data
		    if(size - (bndlen + 1) <= bndlen)
			break;
		    if(size - (int)(cur - prev) <= bndlen)
			break;
		    // on 1-st pass we'll omit some more data until EOB or \r\n\r\n
		    if(0 == cnt)
		    {
			p = strstr(cur, "\n\n");
			if(NULL == p)
			{
			    p = strstr(cur, "\r\n\r\n");
			    if(NULL == p)
			    {
			        LOG("%s !!! Incorrect header data !!!\n", __FUNCTION__);
				r = -1;
			        break;
			    }
			    cur = p + 4;
			}
		        else
			    cur += sizeof(short);
		    }
		    curnode = (pnode)malloc(sizeof(node));
		    if(NULL == curnode)
		    {
			LOG("%s !!! memory allocation error %d !!!\n", __FUNCTION__, GetLastError());
			r = -2;
			break;
		    }
		    memset(curnode, 0, sizeof(node));
		    curnode->psz = cur;
		    size -= (cur - prev);
		    LOG("%s size %d\n", __FUNCTION__, size);
		    // length correction
		    p = memStr(cur, m_pszBound, size);
		    if(NULL == p)
		    {
			LOG("%s !!! Incorrect partial data !!!\n", __FUNCTION__);
			r = -3;
			break;
		    }
		    // end boundary on some symbols longer
		    //DumpLaddrMemory(0xff, p - 0x10, 0x20, 0xff);
		    while(EOB != *(--p));
		    //if(*(p) == '\n' && *(p - 1) == '\r' && *(p - 2) == '\n')
		    //{
			//LOG("%s !!! YUH !!!\n", __FUNCTION__);
		    p -= 1;
		    //}
		    curnode->len = (int)(p - cur);
		    LOG("%s node len %d\n", __FUNCTION__, curnode->len);
		    insTree(curnode);
		    prev = cur;
		}
	    } while (true);
	    LOG("%s --\n", __FUNCTION__);	
	    return r;
	}
	char* memStr(char* pszDst, char* pszSrc, int curLen) const
	{
	    int len = curLen;
	    char* cur = pszDst, *psz = NULL, *ptr = NULL;
	    LOG("%s++\n", __FUNCTION__);
	    for(;;)
	    {
		ptr = (char*)memchr(cur, pszSrc[0], len);
		if(NULL == ptr)
		    break;
		psz = strstr(ptr, pszSrc);
		if(NULL == psz)
	        {
		    len -= (int)(ptr - cur) + 1;
		    if(0 >= len)
			return NULL;
		    cur += ((int)(ptr - cur) + 1);
		    continue;
		}
		break;
	    }
	    LOG("%s %p -- \n", __FUNCTION__, ptr);
	    return psz;
	}
};

int main(int argc, char* argv[])
{
    char c;
    char* r_method;
    char* c_length;
    char* r_line;
    char* c_file;
    char* d_uri;
    char* q_str = NULL;
    char* c_type;
    unsigned len = 0, rd = 0, cur_l = 0;
    char* data = NULL;
    vector<char> chvec;
    LOG("%s ++\n", __FUNCTION__);
    printf("Content-type: text/html\n");
    printf("Pragma: no-cache\n");
    printf("\n");
    printf("<HTML><HEAD><TITLE>oleshii</TITLE></HEAD>\n");
    //printf("Proga: loader.<br>\n");
    printf("<body bgcolor='black' text='#F2F2F2' link='#FF9900' vlink='#FF9900' alink='#FF9900'>\n");
    printf("<meta http-equiv='content-type' content='text/html; charset=windows-1251'>\n");
    printf("<p style='line-height:100%; margin-left:10%;' align='middle'><span style='font-size:14pt;'>!!! YUH !!!</span></p>\n");
    //printf("<p style='line-height:100%; margin-left:10%;'><a href=http://www.pagel.by.ru>www.pagel.by.ru</a> Code by Lazy_elf</p>\n");

    r_method = getenv("REQUEST_METHOD");
    c_length = getenv("CONTENT_LENGTH");
    r_line = getenv("REQUEST_LINE");
    c_file = getenv("CONTENT_FILE");
    d_uri = getenv("DOCUMENT_URI");
    q_str = getenv("QUERY_STRING");
    // a local variable can't be stored into a environment variable
    c_type = getenv("CONTENT_TYPE");

    if(r_line)
	LOG("%s REQUEST_LINE %s\n", __FUNCTION__, r_line);
    if(r_method)
	LOG("%s REQUEST_METHOD %s\n", __FUNCTION__, r_method);
    if(c_file)
	LOG("%s CONTENT_FILE %s\n", __FUNCTION__, c_file);
    if(d_uri)
	LOG("%s DOCUMENT_URI %s\n", __FUNCTION__, d_uri);
    if(q_str)
    {
	LOG("%s QUERY_STRING %s\n", __FUNCTION__, q_str);
	//printf("<I><B>QUERY_STRING</B></I>=%d<br>\n",getenv("QUERY_STRING"));
    }
    if(q_str && 0 != q_str[0] && 0 < len)
    {
	DumpLaddrMemory(0, q_str, len > 0x2000 ? 0x2000 : len, 0);
    }
    if(NULL == c_type)
    {
	LOG("%s !!! No CONTENT_TYPE !!!\n", __FUNCTION__);
	return -1;
    }
    if(NULL == c_length)
    {
	LOG("%s !!! No CONTENT_LENGTH !!!\n", __FUNCTION__);
	return -2;
    }
    _setmode(_fileno(stdin), _O_BINARY);
    while(!feof(stdin))
    {
	c = fgetc(stdin);
	chvec.push_back(c);
    }
    LOG("%s stream length %d\n", __FUNCTION__, chvec.size());
    if(0 >= chvec.size())
    {
	LOG("%s read file error %d\n", __FUNCTION__, GetLastError());
	return -3;
    }
    data = &*chvec.begin();
    len = chvec.size();
    save("c:\\temp\\bindmp.bin", data, len);
    LOG("%s CONTENT_LENGTH %d\n", __FUNCTION__, len);
    LOG("%s CONTENT_TYPE %s\n", __FUNCTION__, getenv("CONTENT_TYPE"));
    CSaveInto into(data, getenv("CONTENT_TYPE"), len);
    into.RqHandler();
    printf("</BODY></HTML>\n");
    LOG("%s --\n", __FUNCTION__);
    return 0;
}