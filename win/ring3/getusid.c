#include <ntddk.h>
#include <stdio.h>
#include "ntdll.h"

//KeyNameInformation

//typedef struct _KEY_NAME_INFORMATION {
//	ULONG NameLength;
//	WCHAR Name[1]; // Variable length string
//} KEY_NAME_INFORMATION, *PKEY_NAME_INFORMATION;

//KeyValueFullInformation

//REG_NONE
//REG_SZ
//REG_EXPAND_SZ
//REG_BINARY
//REG_DWORD
//REG_DWORD_LITTLE_ENDIAN
//REG_DWORD_BIG_ENDIAN
//REG_LINK
//REG_MULTI_SZ
//REG_RESOURCE_LIST
//REG_FULL_RESOURCE_DESCRIPTOR
//REG_RESOURCE_REQUIREMENTS_LIST

//typedef struct _KEY_VALUE_BASIC_INFORMATION {
//ULONG TitleIndex;
//ULONG Type;
//ULONG NameLength;
//WCHAR Name[1]; // Variable length string
//} KEY_VALUE_BASIC_INFORMATION, *PKEY_VALUE_BASIC_INFORMATION;

// HKU HKEY_CURRENT_USER L"\\REGISTRY\\USER\\" HKEY_USERS = \Registry\User
// HKEY_USERS L"\\REGISTRY\\USERS\\"

//#define WINADVAPI DECLSPEC_IMPORT

//#define HKEY_USERS  ((HANDLE) (ULONG_PTR)((LONG)0x80000003))

//WINADVAPI LSTATUS APIENTRY RegOpenKeyW(__in HANDLE hKey, _in_opt WCHAR* lpSubKey, __out HANDLE* phkResult);

typedef struct _OBJECT_DIRECTORY_INFORMATION {
    UNICODE_STRING Name;
    UNICODE_STRING TypeName;
} OBJECT_DIRECTORY_INFORMATION, *POBJECT_DIRECTORY_INFORMATION;

#define	LOG	printf

int main(int argc, char* argv[])
{
	ULONG ctx, len;
	HANDLE hKey = NULL;
	ANSI_STRING as = {0};
	char szCvt[0x50] = {0};
	char szName[0x50] = {0};
	UNICODE_STRING us = {0};
	OBJECT_ATTRIBUTES oa = {0};
	char szTypeName[0x50] = {0};
	WCHAR rpath[] = L"\\??\\Sessions";	// \\Registry\\HKEY_USERS
	NTSTATUS status = STATUS_SUCCESS;
	OBJECT_DIRECTORY_INFORMATION oi = {0};

	do
	{
		RtlInitUnicodeString(&us, rpath);
    		InitializeObjectAttributes(&oa, &us, OBJ_CASE_INSENSITIVE, (HANDLE)NULL, NULL);
    		//status = NtOpenKey(&hKey, KEY_QUERY_VALUE | KEY_ENUMERATE_SUB_KEYS, &oa); 
		// FILE_READ_DATA | FILE_READ_ATTRIBUTES | READ_CONTROL | SYNCHRONIZE
		status = NtOpenDirectoryObject(&hKey, DIRECTORY_QUERY | DIRECTORY_TRAVERSE, &oa);
		if(!NT_SUCCESS(status))
		{
			LOG("%s !!! NtOpenDirectoryObject error %x !!!\n", __FUNCTION__, status);
			break;
		}
		while(TRUE)
		{
			memset(szTypeName, 0, sizeof(szTypeName));
			memset(szName, 0, sizeof(szName));
			oi.Name.Buffer = (PWCHAR)szName;
			oi.Name.Length = 0;
			oi.Name.MaximumLength = (sizeof(szName) / sizeof(WCHAR)) - 1;
			oi.TypeName.Buffer = (PWCHAR)szTypeName;
			oi.TypeName.Length = 0;
			oi.TypeName.MaximumLength = (sizeof(szTypeName) / sizeof(WCHAR)) - 1;
			status = NtQueryDirectoryObject(hKey, &oi, sizeof(oi) + sizeof(szTypeName) + sizeof(szName), TRUE, 
				FALSE, &ctx, &len);
			if(!NT_SUCCESS(status))
			{
				if(status != STATUS_NO_MORE_ENTRIES)
					LOG("%s !!! NtQueryDirectoryObject error %x len %x ctx %x !!!\n", __FUNCTION__, status, 
					len, ctx);
				break;
			}
			memset(szCvt, 0, sizeof(szCvt));
			as.Buffer = szCvt;
			as.Length = 0;
			as.MaximumLength = sizeof(szCvt) - 1;
			status = RtlUnicodeStringToAnsiString(&as, &oi.Name, FALSE);
			if(!NT_SUCCESS(status))
			{
				LOG("%s RtlUnicodeStringToAnsiString error %x\n", __FUNCTION__, status);
				continue;
			}
			LOG("%s %s\n", __FUNCTION__, szCvt);
		}
	} while (FALSE);
	if(NULL != hKey)
		NtClose(hKey);
	return 0;
}