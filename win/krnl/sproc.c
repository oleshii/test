#include <ntddk.h>

#if 1
#define LOG DbgPrint
#else
#define LOG
#endif

#define NTDEVICE_NAME_STRING      L"\\Device\\Sproc"
#define SYMBOLIC_NAME_STRING      L"\\DosDevices\\Sproc"

#if !defined(FALSE)
#define	FALSE	0
#define	TRUE	1
#endif

#define	DRV_POOL_TAG		'CRPS'		// SPRC
#define	DWORD 			unsigned int
#define SID_STR_LEN		0x80
#define MAX_CMP_PRC_NAME_LEN	0x10	// EPROCESS->UCHAR ImageFileName[16];
#define	MAX_SESSIONS_NUMBER	0xff
// it's sufficient for the MAX_SESSIONS_NUMBER bits storage
#define	BITMAP_SIZE		(sizeof(unsigned long long) << 2)
#define	KERN_DIR_TEMPL_NAME	L"\\Sessions\\"
#define	GETCURPROCNAME(o)	(o > 0 ? (char*)(PsGetCurrentProcess()) + o : "NULL")
#define CURPROCNAME()		GETCURPROCNAME(g_ProcessNameOffset)
#define PROCNAME(p)		(g_ProcessNameOffset > 0 && p ? (char*)p + g_ProcessNameOffset : "NULL")

typedef struct _DEVICE_EXTENSION {
    PDEVICE_OBJECT  Self;
    LIST_ENTRY      EventQueueHead;
    KSPIN_LOCK      QueueLock;
} DEVICE_EXTENSION, *PDEVICE_EXTENSION;

typedef struct _SPROCSENTRY_s_
{
    LIST_ENTRY  stor;
    ULONG	ussId;
    long long	authLuid;
    WCHAR*	wssId;
} SPROCSENTRY_s, *SPROCSENTRY_p_s;

PDEVICE_OBJECT g_DeviceObject = NULL;
ULONG g_ProcessNameOffset = 0;
LIST_ENTRY g_SessionList;
KSPIN_LOCK g_SessionLock;

DRIVER_INITIALIZE DriverEntry;
DRIVER_UNLOAD SprocUnload;

#pragma alloc_text (INIT, DriverEntry)
#pragma alloc_text (PAGE, SprocUnload)

typedef enum _TOKEN_TYPE {
    TokenPrimary = 1,
    TokenImpersonation
    } TOKEN_TYPE;
typedef TOKEN_TYPE *PTOKEN_TYPE;

#define ANYSIZE_ARRAY 1
#define SID_HASH_SIZE 32
typedef ULONG_PTR SID_HASH_ENTRY, *PSID_HASH_ENTRY;

typedef struct _OBJECT_DIRECTORY_INFORMATION {
    UNICODE_STRING Name;
    UNICODE_STRING TypeName;
} OBJECT_DIRECTORY_INFORMATION, *POBJECT_DIRECTORY_INFORMATION;

typedef struct _SID_AND_ATTRIBUTES {
    PSID Sid;
    DWORD Attributes;
} SID_AND_ATTRIBUTES, *PSID_AND_ATTRIBUTES;

typedef struct _SID_AND_ATTRIBUTES_HASH {
    ULONG SidCount;
    PSID_AND_ATTRIBUTES SidAttr;
    SID_HASH_ENTRY Hash[SID_HASH_SIZE];
} SID_AND_ATTRIBUTES_HASH, *PSID_AND_ATTRIBUTES_HASH;

typedef enum _TOKEN_INFORMATION_CLASS {
    TokenUser = 1,
    TokenGroups,
    TokenPrivileges,
    TokenOwner,
    TokenPrimaryGroup,
    TokenDefaultDacl,
    TokenSource,
    TokenType,
    TokenImpersonationLevel,
    TokenStatistics,
    TokenRestrictedSids,
    TokenSessionId,
    TokenGroupsAndPrivileges,
    TokenSessionReference,
    TokenSandBoxInert,
    TokenAuditPolicy,
    TokenOrigin,
    TokenElevationType,
    TokenLinkedToken,
    TokenElevation,
    TokenHasRestrictions,
    TokenAccessInformation,
    TokenVirtualizationAllowed,
    TokenVirtualizationEnabled,
    TokenIntegrityLevel,
    TokenUIAccess,
    TokenMandatoryPolicy,
    TokenLogonSid,
    MaxTokenInfoClass  // MaxTokenInfoClass should always be the last enum
} TOKEN_INFORMATION_CLASS, *PTOKEN_INFORMATION_CLASS;

// Token information class structures
typedef struct _TOKEN_USER {
    SID_AND_ATTRIBUTES User;
} TOKEN_USER, *PTOKEN_USER;

typedef struct _TOKEN_GROUPS {
    ULONG GroupCount;
    SID_AND_ATTRIBUTES Groups[ANYSIZE_ARRAY];
} TOKEN_GROUPS, *PTOKEN_GROUPS;

typedef struct _TOKEN_PRIVILEGES {
    ULONG PrivilegeCount;
    LUID_AND_ATTRIBUTES Privileges[ANYSIZE_ARRAY];
} TOKEN_PRIVILEGES, *PTOKEN_PRIVILEGES;

typedef struct _TOKEN_OWNER {
    PSID Owner;
} TOKEN_OWNER, *PTOKEN_OWNER;

typedef struct _TOKEN_PRIMARY_GROUP {
    PSID PrimaryGroup;
} TOKEN_PRIMARY_GROUP, *PTOKEN_PRIMARY_GROUP;

typedef struct _TOKEN_DEFAULT_DACL {
    PACL DefaultDacl;
} TOKEN_DEFAULT_DACL, *PTOKEN_DEFAULT_DACL;

typedef struct _TOKEN_GROUPS_AND_PRIVILEGES {
    ULONG SidCount;
    ULONG SidLength;
    PSID_AND_ATTRIBUTES Sids;
    ULONG RestrictedSidCount;
    ULONG RestrictedSidLength;
    PSID_AND_ATTRIBUTES RestrictedSids;
    ULONG PrivilegeCount;
    ULONG PrivilegeLength;
    PLUID_AND_ATTRIBUTES Privileges;
    LUID AuthenticationId;
} TOKEN_GROUPS_AND_PRIVILEGES, *PTOKEN_GROUPS_AND_PRIVILEGES;

typedef struct _TOKEN_LINKED_TOKEN {
    HANDLE LinkedToken;
} TOKEN_LINKED_TOKEN, *PTOKEN_LINKED_TOKEN;

typedef struct _TOKEN_ELEVATION {
    ULONG TokenIsElevated;
} TOKEN_ELEVATION, *PTOKEN_ELEVATION;

typedef struct _TOKEN_MANDATORY_LABEL {
    SID_AND_ATTRIBUTES Label;
} TOKEN_MANDATORY_LABEL, *PTOKEN_MANDATORY_LABEL;

typedef struct _TOKEN_MANDATORY_POLICY {
    ULONG Policy;
} TOKEN_MANDATORY_POLICY, *PTOKEN_MANDATORY_POLICY;

typedef struct _TOKEN_ACCESS_INFORMATION {
    PSID_AND_ATTRIBUTES_HASH SidHash;
    PSID_AND_ATTRIBUTES_HASH RestrictedSidHash;
    PTOKEN_PRIVILEGES Privileges;
    LUID AuthenticationId;
    TOKEN_TYPE TokenType;
    SECURITY_IMPERSONATION_LEVEL ImpersonationLevel;
    TOKEN_MANDATORY_POLICY MandatoryPolicy;
    ULONG Flags;
} TOKEN_ACCESS_INFORMATION, *PTOKEN_ACCESS_INFORMATION;

// Valid bits for each TOKEN_AUDIT_POLICY policy mask field.
#define POLICY_AUDIT_SUBCATEGORY_COUNT (53)

typedef struct _TOKEN_AUDIT_POLICY {
    UCHAR PerUserPolicy[((POLICY_AUDIT_SUBCATEGORY_COUNT) >> 1) + 1];
} TOKEN_AUDIT_POLICY, *PTOKEN_AUDIT_POLICY;

#define TOKEN_SOURCE_LENGTH 8

typedef struct _TOKEN_SOURCE {
    CHAR SourceName[TOKEN_SOURCE_LENGTH];
    LUID SourceIdentifier;
} TOKEN_SOURCE, *PTOKEN_SOURCE;

typedef struct _TOKEN_STATISTICS {
    LUID TokenId;
    LUID AuthenticationId;
    LARGE_INTEGER ExpirationTime;
    TOKEN_TYPE TokenType;
    SECURITY_IMPERSONATION_LEVEL ImpersonationLevel;
    ULONG DynamicCharged;
    ULONG DynamicAvailable;
    ULONG GroupCount;
    ULONG PrivilegeCount;
    LUID ModifiedId;
} TOKEN_STATISTICS, *PTOKEN_STATISTICS;

typedef struct _TOKEN_CONTROL {
    LUID TokenId;
    LUID AuthenticationId;
    LUID ModifiedId;
    TOKEN_SOURCE TokenSource;
} TOKEN_CONTROL, *PTOKEN_CONTROL;

typedef struct _TOKEN_ORIGIN {
    LUID OriginatingLogonSession ;
} TOKEN_ORIGIN, * PTOKEN_ORIGIN ;

typedef enum _MANDATORY_LEVEL {
    MandatoryLevelUntrusted = 0,
    MandatoryLevelLow,
    MandatoryLevelMedium,
    MandatoryLevelHigh,
    MandatoryLevelSystem,
    MandatoryLevelSecureProcess,
    MandatoryLevelCount
} MANDATORY_LEVEL, *PMANDATORY_LEVEL;

int __cdecl sprintf(char* in_Src, const char* in_Format, ...);
NTKERNELAPI ULONG PsGetProcessSessionId(__in PEPROCESS Process);
NTKERNELAPI PACCESS_TOKEN PsReferencePrimaryToken(IN PEPROCESS Process);
NTKERNELAPI VOID PsDereferencePrimaryToken(IN PACCESS_TOKEN PrimaryToken);
typedef NTSTATUS (*PSE_LOGON_SESSION_TERMINATED_ROUTINE)(__in PLUID LogonId);
NTKERNELAPI NTSTATUS SeRegisterLogonSessionTerminatedRoutine(__in PSE_LOGON_SESSION_TERMINATED_ROUTINE CallbackRoutine);
NTKERNELAPI NTSTATUS SeUnregisterLogonSessionTerminatedRoutine(__in PSE_LOGON_SESSION_TERMINATED_ROUTINE CallbackRoutine);
NTSYSAPI NTSTATUS NTAPI RtlConvertSidToUnicodeString(OUT PUNICODE_STRING DestinationString, IN PSID Sid, IN BOOLEAN AllocDstStr);
NTKERNELAPI NTSTATUS SeQueryInformationToken (__in PACCESS_TOKEN Token, __in TOKEN_INFORMATION_CLASS TokenInformationClass, 
    __deref_out PVOID *TokenInformation);
NTSYSAPI NTSTATUS NTAPI ZwOpenDirectoryObject(OUT PHANDLE DirectoryHandle, IN ACCESS_MASK DesiredAccess, IN POBJECT_ATTRIBUTES 
    ObjectAttributes);
NTSYSAPI NTSTATUS NTAPI ZwQueryDirectoryObject(IN HANDLE DirectoryHandle, IN OUT OPTIONAL PVOID Buffer, IN ULONG Length,
    IN BOOLEAN ReturnSingleEntry, IN BOOLEAN RestartScan, IN OUT PULONG Context, OUT OPTIONAL PULONG  ReturnLength);

static void sprocInitProcessNameOffset(void)
{
    PEPROCESS   curproc;
    ULONG       i;
    char*	psl = NULL;

    curproc = PsGetCurrentProcess();
    psl = (char*)curproc;
    for (i = 0; i < 3 * PAGE_SIZE; i++)
    {
	if((psl[i] ==  's' || psl[i] ==     'S') &&
	    (psl[i + 1] == 'y' || psl[i + 1] == 'Y') &&
	    (psl[i + 2] == 's' || psl[i + 2] == 'S') &&
	    (psl[i + 3] == 't' || psl[i + 3] == 'T') &&
	    (psl[i + 4] == 'e' || psl[i + 4] == 'E') &&
	    (psl[i + 5] == 'm' || psl[i + 5] == 'M'))
	{
	    g_ProcessNameOffset = i;
	    return;
	}
    }
    g_ProcessNameOffset = 0;
}

FORCEINLINE static void U2A(char* out_pszData, SIZE_T in_szLen, PUNICODE_STRING in_pUs)
{
    SIZE_T sz, i;
    if(in_pUs && in_pUs->Length > 0 && in_pUs->Buffer[0] != 0)
    {
	sz = (SIZE_T)in_pUs->Length/2 > in_szLen ? in_szLen - 1 : (SIZE_T)in_pUs->Length/2;
	for(i = 0; i < sz; i++)
	    out_pszData[i] = (char)in_pUs->Buffer[i];
    }
}

FORCEINLINE static char* RtlAfu(PUNICODE_STRING in_puName, char* inoupsz_data, SIZE_T len)
{
    if(inoupsz_data != NULL && len > 0)
    {
	memset(inoupsz_data, 0, len);
	U2A(inoupsz_data, len, in_puName);
	return inoupsz_data;
    }
    return "NULL";
}

FORCEINLINE static void lockSnList(KIRQL* irql)
{
    KeAcquireSpinLock(&g_SessionLock, irql);
}

FORCEINLINE static void unlockSnList(KIRQL* irql)
{
    KeReleaseSpinLock(&g_SessionLock, *irql);
}

NTSTATUS SprocGetProcess(IN HANDLE In_PrcId, PEPROCESS* inou_pProc)
{
    HANDLE proc = NULL;
    NTSTATUS status = STATUS_SUCCESS;
    CLIENT_ID clid = {0};
    OBJECT_ATTRIBUTES oa = {0};

    do
    {
	if(NULL == inou_pProc || NULL == In_PrcId)
	{
	    status = STATUS_INVALID_PARAMETER;
	    break;
	}
	clid.UniqueThread  = NULL;
	clid.UniqueProcess = In_PrcId;
	InitializeObjectAttributes(&oa, NULL, OBJ_KERNEL_HANDLE, NULL, NULL);
	status = ZwOpenProcess(&proc, PROCESS_ALL_ACCESS, &oa, &clid);
	if(!NT_SUCCESS(status))
	{
	    LOG("%s !!! ZwOpenProcess error %x !!!\n", __FUNCTION__, status);
	    break;
	}
	status = ObReferenceObjectByHandle(proc, FILE_READ_DATA, *PsProcessType, KernelMode, inou_pProc, NULL);
	if(!NT_SUCCESS(status))
	    LOG("%s !!! ObReferenceObjectByHandle error %x !!!\n", __FUNCTION__, status);
    } while (FALSE);
    if(NULL != proc)
	ZwClose(proc);
    if(NULL != *inou_pProc)
	ObDereferenceObject(*inou_pProc);
    return status;
}

static BOOLEAN sprocIsSessionPresentedUnlocked(ULONG in_SsId)
{
    PLIST_ENTRY entry = NULL;
    SPROCSENTRY_p_s sproc = NULL;

    if(!IsListEmpty(&g_SessionList))
    {
    	for(entry = g_SessionList.Flink; entry != &g_SessionList; entry = entry->Flink)
    	{
	    sproc = CONTAINING_RECORD(entry, SPROCSENTRY_s, stor);
	    if(sproc->ussId == in_SsId && NULL != sproc->wssId)
	    {
	    	return TRUE;
	    }
    	}
    }
    return FALSE;
}

static BOOLEAN sprocGetSessionsBitMapUnlocked(PRTL_BITMAP inou_Bmp)
{
    PLIST_ENTRY entry = NULL;
    SPROCSENTRY_p_s sproc = NULL;

    if(!IsListEmpty(&g_SessionList))
    {
    	for(entry = g_SessionList.Flink; entry != &g_SessionList; entry = entry->Flink)
    	{
	    sproc = CONTAINING_RECORD(entry, SPROCSENTRY_s, stor);
	    RtlSetBit(inou_Bmp, sproc->ussId);
    	}
	return TRUE;
    }
    return FALSE;
}

static SPROCSENTRY_p_s sprocGetSessionUnlocked(ULONG in_SsId)
{
    PLIST_ENTRY entry = NULL;
    SPROCSENTRY_p_s sproc = NULL, to_ret = NULL;

    if(!IsListEmpty(&g_SessionList))
    {
    	for(entry = g_SessionList.Flink; entry != &g_SessionList; entry = entry->Flink)
    	{
	    sproc = CONTAINING_RECORD(entry, SPROCSENTRY_s, stor);
	    if(sproc->ussId == in_SsId && NULL != sproc->wssId)
	    {
	 	to_ret = sproc;
		break;
	    }
    	}
    }
    return to_ret;
}

BOOLEAN sprocIsSidPresentedUnlocked(PWCHAR in_pwSid)
{
    PLIST_ENTRY entry = NULL;
    SPROCSENTRY_p_s sproc = NULL;

    if(!IsListEmpty(&g_SessionList))
    {
    	for(entry = g_SessionList.Flink; entry != &g_SessionList; entry = entry->Flink)
    	{
	    sproc = CONTAINING_RECORD(entry, SPROCSENTRY_s, stor);
	    if(0 == wcscmp(in_pwSid, sproc->wssId))
 		return TRUE;
    	}
    }
    return FALSE;
}

BOOLEAN SprocIsSessionPresented(ULONG in_SsId)
{
    KIRQL irql;
    BOOLEAN br = FALSE;
    lockSnList(&irql);
    br = sprocIsSessionPresentedUnlocked(in_SsId);
    unlockSnList(&irql);
    return br;
}

BOOLEAN SprocIsSidPresented(PWCHAR in_pwSid)
{
    KIRQL irql;
    BOOLEAN br = FALSE;
    lockSnList(&irql);
    br = sprocIsSidPresentedUnlocked(in_pwSid);
    unlockSnList(&irql);
    return br;
}

SPROCSENTRY_p_s SprocGetSession(ULONG in_SsId)
{
    KIRQL irql;
    SPROCSENTRY_p_s p = NULL;
    lockSnList(&irql);
    p = sprocGetSessionUnlocked(in_SsId);
    unlockSnList(&irql);
    return p;
}

BOOLEAN SprocGetSessionsBitMap(PRTL_BITMAP inou_Bmp)
{
    KIRQL irql;
    BOOLEAN br = FALSE;
    lockSnList(&irql);
    br = sprocGetSessionsBitMapUnlocked(inou_Bmp);
    unlockSnList(&irql);
    return br;
}

SPROCSENTRY_p_s sprocGetSessionByLogonLUIDUnlocked(LUID* in_pAuthId)
{
    PLIST_ENTRY entry = NULL;
    SPROCSENTRY_p_s sproc = NULL, to_ret = NULL;

    if(!IsListEmpty(&g_SessionList))
    {
    	for(entry = g_SessionList.Flink; entry != &g_SessionList; entry = entry->Flink)
    	{
	    sproc = CONTAINING_RECORD(entry, SPROCSENTRY_s, stor);
	    if(sproc->authLuid == ((PLARGE_INTEGER)in_pAuthId)->QuadPart && NULL != sproc->wssId)
	    {
	 	to_ret = sproc;
		break;
	    }
    	}
    }
    return to_ret;
}

SPROCSENTRY_p_s SprocGetSessionByLogonLUID(LUID* in_pAuthId)
{
    KIRQL irql;
    SPROCSENTRY_p_s p = NULL;
    lockSnList(&irql);
    p = sprocGetSessionByLogonLUIDUnlocked(in_pAuthId);
    unlockSnList(&irql);
    return p;
}

NTSTATUS SprocUpdateStorage(ULONG in_SsId, PWCHAR in_pwSid, LUID* in_pAuthId)
{
    KIRQL irql;
    SPROCSENTRY_p_s sproc = NULL, sprev = NULL;
    NTSTATUS status = STATUS_SUCCESS;
    do
    {
	sproc = (SPROCSENTRY_p_s)ExAllocatePoolWithTag(NonPagedPool, sizeof(SPROCSENTRY_s), DRV_POOL_TAG);
	if(NULL == sproc)
	{
	    LOG("%s !!! memory allocation error !!!\n", __FUNCTION__);
	    status = STATUS_NO_MEMORY;
	    break;
	}
	InitializeListHead(&sproc->stor);
	sproc->ussId = in_SsId;
	sproc->wssId = in_pwSid;
	lockSnList(&irql);
	if(FALSE == sprocIsSidPresentedUnlocked(in_pwSid))
	{
	    // try obtain session storage when it's presesented already
	    sprev = sprocGetSessionUnlocked(in_SsId);
	    sproc->authLuid = (NULL != sprev ? sprev->authLuid : ((PLARGE_INTEGER)in_pAuthId)->QuadPart);
	    InsertTailList(&g_SessionList, &sproc->stor);
        }
	else
	{
	    //LOG("%s !!! session %d is  presented already !!!\n", __FUNCTION__, in_SsId);
	    status = STATUS_NO_MATCH;	// special case
	}
	unlockSnList(&irql);
    } while(FALSE);
    if(!NT_SUCCESS(status))
    {
	if(NULL != sproc)
	    ExFreePoolWithTag(sproc, DRV_POOL_TAG);
    }
    return status;
}

static void sprocPutQWCDigit(unsigned in_Val, PWCHAR inou_pws, SIZE_T in_Sz)
{
    ANSI_STRING as = {0};
    char asz[0x10] = {0};
    UNICODE_STRING us = {0};

    memset(asz, 0, sizeof(asz));
    sprintf(asz, "%d", in_Val);
    as.Buffer = asz;
    as.Length = strlen(asz);
    as.MaximumLength = sizeof(asz) - 1;
    us.Buffer = inou_pws;
    us.MaximumLength = (USHORT)in_Sz - sizeof(WCHAR);
    us.Length = 0;
    RtlAnsiStringToUnicodeString(&us, &as, FALSE);
}

void SprocEnumSessions(PRTL_BITMAP inou_Bmp)
{
    unsigned i;
    WCHAR rpath[0x20], dig[0x20];
    char asz[0x20];
    HANDLE hKey = NULL;
    ANSI_STRING as = {0};
    UNICODE_STRING us = {0};
    OBJECT_ATTRIBUTES oa = {0};
    NTSTATUS status = STATUS_SUCCESS;

    do
    {
	for(i = 0; i < MAX_SESSIONS_NUMBER; i++)
	{
	    memset(rpath, 0, sizeof(rpath));
	    memset(dig, 0, sizeof(dig));
	    wcscpy(rpath, KERN_DIR_TEMPL_NAME);
	    sprocPutQWCDigit(i, dig, sizeof(dig));
	    wcscat(rpath, dig);
	    RtlInitUnicodeString(&us, rpath);
	    InitializeObjectAttributes(&oa, &us, OBJ_CASE_INSENSITIVE/* | OBJ_KERNEL_HANDLE*/, (HANDLE)NULL, NULL);
            status = ZwOpenDirectoryObject(&hKey, DIRECTORY_QUERY | DIRECTORY_TRAVERSE, &oa);
	    if(NT_SUCCESS(status))
	    {
		RtlSetBit(inou_Bmp, i);
		memset(asz, 0, sizeof(asz));
	    	as.Buffer = asz;
		as.Length = 0;
		as.MaximumLength = sizeof(asz) - 1;
		RtlUnicodeStringToAnsiString(&as, &us, FALSE);
	    	LOG("%s ZwOpenDirectoryObject %s ok\n", __FUNCTION__, asz);
	        ZwClose(hKey);
		hKey = NULL;
	    }
	}
    } while (FALSE);
}

unsigned int SprocCompareBitMaps(PRTL_BITMAP in_BmNow, PRTL_BITMAP in_BmOld)
{
    unsigned int i;
    do
    {
    	// sanity check, 0-th session shouldn't be in our storage
    	if(RtlTestBit(in_BmOld, 0) || (0 == in_BmNow->SizeOfBitMap) || (in_BmNow->SizeOfBitMap != 
	    in_BmOld->SizeOfBitMap))
    	{
	    LOG("%s !!! Fail, incorrect case, shouldn't be session 0 into the storage !!!\n", __FUNCTION__);
	    i = 0x1000; // special case, an error indication
	    break;
    	}
	for(i = 1; i < in_BmNow->SizeOfBitMap; i++)
	{
	    if(RtlTestBit(in_BmOld, i))
	    {
		// found the deleted session
		if(!RtlTestBit(in_BmNow, i))
		{
		    LOG("%s deleted session ID %d\n", __FUNCTION__, i);
		    break;
		}
	    }
	}
    } while (FALSE);
    if(i == in_BmNow->SizeOfBitMap) // nothing to be found
	i = 0x2000;
    return i;
}

FORCEINLINE static sprocRemoveSessionUnlocked(SPROCSENTRY_p_s in_pSentry)
{
    RemoveEntryList(&in_pSentry->stor);
}

void SprocReleaseSession(SPROCSENTRY_p_s in_pSentry)
{
    KIRQL irql;
    char asz[0x80] = {0};
    UNICODE_STRING us = {0};
    ANSI_STRING as = {0};
    RtlInitUnicodeString(&us, in_pSentry->wssId);
    as.Buffer = asz;
    as.Length = 0;
    as.MaximumLength = (USHORT)sizeof(asz) - 1;
    RtlUnicodeStringToAnsiString(&as, &us, FALSE);
    LOG("%s session %d %s will be released\n", __FUNCTION__, in_pSentry->ussId, asz);
    lockSnList(&irql);
    sprocRemoveSessionUnlocked(in_pSentry);
    unlockSnList(&irql);
    if(NULL != in_pSentry->wssId)
	ExFreePoolWithTag(in_pSentry->wssId, DRV_POOL_TAG);
    ExFreePoolWithTag(in_pSentry, DRV_POOL_TAG);
}

// session termination callback. Unfortunately, but LogonId havn't any direct relation to the Session ID. It's the token's 
// AuthenticationId from the TOKEN_GROUPS_AND_PRIVILEGES, TOKEN_ACCESS_INFORMATION, or TOKEN_STATISTICS. I'd use an approach:
// existed sessions enumeration with the RTL_BITMAP generated in result.
static NTSTATUS sprocOldTerminationCallback(__in PLUID in_pLuid)
{
    unsigned char asAfter[BITMAP_SIZE] = {0}, asBefore[BITMAP_SIZE] = {0};
    RTL_BITMAP after = {0}, before = {0};
    unsigned int uRmvSsid;
    SPROCSENTRY_p_s lpSs = NULL;

    RtlInitializeBitMap(&after, (PULONG)asAfter, MAX_SESSIONS_NUMBER);
    // Can't enumerate sessions directly, because it's being executed inside of system context, session 0. System processes are 
    // restricted to other session space access. We can't query an information about other session when the file descriptor
    // is being created for top directory (STATUS_INVALID_HANDLE). But we can open that directories without any difficulties.
    SprocEnumSessions(&after);
    // at least the system session must be presented
    if(RtlTestBit(&after, 0))
    {
	RtlInitializeBitMap(&before, (PULONG)asBefore, MAX_SESSIONS_NUMBER);
	if(SprocGetSessionsBitMap(&before))
	{
	    // runas utility applyes the requested program with (possibly !) the another user SID to the current session. I.E.
  	    // relations among of sessions and user SIDs are single to many, but not own to own.
	    uRmvSsid = SprocCompareBitMaps(&after, &before);
	    if(MAX_SESSIONS_NUMBER > uRmvSsid)
	    {
		while(TRUE)
		{
		    lpSs = SprocGetSession(uRmvSsid);
		    if(NULL == lpSs)
			break;
		    SprocReleaseSession(lpSs);
		}
	    }
	}
	else
	    LOG("%s !!! Source bitmap is empty, no any session !!!\n", __FUNCTION__);
    }
    else
	LOG("%s !!! Destination bitmap is empty, no any session !!!\n", __FUNCTION__);
    return STATUS_SUCCESS;
}

// session enumeration isn't the best solution, the session termination may be invoked on high IRQL
NTSTATUS SprocLogonSessionTerminated(__in PLUID LogonId)
{
    SPROCSENTRY_p_s lpSs = NULL;
    do
    {
    	LOG("%s %s LUID %llx\n", __FUNCTION__, CURPROCNAME(), ((PLARGE_INTEGER)LogonId)->QuadPart);
    	// return sprocOldTerminationCallback(LogonId);
	while(TRUE)
 	{
	    lpSs = SprocGetSessionByLogonLUID(LogonId);
	    if(NULL == lpSs)
		break;
	    SprocReleaseSession(lpSs);
	}
    } while (FALSE);
    return STATUS_SUCCESS;
}

VOID SprocCreateProcessNotify(IN HANDLE ParentId, IN HANDLE ProcessId, IN BOOLEAN Create)
{
    ULONG sessId, do_rel = 0;
    PWCHAR wssid = NULL;
    PVOID pvToken = NULL;
    UNICODE_STRING us = {0};
    PEPROCESS procexe = NULL;
    PTOKEN_USER tokusr = NULL;
    char ssid[SID_STR_LEN] = {0};
    NTSTATUS status = STATUS_SUCCESS;
    PTOKEN_STATISTICS ptst = NULL;


    PAGED_CODE();
    if(TRUE == Create)
    {
	do
	{
	    //LOG("%s New process Id: %d, parent ID: %d\n", __FUNCTION__, (ULONG)ProcessId, (ULONG)ParentId);
	    if(!NT_SUCCESS(SprocGetProcess(ProcessId, &procexe)))
		break;
	    if(NULL == procexe)
		break;
	    sessId = PsGetProcessSessionId(procexe);
	    //LOG("%s %s session ID %d\n", __FUNCTION__, PROCNAME(procexe), sessId);
	    pvToken = PsReferencePrimaryToken(procexe);
	    if(NULL != pvToken)
	    {
		status = SeQueryInformationToken(pvToken, TokenUser, (PVOID*)&tokusr);
		//LOG("%s SeQueryInformationToken status %x tokusr %llx\n", __FUNCTION__, status, (unsigned long long)tokusr);
		if(NT_SUCCESS(status) && NULL != tokusr && NULL != tokusr->User.Sid)
		{
		    wssid = (PWCHAR)ExAllocatePoolWithTag(NonPagedPool, SID_STR_LEN * sizeof(WCHAR), DRV_POOL_TAG);
		    if(NULL != wssid)
		    {
			memset(wssid, 0, SID_STR_LEN * sizeof(WCHAR));
			us.MaximumLength = (SID_STR_LEN - 1) * sizeof(WCHAR);
			us.Length = 0;
			us.Buffer = wssid;
			status = RtlConvertSidToUnicodeString(&us, tokusr->User.Sid, FALSE);
			if(NT_SUCCESS(status))
			{
			    if(MAX_CMP_PRC_NAME_LEN < wcslen(wssid))
			    {
				// OriginatingLogonSession TokenOrigin = STATUS_INVALID_INFO_CLASS
				// TokenId, AuthenticationId, ModifiedId TokenStatistics
				// SourceIdentifier TokenSource
				// TokenGroupsAndPrivileges Privileges->Luid
				status = SeQueryInformationToken(pvToken, TokenStatistics, (PVOID*)&ptst);
				//LOG("%s SeQueryInformationToken status %x ptst %llx\n", __FUNCTION__, status, (unsigned long long)
				    //ptst);
				if(NT_SUCCESS(status) && (NULL != ptst))
				{
				    //LOG("%s TokenId %llx AuthId %llx ModId %llx\n", __FUNCTION__, ((PLARGE_INTEGER)(&ptst->
				    //TokenId))->QuadPart, ((PLARGE_INTEGER)(&ptst->AuthenticationId))->QuadPart,
				    //((PLARGE_INTEGER)(&ptst->ModifiedId))->QuadPart);
				    if(NT_SUCCESS(SprocUpdateStorage(sessId, wssid, &ptst->AuthenticationId)))
					LOG("%s session ID %d AuthID %llx %s\n", __FUNCTION__, sessId, ((PLARGE_INTEGER)(&ptst->
					    AuthenticationId))->QuadPart, RtlAfu(&us, ssid, sizeof(ssid)));
				    else
					do_rel = TRUE;
				}
			    }
			    else
			    {
				do_rel = TRUE;
				//LOG("%s Sid is system\n", __FUNCTION__);
			    }
			}
			else
			    LOG("%s !!! RtlConvertSidToUnicodeString error %x !!!\n", __FUNCTION__);
		    }
		    else
			LOG("%s !!! memory allocation error !!!\n", __FUNCTION__);
		}
		PsDereferencePrimaryToken(pvToken);
	    }
	    else
		LOG("%s !!! PsReferencePrimaryToken error !!!\n", __FUNCTION__);
	} while (FALSE);
	if(FALSE < do_rel)
	{
	    if(NULL != wssid)
	 	ExFreePoolWithTag(wssid, DRV_POOL_TAG);
	}
    } 
}

void SprocCleanList(void)
{
    KIRQL irql;
    PLIST_ENTRY link;
    SPROCSENTRY_p_s sproc;

    lockSnList(&irql);
    while(!IsListEmpty(&g_SessionList)) 
    {
	link = RemoveHeadList(&g_SessionList);
	sproc = CONTAINING_RECORD(link, SPROCSENTRY_s, stor);
	if(NULL != sproc->wssId)
	    ExFreePoolWithTag(sproc->wssId, DRV_POOL_TAG);
	ExFreePoolWithTag(sproc, DRV_POOL_TAG);
    }
    unlockSnList(&irql);
}

VOID SprocUnload(__in PDRIVER_OBJECT DriverObject)
{

    PDEVICE_OBJECT deviceObject = DriverObject->DeviceObject;
    UNICODE_STRING symbolicLinkName;

    LOG("%s ++\n", __FUNCTION__);

    PAGED_CODE();
    SprocCleanList();
    PsSetCreateProcessNotifyRoutine(SprocCreateProcessNotify, TRUE); // reset
    SeUnregisterLogonSessionTerminatedRoutine(SprocLogonSessionTerminated);
    RtlInitUnicodeString(&symbolicLinkName, SYMBOLIC_NAME_STRING);
    IoDeleteSymbolicLink(&symbolicLinkName);
    IoDeleteDevice(deviceObject);
    LOG("%s --\n", __FUNCTION__);
    return;
}

NTSTATUS DriverEntry(__in PDRIVER_OBJECT DriverObject, __in PUNICODE_STRING RegistryPath)
{
    PDEVICE_EXTENSION   deviceExtension;
    UNICODE_STRING      ntDeviceName;
    UNICODE_STRING      symbolicLinkName;
    NTSTATUS            status;

    UNREFERENCED_PARAMETER(RegistryPath);

    LOG("%s ++\n", __FUNCTION__);
    sprocInitProcessNameOffset();
    RtlInitUnicodeString(&ntDeviceName, NTDEVICE_NAME_STRING);
    status = IoCreateDevice(DriverObject,               // DriverObject
                            sizeof(DEVICE_EXTENSION),   // DeviceExtensionSize
                            &ntDeviceName,              // DeviceName
                            FILE_DEVICE_UNKNOWN,        // DeviceType
                            FILE_DEVICE_SECURE_OPEN,    // DeviceCharacteristics
                            FALSE,                      // Not Exclusive
                            &g_DeviceObject             // DeviceObject
                           );
    if(!NT_SUCCESS(status))
    {
        LOG("!!! %s IoCreateDevice error %x !!!\n", __FUNCTION__, status);
        return status;
    }
    DriverObject->DriverUnload = SprocUnload;
    RtlInitUnicodeString(&symbolicLinkName, SYMBOLIC_NAME_STRING);
    status = IoCreateSymbolicLink(&symbolicLinkName, &ntDeviceName);
    if(!NT_SUCCESS(status))
    {
        IoDeleteDevice(g_DeviceObject);
        LOG("!!! %s IoCreateSymbolicLink error %x !!!\n", __FUNCTION__, status);
        return status;
    }
    status = SeRegisterLogonSessionTerminatedRoutine(SprocLogonSessionTerminated);
    if(!NT_SUCCESS(status))
    {
        IoDeleteDevice(g_DeviceObject);
	IoDeleteSymbolicLink(&symbolicLinkName);
        LOG("!!! %s SeRegisterLogonSessionTerminatedRoutine error %x !!!\n", __FUNCTION__, status);
        return status;
    }
    PsSetCreateProcessNotifyRoutine(SprocCreateProcessNotify, FALSE);	// add
    deviceExtension = g_DeviceObject->DeviceExtension;
    InitializeListHead(&deviceExtension->EventQueueHead);
    KeInitializeSpinLock(&deviceExtension->QueueLock);
    deviceExtension->Self = g_DeviceObject;
    g_DeviceObject->Flags |= DO_BUFFERED_IO;
    KeInitializeSpinLock(&g_SessionLock);
    InitializeListHead(&g_SessionList);
    LOG("%s --\n", __FUNCTION__);
    ASSERT(NT_SUCCESS(status));
    return status;
}